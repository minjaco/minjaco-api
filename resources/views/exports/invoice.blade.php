<style>
    .f-BB {
        font-weight: bold;
    }

    .f-BBB {
        font-weight: 2000;
    }

    .f-14 {
        font-size: 14px;
    }

    .f-16 {
        font-size: 16px;
    }

    .f-18 {
        font-size: 18px;
    }

    .f-24 {
        font-size: 24px;
    }

    .right {
        text-align: right;
    }

    table {
        border-collapse: collapse;
    }

    th {
        background-color: #eee;
        color: #000;
        border-top: 1px solid #000;
        border-bottom: 1px solid #000;
    }

    td {
        padding: 2px;
    }

    .row {
        border: none;
        border-bottom: 1px solid lightgray;
    }

    .rowLeft {
        border: none;
        border-bottom: 1px solid lightgray;
    }

    .font-sarabun {
        font-family: 'thsarabun' !important;
    }
</style>
<table style="width: 100%;">
    <tr>
        <td align="left" width="100px">
            <img src='https://www.minjaco.com/images/Other/logo-only.png' height='100px'>
        </td>
        <td align="left" class="font-sarabun" style="font-size:16px;">
            <strong>Minja Co., Ltd.<br>
                77 Moo 19,<br>
                T. Makhuea Chae, A. Muang<br>
                Lamphun 51000 Thailand<br>
                ID: 0505560009852 (Head office)</strong>
        </td>
        <td align="right" class="font-sarabun" style="font-size:24px;vertical-align: top;">
            <strong>Tax Invoice No:<br>
                Date:</strong>
        </td>
        <td align="right" class="font-sarabun" width="110px" style="font-size:24px;vertical-align: top;">
            <strong>{{ $invoice_id }}<br>
                {{ date('d-m-Y', strtotime($invoice->time_stamp)) }}
            </strong>
        </td>
    </tr>
    <tr>
        <td colspan="4" style="height: 10px;"></td>
    </tr>
    <tr>
        <td colspan="4" style="border-top: 1px solid #000000">&nbsp;</td>
    </tr>
</table>
<table width="100%">
    <tr>
        <td align="left" class="font-sarabun" style="font-size:16px;vertical-align: top;">
            <strong>
                <span style="font-size: 24px;">Billing Details</span><br><br>
                {{$billing->bill_first_name}} {{$billing->bill_last_name}}<br>
                {{$billing->bill_street_1}}<br>
                {{$billing->bill_street_2}}<br>
                {{$billing->bill_city}},{{$billing->bill_state}},{{$billing->bill_zip}}<br>
                {{$billing->bill_country}}<br><br>
                Phone :{{$billing->bill_phone}}<br>
                Email :{{$billing->bill_email}}<br>
            </strong>
        </td>
        <td align="left" class="font-sarabun" style="font-size:16px;vertical-align: top;">
			<span lang="ja" class="lang_ja" style="font-weight: bold">
				<span style="font-size: 24px;">Shipping Details</span><br><br>
				 {{$shipping->ship_first_name}} {{$shipping->ship_last_name}}<br>
                {{$shipping->ship_street_1}}<br>
                {{$shipping->ship_street_2}}<br>
                {{$shipping->ship_city}},{{$shipping->ship_state}},{{$shipping->ship_zip}}<br>
                {{$shipping->ship_country}}<br><br>
                Phone :{{$shipping->ship_phone}}<br>
                Email :{{$shipping->ship_email}}<br>
			</span>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="height: 5px;"></td>
    </tr>
    <tr>
        <td colspan="2" style="border-top: 1px solid #000000">&nbsp;</td>
    </tr>
</table>
<strong class="f-24 font-sarabun f-BB">Order Items</strong><br><br>
<table width="100%" class="font-sarabun f-BB f-16">
    <thead>
    <tr>
        <th width="10%" style="text-align: center">Order No.</th>
        <th width="10%" style="text-align: center">Order Date</th>
        <th width="10%" style="text-align: center">Qty</th>
        <th width="10%" style="text-align: center">Unit</th>
        <th width="10%" style="text-align: center">Code</th>
        <th width="30%" style="text-align: left">Description</th>
        <th width="10%" style="text-align: center">Price</th>
        <th width="10%" style="text-align: center">Total</th>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>

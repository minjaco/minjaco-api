<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCrystalRawMaterialsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crystal_raw_materials', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('product_id')->unsigned()->nullable();
            $table->integer('part_number_id')->unsigned()->nullable();
            $table->string('part_number', 50)->nullable();
            $table->string('description')->nullable();
            $table->string('size', 100)->nullable();
            $table->decimal('width')->unsigned()->nullable();
            $table->decimal('length')->unsigned()->nullable();
            $table->integer('quantity')->nullable();
            $table->string('unit', 50)->nullable();
            $table->decimal('unit_price')->unsigned()->nullable();
            $table->decimal('ship')->unsigned()->nullable();
            $table->decimal('total')->unsigned()->nullable();
            $table->decimal('loss')->unsigned()->nullable();
            $table->timestamp('time_stamp')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('crystal_raw_materials');
    }
}

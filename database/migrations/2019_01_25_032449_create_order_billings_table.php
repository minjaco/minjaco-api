<?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Support\Facades\Schema;

    class CreateOrderBillingsTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create(
                'order_billings',
                function (Blueprint $table) {
                    $table->increments('id');
                    $table->bigInteger('order_id')->index('order_id');
                    $table->string('first_name');
                    $table->string('last_name');
                    $table->string('company')->nullable();
                    $table->string('street_1');
                    $table->string('street_2')->nullable();
                    $table->string('city');
                    $table->string('zip');
                    $table->string('country');
                    $table->string('country_iso2');
                    $table->string('state')->nullable();
                    $table->string('phone');
                    $table->string('email');
                    $table->timestamps();
                }
            );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('order_billings');
        }
    }

<?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Support\Facades\Schema;

    class CreateCustomersTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create(
                'customers',
                function (Blueprint $table) {
                    $table->bigIncrements('id')->unsigned();
                    $table->uuid('uuid')->index('customer_unique')->comment('Customer unique id');
                    $table->string('name')->index('customer_name')->comment('Customer name surname');
                    $table->string('phone_number')->index('customer_phone')->comment('Customer phone number');
                    $table->string('email')->index('customer_email')->comment('Customer email address');
                    $table->text('address')->nullable()->comment('Customer address');
                    $table->string('instagram')->nullable()->comment('Customer instagram url');
                    $table->string('facebook')->nullable()->comment('Customer facebook url');
                    $table->string('linkedin')->nullable()->comment('Customer linkedin url');
                    $table->string('website')->nullable()->comment('Customer website url');
                    $table->string('federations')->nullable()->comment('Customer federations');
                    $table->string('division')->nullable()->comment('Customer division');
                    $table->longText('profile')->nullable()->comment('Customer profile');
                    $table->tinyInteger('status')->default(1)->comment('Customer status : 0-Passive, 1-Active');
                    $table->timestamps();
                    $table->softDeletes();
                }
            );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('customers');
        }
    }

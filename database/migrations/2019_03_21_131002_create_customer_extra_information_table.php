<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerExtraInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_extra_information', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('customer_id')->unsigned()->index('customer_id');
            $table->string('title');
            $table->integer('is_default')->default(0);
            $table->string('value')->index('value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_extra_information');
    }
}

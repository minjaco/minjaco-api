<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItemCardsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_cards', function (Blueprint $table) {
            $table->integer('runid', true);
            $table->string('product_code')->nullable();
            $table->string('product_type')->nullable();
            $table->string('collection')->nullable();
            $table->string('design_code')->nullable();
            $table->string('color')->nullable();
            $table->string('original_code')->nullable();
            $table->float('total_rm', 10, 0)->nullable()->unsigned();
            $table->float('total_pm', 10, 0)->nullable()->unsigned();
            $table->float('total_av', 10, 0)->nullable()->unsigned();
            $table->float('foh_cost', 10, 0)->nullable()->unsigned();
            $table->float('shipping_cost', 10, 0)->nullable()->unsigned();
            $table->float('total_mc', 10, 0)->nullable()->unsigned();
            $table->float('admin_cost', 10, 0)->nullable()->unsigned();
            $table->float('remake_cost', 10, 0)->nullable()->unsigned();
            $table->float('paypal_cost', 10, 0)->nullable()->unsigned();
            $table->float('total_cost', 10, 0)->nullable()->unsigned();
            $table->integer('margin')->nullable()->unsigned();
            $table->float('margin_thb', 10, 0)->nullable()->unsigned();
            $table->float('actual_margin', 10, 0)->nullable();
            $table->float('actual_margin_thb', 10, 0)->nullable();
            $table->float('rate_thb_usd', 10, 0)->nullable()->unsigned();
            $table->float('csp_thb', 10, 0)->nullable()->unsigned()->comment('Calculate selling price(THB)');
            $table->float('fsp_thb', 10, 0)->nullable()->unsigned()->comment('Final selling price(THB)');
            $table->float('csp_usd', 10, 0)->nullable()->unsigned()->comment('Calculate selling price(USD)');
            $table->float('fsp_usd', 10, 0)->nullable()->unsigned()->comment('Final selling price(USD)');
            $table->integer('status_card')->unsigned()->nullable()->default(1)->comment('1=normal item card, 2=draft item card');
            $table->text('how_to')->nullable();
            $table->float('net_margin', 10, 0)->nullable()->unsigned();
            $table->float('net_margin_per', 10, 0)->nullable()->unsigned();
            $table->string('create_user')->nullable();
            $table->string('update_user')->nullable();
            $table->dateTime('time_stamp')->nullable();

            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('item_cards');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDashboardMessageThreadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dashboard_message_threads', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('dashboard_message_id')->unsigned();
            $table->bigInteger('user_id')->index('user_id')->unsigned()->comment('Message was created by');
            $table->text('content')->comment('Message content');
            $table->tinyInteger('priority')->comment('1-Urgent, 2-Normal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dashboard_message_threads');
    }
}

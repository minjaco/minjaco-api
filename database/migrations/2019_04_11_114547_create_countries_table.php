<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCountriesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->string('country_name', 100)->nullable();
            $table->string('country_iso2', 10)->nullable();
            $table->string('country_iso3', 10)->nullable();
            $table->integer('country_ds')->nullable();
            $table->integer('country_ds_full')->nullable();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('countries');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFabricsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fabrics', function (Blueprint $table) {
            $table->integer('fabric_id')->primary();
            $table->string('fabric_code')->nullable();
            $table->string('fabric_old_code')->nullable();
            $table->integer('fabric_status')->nullable()->comment('0=disable,1=enable');
            $table->string('fabric_img');
            $table->string('fabric_name')->nullable();
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fabrics');
    }
}

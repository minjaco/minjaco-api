<?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Support\Facades\Schema;

    class AddExtraColumnsToCustomersTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::table(
                'customers',
                function (Blueprint $table) {
                    $table->bigInteger('customer_id')->index('customer_id')->unsigned()->after('id')->comment('Big Commerce customer id');
                    $table->timestamp('registered_at')->nullable()->after('status')->comment('Customer registration date');
                    $table->string('country')->nullable()->index('customer_country')->after('address')->comment('Customer country');
                    $table->string('team_name')->after('division')->nullable()->comment('Customer team name');
                    $table->tinyInteger('is_team_leader')->unsigned()->nullable()->after('team_name')->comment('If Customer is leader of given team');
                    $table->string('coach_name')->nullable()->after('is_team_leader')->comment('Customer coach name');
                    $table->tinyInteger('is_coach')->unsigned()->nullable()->after('coach_name')->comment('If Customer is also a coach');
                }
            );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::table(
                'customers',
                function (Blueprint $table) {
                    $table->dropColumn('customer_id');
                    $table->dropColumn('registered_at');
                    $table->dropColumn('country');
                    $table->dropColumn('team_name');
                    $table->dropColumn('is_team_leader');
                    $table->dropColumn('coach_name');
                    $table->dropColumn('is_coach');
                }
            );
        }
    }

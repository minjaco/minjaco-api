<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCampetedColumnsToCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->string('competed_before')->nullable()->after('is_coach')->comment('Is Customer competed before');
            $table->string('next_competition_date')->nullable()->after('competed_before')->nullable()->comment('Customer\'s next competition date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->dropColumn('competed_before');
            $table->dropColumn('next_competition_date');
        });
    }
}

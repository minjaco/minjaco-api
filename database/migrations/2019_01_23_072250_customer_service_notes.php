<?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Support\Facades\Schema;

    class CustomerServiceNotes extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create(
                'customer_service_notes',
                function (Blueprint $table) {
                    $table->increments('id')->unsigned();
                    $table->uuid('note_uuid')->index('n_uiid')->comment('Note unique id');
                    $table->bigInteger('customer_id')->unsigned()->comment('FK for customers table');
                    $table->integer('user_id')->unsigned()->comment('FK for users table');
                    $table->bigInteger('order_id')->unsigned()->comment('FK for orders table');
                    $table->text('content')->comment('Note content');
                    $table->timestamps();
                    $table->softDeletes();
                }
            );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('customer_service_notes');
        }
    }

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePackingRawMaterialsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packing_raw_materials', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('product_id')->unsigned()->nullable();
            $table->integer('part_number_id')->unsigned()->nullable();
            $table->string('part_number')->nullable();
            $table->string('description')->nullable();
            $table->string('size')->nullable();
            $table->integer('quantity')->unsigned()->nullable();
            $table->string('unit')->nullable();
            $table->decimal('unit_price')->unsigned()->nullable();
            $table->decimal('ship')->unsigned()->nullable();
            $table->decimal('total')->unsigned()->nullable();
            $table->decimal('loss')->unsigned()->nullable();
            $table->timestamp('time_stamp')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('packing_raw_materials');
    }
}

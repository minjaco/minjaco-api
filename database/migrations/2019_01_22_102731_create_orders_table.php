<?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Support\Facades\Schema;

    class CreateOrdersTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            // Refer : https://developer.bigcommerce.com/api-reference/orders/orders-api/orders/getorders
            Schema::create(
                'orders',
                function (Blueprint $table) {
                    $table->bigIncrements('id')->unsigned();
                    $table->bigInteger('order_id')->index('order_id')->unsigned();
                    $table->bigInteger('customer_id')->index('customer_id')->unsigned();
                    $table->bigInteger('minja_id')->index('minja_id')->unsigned();
                    $table->string('order_from');
                    $table->string('order_source');
                    $table->tinyInteger('items_total')->unsigned();
                    $table->string('country');
                    $table->string('payment_method');
                    $table->string('payment_provider_id');
                    $table->string('payment_status');
                    $table->string('external_source')->nullable();
                    $table->tinyInteger('bc_status_id')->unsigned()->comment('Status id of bigcommerce');
                    $table->string('bc_status')->comment('Status of bigcommerce');
                    $table->tinyInteger('mc_status_id')->index('mc_status_id')->unsigned()->comment('Status id of minjaco');
                    $table->tinyInteger('mc_old_status_id')->unsigned()->default(0)->comment('Previous status id of minjaco');
                    $table->tinyInteger('urgent_order')->unsigned()->default(0);
                    $table->tinyInteger('has_multiple_products')->unsigned()->default(0);
                    $table->decimal('coupon_discount');
                    $table->decimal('discount_amount');
                    $table->decimal('base_shipping_cost');
                    $table->decimal('subtotal_inc_tax');
                    $table->decimal('total_inc_tax');
                    $table->tinyInteger('payment_option')->unsigned()->comment('1=%50, 0=100% paid');
                    $table->text('customer_message')->nullable();
                    $table->text('staff_notes')->nullable();
                    $table->timestamp('show_date')->nullable()->comment('Competition date');
                    $table->timestamp('date_created')->index('date_created')->nullable();
                    $table->timestamp('date_modified')->nullable();
                    $table->timestamp('date_shipped')->nullable();
                    $table->timestamps();

                    //order_products -> api call
                }
            );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('orders');
        }
    }

<?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Support\Facades\Schema;

    class AddTypeToCustomerServiceNotesTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::table(
                'customer_service_notes',
                function (Blueprint $table) {
                    $table->tinyInteger('type')->unsigned()->after('content')->comment('1-Urgent, 2-Normal');
                }
            );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::table(
                'customer_service_notes',
                function (Blueprint $table) {
                    $table->dropColumn('type');
                }
            );
        }
    }

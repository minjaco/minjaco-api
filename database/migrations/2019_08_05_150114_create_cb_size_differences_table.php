<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCbSizeDifferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cb_size_differences', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('top_size')->nullable();
            $table->unsignedInteger('bottom_size')->nullable();
            $table->string('bottom_cut')->nullable();
            $table->string('pattern_code_one')->nullable();
            $table->string('pattern_code_two')->nullable();
            $table->string('pattern_code_three')->nullable();
            $table->unsignedInteger('top_sqrcm')->nullable();
            $table->unsignedInteger('bottom_front_sqrcm')->nullable();
            $table->unsignedInteger('bottom_back_sqrcm')->nullable();
            $table->unsignedInteger('bottom_total_sqrcm')->nullable();
            $table->unsignedInteger('top_bottom_total_sqrcm')->nullable();
            $table->unsignedDecimal('top_percent')->nullable();
            $table->unsignedDecimal('bottom_front_percent')->nullable();
            $table->unsignedDecimal('bottom_back_percent')->nullable();
            $table->unsignedDecimal('bottom_total_percent')->nullable();
            $table->unsignedDecimal('top_bottom_total_percent')->nullable();
            $table->unsignedInteger('total_crystals_as_pcs')->nullable();
            $table->string('total_pack_to_distribute')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cb_size_differences');
    }
}

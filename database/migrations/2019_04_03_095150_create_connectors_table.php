<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConnectorsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('connectors', function (Blueprint $table) {
            $table->integer('runid', true);
            $table->integer('con_id')->nullable();
            $table->string('con_code', 50)->nullable();
            $table->string('con_name', 100)->nullable();
            $table->string('con_old_name', 100)->nullable();
            $table->string('con_sku')->nullable();
            $table->string('con_pic', 50)->nullable();
            $table->string('con_type', 5)->nullable()->comment('number 3 digit 
1st = top connector
2nd = middle connector
3rd = bottom connector

ex. 
010 = middle connector only
101 = top and bottom connector
');
            $table->integer('con_status_info')->nullable()->default(1)->comment('0=do not use,1= use');
            $table->integer('con_status_bc')->nullable()->comment('0=do not use,1= use');
            $table->integer('con_status_db')->nullable()->comment('0=do not use,1= use');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('connectors');
    }
}

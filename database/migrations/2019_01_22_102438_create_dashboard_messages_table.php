<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDashboardMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'dashboard_messages',
            function (Blueprint $table) {
                $table->bigIncrements('id')->unsigned();
                $table->bigInteger('user_id')->index('user_id')->unsigned()->comment('Message was created by');
                $table->integer('to')->index('to')->default(0)->unsigned()->comment('Message was sent to');
                $table->string('group')->nullable()->comment('Which group to send');
                $table->tinyInteger('type')->nullable()->comment('1-Management, 2- Department');
                $table->tinyInteger('is_completed')->default(0)->comment('Status of message 1- Completed, 0- Uncompleted');
                $table->text('content')->comment('Message content');
                $table->timestamps();
                $table->softDeletes();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dashboard_messages');
    }
}

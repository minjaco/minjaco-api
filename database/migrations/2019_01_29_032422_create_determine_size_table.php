<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDetermineSizeTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('determine_size', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('order_id')->unsigned()->nullable();
            $table->bigInteger('customer_id')->unsigned()->nullable();
            $table->string('current_w', 20)->nullable();
            $table->string('expected_w', 10)->nullable();
            $table->string('height', 10)->nullable();
            $table->string('bra_cup_size')->nullable();
            $table->string('m_bra_cup_size')->nullable();
            $table->tinyInteger('top_style')->unsigned()->nullable();
            $table->string('top_size', 5)->nullable();
            $table->string('top_comment')->nullable();
            $table->tinyInteger('extra_padding')->unsigned()->nullable();
            $table->string('bottom_front_size_no', 5)->nullable();
            $table->string('bottom_front_letter', 50)->nullable();
            $table->string('bottom_front_comment')->nullable();
            $table->string('bottom_back_size_no', 5)->nullable();
            $table->string('bottom_back_letter', 50)->nullable();
            $table->string('bottom_back_comment')->nullable();
            $table->integer('cco_fabric')->unsigned()->nullable();
            $table->integer('con_top')->unsigned()->nullable();
            $table->integer('con_mid')->unsigned()->nullable();
            $table->integer('con_bot')->unsigned()->nullable();
            $table->string('waistline')->nullable();
            $table->string('gap_between_cups', 10)->nullable();
            $table->string('special_instruction')->nullable();
            $table->integer('implants')->unsigned()->nullable()->comment('0=No,1=Yes');
            $table->integer('ok_rm')->unsigned()->nullable()->comment('Ready Made 0=No,1=Yes');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('determine_size');
    }
}

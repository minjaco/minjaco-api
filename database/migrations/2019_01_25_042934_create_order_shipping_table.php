<?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Support\Facades\Schema;

    class CreateOrderShippingTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create(
                'order_shippings',
                function (Blueprint $table) {
                    $table->bigInteger('id')->unsigned();
                    $table->bigInteger('order_id')->unsigned()->index('order_id');
                    $table->string('first_name');
                    $table->string('last_name');
                    $table->string('company')->nullable();
                    $table->string('street_1');
                    $table->string('street_2')->nullable();
                    $table->string('city');
                    $table->string('zip');
                    $table->string('country');
                    $table->string('country_iso2');
                    $table->string('state')->nullable();
                    $table->string('phone');
                    $table->string('email');
                    $table->string('items_total');
                    $table->string('shipping_method');
                    $table->decimal('base_cost')->unsigned();
                    $table->decimal('cost_inc_tax')->unsigned();
                    $table->tinyInteger('shipping_zone_id')->unsigned();
                    $table->string('shipping_zone_name');

                    $table->timestamps();
                }
            );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('order_shippings');
        }
    }

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_products', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('order_id')->index('order_id')->comment('Big Commerce order id of product');
            $table->bigInteger('product_id')->comment('Big Commerce product id');
            $table->string('name');
            $table->string('sku');
            $table->decimal('total_inc_tax');
            $table->tinyInteger('quantity');
            $table->tinyInteger('is_refunded')->default(0);
            $table->tinyInteger('quantity_refunded')->default(0);
            $table->text('p_special_request')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_products');
    }
}

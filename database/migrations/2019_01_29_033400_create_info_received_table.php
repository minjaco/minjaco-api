<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInfoReceivedTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('info_received', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('order_id')->unsigned();
            $table->string('top_con')->nullable();
            $table->string('mid_con')->nullable();
            $table->string('bot_con')->nullable();
            $table->string('top_style')->nullable();
            $table->string('bot_style')->nullable();
            $table->string('bot_front_cov_level')->nullable();
            $table->string('bot_back_cov_level')->nullable();
            $table->string('weight_c', 100)->nullable();
            $table->string('weight_e', 100)->nullable();
            $table->string('tall', 100)->nullable();
            $table->string('bra_cup_size_c', 100)->nullable();
            $table->string('bra_cup_size_s', 100)->nullable();
            $table->string('how_com')->nullable();
            $table->string('anything_else')->nullable();
            $table->string('how_confi')->nullable();
            $table->string('implants')->nullable();
            $table->integer('top_id')->unsigned()->nullable();
            $table->integer('mid_id')->unsigned()->nullable();
            $table->integer('bot_id')->unsigned()->nullable();
            $table->string('comp_name')->nullable();
            $table->string('hear_ab', 100)->nullable();
            $table->timestamp('time_stamp')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('info_received');
    }
}

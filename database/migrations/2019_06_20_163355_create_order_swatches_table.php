<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderSwatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_swatches', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('order_id')->index('order_id');
            $table->integer('user_id')->comment('Added by');
            $table->string('path')->comment('Image path of swatches');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_swatches');
    }
}

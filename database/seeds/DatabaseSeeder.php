<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        //$this->call(SupplierTableSeeder::class);
        //$this->call(StatusTableSeeder::class);
        //$this->call(ConnectorsTableSeeder::class);
        //$this->call(FabricOldCodeTableSeeder::class);
        //$this->call(InfoReceivedTableSeeder::class);
        //$this->call(ItemCardTableSeeder::class);
        //$this->call(SewingRawMatTableSeeder::class);
        //$this->call(CountrySizeTableSeeder::class);
        //$this->call(CountryDsTableSeeder::class);
        //$this->call(DetermineSizeTableSeeder::class);
        $this->call(DepartmentSeeder::class);
    }
}

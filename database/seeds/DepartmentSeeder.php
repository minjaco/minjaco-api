<?php

use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $departments = [
            ['parent_id' => 0, 'name' => 'Management'],
            ['parent_id' => 0, 'name' => 'Customer Service'],
            ['parent_id' => 0, 'name' => 'HR'],
            ['parent_id' => 0, 'name' => 'Accounting'],
            ['parent_id' => 0, 'name' => 'Design'],
            ['parent_id' => 0, 'name' => 'Logistics'],
            ['parent_id' => 0, 'name' => 'Shipping'],
            ['parent_id' => 0, 'name' => 'Stock'],
            ['parent_id' => 0, 'name' => 'Purchase'],
            ['parent_id' => 0, 'name' => 'Production'],
            ['parent_id' => 0, 'name' => 'Sewing'],
            ['parent_id' => 0, 'name' => 'Decoration'],
        ];


        foreach ($departments as $dept) {
            app(\App\Models\Department::class)->create(
                $dept
            );
        }
    }
}

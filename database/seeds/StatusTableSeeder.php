<?php

use Illuminate\Database\Seeder;

class StatusTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        \DB::table('status')->truncate();

        \DB::table('status')->insert([
            0  =>
                [
                    'id'         => 1,
                    'name'       => 'Wating for sizing info',
                    'owner'      => 'CS',
                    'can_change' => 1,
                    'order_by'   => 1,
                ],
            1  =>
                [
                    'id'         => 2,
                    'name'       => 'Sizing info received',
                    'owner'      => 'CS',
                    'can_change' => 1,
                    'order_by'   => 2,
                ],
            2  =>
                [
                    'id'         => 3,
                    'name'       => 'Time toget in touch with clients',
                    'owner'      => 'CS',
                    'can_change' => 1,
                    'order_by'   => 3,
                ],
            3  =>
                [
                    'id'         => 4,
                    'name'       => 'Prepare for production',
                    'owner'      => 'Order',
                    'can_change' => 1,
                    'order_by'   => 8,
                ],
            4  =>
                [
                    'id'         => 5,
                    'name'       => 'Ready for production',
                    'owner'      => 'Order',
                    'can_change' => 1,
                    'order_by'   => 9,
                ],
            5  =>
                [
                    'id'         => 6,
                    'name'       => 'In Queue For Sewing',
                    'owner'      => 'Sewing',
                    'can_change' => 1,
                    'order_by'   => 10,
                ],
            6  =>
                [
                    'id'         => 7,
                    'name'       => 'Pattern',
                    'owner'      => 'Sewing',
                    'can_change' => 2,
                    'order_by'   => 11,
                ],
            7  =>
                [
                    'id'         => 8,
                    'name'       => 'Begin Sewn',
                    'owner'      => 'Sewing',
                    'can_change' => 1,
                    'order_by'   => 12,
                ],
            8  =>
                [
                    'id'         => 9,
                    'name'       => 'QC Sewing',
                    'owner'      => 'QC',
                    'can_change' => 1,
                    'order_by'   => 13,
                ],
            9  =>
                [
                    'id'         => 10,
                    'name'       => 'Failed QC - being fixed by sewing',
                    'owner'      => 'Sewing',
                    'can_change' => 1,
                    'order_by'   => 14,
                ],
            10 =>
                [
                    'id'         => 11,
                    'name'       => 'In queue for decoration',
                    'owner'      => 'Decorative',
                    'can_change' => 1,
                    'order_by'   => 15,
                ],
            11 =>
                [
                    'id'         => 12,
                    'name'       => 'Blocking',
                    'owner'      => 'Decorative',
                    'can_change' => 1,
                    'order_by'   => 16,
                ],
            12 =>
                [
                    'id'         => 13,
                    'name'       => 'Decoration 25%',
                    'owner'      => 'Decorative',
                    'can_change' => 1,
                    'order_by'   => 17,
                ],
            13 =>
                [
                    'id'         => 14,
                    'name'       => 'Decoration 50%',
                    'owner'      => 'Decorative',
                    'can_change' => 1,
                    'order_by'   => 18,
                ],
            14 =>
                [
                    'id'         => 15,
                    'name'       => 'Decoration 75%',
                    'owner'      => 'Decorative',
                    'can_change' => 1,
                    'order_by'   => 19,
                ],
            15 =>
                [
                    'id'         => 16,
                    'name'       => 'QC by Decoration Team',
                    'owner'      => 'Decorative',
                    'can_change' => 1,
                    'order_by'   => 21,
                ],
            16 =>
                [
                    'id'         => 17,
                    'name'       => 'Attaching connectors',
                    'owner'      => 'Sewing',
                    'can_change' => 2,
                    'order_by'   => 22,
                ],
            17 =>
                [
                    'id'         => 18,
                    'name'       => 'Final QC & Enter Ratings',
                    'owner'      => 'QC',
                    'can_change' => 1,
                    'order_by'   => 23,
                ],
            18 =>
                [
                    'id'         => 19,
                    'name'       => 'Failed QC - being fixed by decorative',
                    'owner'      => 'Decorative',
                    'can_change' => 1,
                    'order_by'   => 24,
                ],
            19 =>
                [
                    'id'         => 20,
                    'name'       => 'Take Photos and paste link',
                    'owner'      => 'Photographer',
                    'can_change' => 1,
                    'order_by'   => 25,
                ],
            20 =>
                [
                    'id'         => 21,
                    'name'       => 'Finish Goods',
                    'owner'      => 'Stock',
                    'can_change' => 1,
                    'order_by'   => 26,
                ],
            21 =>
                [
                    'id'         => 22,
                    'name'       => 'Awaiting packaging',
                    'owner'      => 'Shipping',
                    'can_change' => 1,
                    'order_by'   => 27,
                ],
            22 =>
                [
                    'id'         => 23,
                    'name'       => 'Awaiting pickup by DHL',
                    'owner'      => 'Shipping',
                    'can_change' => 1,
                    'order_by'   => 28,
                ],
            23 =>
                [
                    'id'         => 24,
                    'name'       => 'Complete',
                    'owner'      => '',
                    'can_change' => 1,
                    'order_by'   => 30,
                ],
            24 =>
                [
                    'id'         => 25,
                    'name'       => 'On hold',
                    'owner'      => '',
                    'can_change' => 1,
                    'order_by'   => 31,
                ],
            25 =>
                [
                    'id'         => 26,
                    'name'       => 'Cancelled',
                    'owner'      => '',
                    'can_change' => 1,
                    'order_by'   => 32,
                ],
            26 =>
                [
                    'id'         => 27,
                    'name'       => 'Prepare Material',
                    'owner'      => 'Stock',
                    'can_change' => 2,
                    'order_by'   => 33,
                ],
            27 =>
                [
                    'id'         => 28,
                    'name'       => 'Decoration 100%',
                    'owner'      => 'Decorative',
                    'can_change' => 1,
                    'order_by'   => 20,
                ],
            28 =>
                [
                    'id'         => 29,
                    'name'       => 'Awaiting Draft Suit feedback',
                    'owner'      => 'CS',
                    'can_change' => 1,
                    'order_by'   => 4,
                ],
            29 =>
                [
                    'id'         => 30,
                    'name'       => 'Draft Suit feedback received',
                    'owner'      => 'CS',
                    'can_change' => 1,
                    'order_by'   => 5,
                ],
            30 =>
                [
                    'id'         => 31,
                    'name'       => 'Awaiting picture for sizing',
                    'owner'      => 'CS',
                    'can_change' => 1,
                    'order_by'   => 6,
                ],
            31 =>
                [
                    'id'         => 32,
                    'name'       => 'Awaiting Confirm',
                    'owner'      => 'CS',
                    'can_change' => 1,
                    'order_by'   => 7,
                ],
            32 =>
                [
                    'id'         => 33,
                    'name'       => 'Shipped',
                    'owner'      => 'Shipping',
                    'can_change' => 1,
                    'order_by'   => 29,
                ],
            33 =>
                [
                    'id'         => 34,
                    'name'       => 'Awaiting approve refund',
                    'owner'      => 'MG',
                    'can_change' => 2,
                    'order_by'   => 34,
                ],
            34 =>
                [
                    'id'         => 35,
                    'name'       => 'Refunding',
                    'owner'      => 'AC',
                    'can_change' => 2,
                    'order_by'   => 35,
                ],
            35 =>
                [
                    'id'         => 36,
                    'name'       => 'Refunded',
                    'owner'      => 'AC',
                    'can_change' => 2,
                    'order_by'   => 36,
                ],
            36 =>
                [
                    'id'         => 37,
                    'name'       => 'Moved to Stock RM',
                    'owner'      => 'Stock',
                    'can_change' => 2,
                    'order_by'   => 37,
                ],
        ]);
    }
}

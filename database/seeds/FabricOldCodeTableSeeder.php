<?php

use Illuminate\Database\Seeder;

class FabricOldCodeTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        \DB::table('fabrics')->delete();

        \DB::table('fabrics')->insert([
            0  =>
                [
                    'fabric_id'       => 215,
                    'fabric_code'     => 'FAB-SPDEX-MAUVE-VI004-58',
                    'fabric_old_code' => 'PP04',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_e9611b23-1628-467e-8bc1-a947cfdcaf28_PP04-Lavender-mystique_500.jpg',
                    'fabric_name'     => 'Lavender mystique'
                ],
            1  =>
                [
                    'fabric_id'       => 186,
                    'fabric_code'     => 'FAB-SPDEX-01927-BK001-58',
                    'fabric_old_code' => 'BK01',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_e58b79df-5b4f-48a3-b061-0769afe56b89_BK01-Black-mystique_500.jpg',
                    'fabric_name'     => 'Black mystique'
                ],
            2  =>
                [
                    'fabric_id'       => 187,
                    'fabric_code'     => 'FAB-SPDEX-H1143-BK003-58',
                    'fabric_old_code' => 'BK03',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_e58b79df-5b4f-48a3-b061-0769afe56b89_BK03-Black-hologram_500.jpg',
                    'fabric_name'     => 'Black hologram',
                    
                ],
            3  =>
                [
                    'fabric_id'       => 188,
                    'fabric_code'     => 'FAB-SPDEX-10891-BK009-58',
                    'fabric_old_code' => 'BK09',
                    'fabric_status'   => 0,
                    'fabric_img'      => '',
                    'fabric_name'     => null,
                    
                ],
            4  =>
                [
                    'fabric_id'       => 189,
                    'fabric_code'     => 'FAB-SPDEX-04252-BK010-58',
                    'fabric_old_code' => 'BK10',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_e58b79df-5b4f-48a3-b061-0769afe56b89_BK10-Black-silver-hologram_500.jpg',
                    'fabric_name'     => 'Black silver hologram',
                    
                ],
            5  =>
                [
                    'fabric_id'       => 190,
                    'fabric_code'     => 'FAB-SPDEX-H1794-BK011-58',
                    'fabric_old_code' => 'BK11',
                    'fabric_status'   => 0,
                    'fabric_img'      => '',
                    'fabric_name'     => null,
                    
                ],
            6  =>
                [
                    'fabric_id'       => 420,
                    'fabric_code'     => 'FAB-SPDEX-000-BL001-58',
                    'fabric_old_code' => 'B01',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'B01-Royal-mini-dots-on-black.jpg',
                    'fabric_name'     => 'Royal mini dots on black',
                    
                ],
            7  =>
                [
                    'fabric_id'       => 182,
                    'fabric_code'     => 'FAB-SPDEX-05003-BL002-58',
                    'fabric_old_code' => 'B02',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_e58b79df-5b4f-48a3-b061-0769afe56b89_B02-Royal-blue-hologram_500.jpg',
                    'fabric_name'     => 'Royal blue hologram',
                    
                ],
            8  =>
                [
                    'fabric_id'       => 183,
                    'fabric_code'     => 'FAB-SPDEX-04353-BL004-58',
                    'fabric_old_code' => 'B04',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_e58b79df-5b4f-48a3-b061-0769afe56b89_B04-Sapphire-blue-mystique_500.jpg',
                    'fabric_name'     => 'Sapphire blue mystique',
                    
                ],
            9  =>
                [
                    'fabric_id'       => 184,
                    'fabric_code'     => 'FAB-SPDEX-12711-BL007-58',
                    'fabric_old_code' => 'B07',
                    'fabric_status'   => 0,
                    'fabric_img'      => '',
                    'fabric_name'     => null,
                    
                ],
            10 =>
                [
                    'fabric_id'       => 185,
                    'fabric_code'     => 'FAB-SPDEX-01918-BL008-58',
                    'fabric_old_code' => 'B08',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_e58b79df-5b4f-48a3-b061-0769afe56b89_B08-Royal-blue+mystique_500.jpg',
                    'fabric_name'     => 'Royal blue mystique',
                    
                ],
            11 =>
                [
                    'fabric_id'       => 202,
                    'fabric_code'     => 'FAB-SPDEX-CALAT-GD001-58',
                    'fabric_old_code' => 'GOLD01',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_14f31dc8-6cee-4361-beb3-027a57064091_GOLD01-Cafe-Latte_500.jpg',
                    'fabric_name'     => 'Cafe Latte',
                    
                ],
            12 =>
                [
                    'fabric_id'       => 203,
                    'fabric_code'     => 'FAB-SPDEX-04250-GD005-58',
                    'fabric_old_code' => 'GOLD05',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_e58b79df-5b4f-48a3-b061-0769afe56b89_Gold05-Gold-white-reflect_500.jpg',
                    'fabric_name'     => 'Gold white reflect',
                    
                ],
            13 =>
                [
                    'fabric_id'       => 191,
                    'fabric_code'     => 'FAB-SPDEX-04307-GR003-58',
                    'fabric_old_code' => 'G03',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_e58b79df-5b4f-48a3-b061-0769afe56b89_G03-Kelly-green-mystique_500.jpg',
                    'fabric_name'     => 'Kelly green mystique',
                    
                ],
            14 =>
                [
                    'fabric_id'       => 192,
                    'fabric_code'     => 'FAB-SPDEX-05000-GR004-58',
                    'fabric_old_code' => 'G04',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_e58b79df-5b4f-48a3-b061-0769afe56b89_G04-Light-green-Reflect_500.jpg',
                    'fabric_name'     => 'Light green Reflect',
                    
                ],
            15 =>
                [
                    'fabric_id'       => 193,
                    'fabric_code'     => 'FAB-SPDEX-10934-GR006-58',
                    'fabric_old_code' => 'G06',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_e58b79df-5b4f-48a3-b061-0769afe56b89_G06-Teal-hologram_500.jpg',
                    'fabric_name'     => 'Teal hologram',
                    
                ],
            16 =>
                [
                    'fabric_id'       => 194,
                    'fabric_code'     => 'FAB-SPDEX-H1150-GR007-58',
                    'fabric_old_code' => 'G07',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_e58b79df-5b4f-48a3-b061-0769afe56b89_G07-Dark-green-shatterred-glass-hologram_500.jpg',
                    'fabric_name'     => 'Dark green shatterred glass hologram',
                    
                ],
            17 =>
                [
                    'fabric_id'       => 195,
                    'fabric_code'     => 'FAB-SPDEX-07239-GR008-58',
                    'fabric_old_code' => 'G08',
                    'fabric_status'   => 0,
                    'fabric_img'      => '',
                    'fabric_name'     => null,
                    
                ],
            18 =>
                [
                    'fabric_id'       => 196,
                    'fabric_code'     => 'FAB-SPDEX-02327-GR012-58',
                    'fabric_old_code' => 'G12',
                    'fabric_status'   => 0,
                    'fabric_img'      => '',
                    'fabric_name'     => null,
                    
                ],
            19 =>
                [
                    'fabric_id'       => 197,
                    'fabric_code'     => 'FAB-SPDEX-10513-GR013-58',
                    'fabric_old_code' => 'G13',
                    'fabric_status'   => 0,
                    'fabric_img'      => '',
                    'fabric_name'     => null,
                    
                ],
            20 =>
                [
                    'fabric_id'       => 198,
                    'fabric_code'     => 'FAB-SPDEX-06925-GR015-58',
                    'fabric_old_code' => 'G15',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_e58b79df-5b4f-48a3-b061-0769afe56b89_G15-Seaweed-green-mystique_500.jpg',
                    'fabric_name'     => 'Seaweed green mystique',
                    
                ],
            21 =>
                [
                    'fabric_id'       => 199,
                    'fabric_code'     => 'FAB-SPDEX-12316-GR018-58',
                    'fabric_old_code' => 'G18',
                    'fabric_status'   => 0,
                    'fabric_img'      => '',
                    'fabric_name'     => null,
                    
                ],
            22 =>
                [
                    'fabric_id'       => 200,
                    'fabric_code'     => 'FAB-SPDEX-05017-GR020-58',
                    'fabric_old_code' => 'G20',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_e58b79df-5b4f-48a3-b061-0769afe56b89_G20-Mystique-Lime_Silver_500.jpg',
                    'fabric_name'     => 'Mystique Lime Silver',
                    
                ],
            23 =>
                [
                    'fabric_id'       => 201,
                    'fabric_code'     => 'FAB-SPDEX-H2409-GR026-58',
                    'fabric_old_code' => 'G26',
                    'fabric_status'   => 0,
                    'fabric_img'      => '',
                    'fabric_name'     => null,
                    
                ],
            24 =>
                [
                    'fabric_id'       => 204,
                    'fabric_code'     => 'FAB-SPDEX-04323-OR002-58',
                    'fabric_old_code' => 'OR02',
                    'fabric_status'   => 0,
                    'fabric_img'      => '',
                    'fabric_name'     => null,
                    
                ],
            25 =>
                [
                    'fabric_id'       => 205,
                    'fabric_code'     => 'FAB-SPDEX-APCOT-OR007-58',
                    'fabric_old_code' => 'OR07',
                    'fabric_status'   => 0,
                    'fabric_img'      => '',
                    'fabric_name'     => null,
                    
                ],
            26 =>
                [
                    'fabric_id'       => 206,
                    'fabric_code'     => 'FAB-SPDEX-BAPNK-PI001-58',
                    'fabric_old_code' => 'P01',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_e58b79df-5b4f-48a3-b061-0769afe56b89_P01-Baby-pink-mystique_500.jpg',
                    'fabric_name'     => 'Baby pink mystique',
                    
                ],
            27 =>
                [
                    'fabric_id'       => 207,
                    'fabric_code'     => 'FAB-SPDEX-H1248-PI008-58',
                    'fabric_old_code' => 'P08',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_14f31dc8-6cee-4361-beb3-027a57064091_P08-Fuchsia-hologram_500.jpg',
                    'fabric_name'     => 'Fuchsia hologram',
                    
                ],
            28 =>
                [
                    'fabric_id'       => 208,
                    'fabric_code'     => 'FAB-SPDEX-01466-PI023-58',
                    'fabric_old_code' => 'P23',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_14f31dc8-6cee-4361-beb3-027a57064091_P23-Pink-hologram_500.jpg',
                    'fabric_name'     => 'Pink hologram',
                    
                ],
            29 =>
                [
                    'fabric_id'       => 209,
                    'fabric_code'     => 'FAB-SPDEX-12317-PI024-58',
                    'fabric_old_code' => 'P24',
                    'fabric_status'   => 0,
                    'fabric_img'      => '',
                    'fabric_name'     => null,
                    
                ],
            30 =>
                [
                    'fabric_id'       => 210,
                    'fabric_code'     => 'FAB-SPDEX-06096-PI025-58',
                    'fabric_old_code' => 'P25',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_14f31dc8-6cee-4361-beb3-027a57064091_P25-Fuchsia-mystique_500.jpg',
                    'fabric_name'     => 'Fuchsia mystique',
                    
                ],
            31 =>
                [
                    'fabric_id'       => 211,
                    'fabric_code'     => 'FAB-SPDEX-05863-PI027-58',
                    'fabric_old_code' => 'P27',
                    'fabric_status'   => 0,
                    'fabric_img'      => '',
                    'fabric_name'     => null,
                    
                ],
            32 =>
                [
                    'fabric_id'       => 418,
                    'fabric_code'     => 'FAB-SPDEX-12363-PI028-58',
                    'fabric_old_code' => 'P28',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_14f31dc8-6cee-4361-beb3-027a57064091_P28-Magenta-mystique_500.jpg',
                    'fabric_name'     => 'Magenta mystique',
                    
                ],
            33 =>
                [
                    'fabric_id'       => 212,
                    'fabric_code'     => 'FAB-SPDEX-14308-PI031-58',
                    'fabric_old_code' => 'P31',
                    'fabric_status'   => 0,
                    'fabric_img'      => '',
                    'fabric_name'     => null,
                    
                ],
            34 =>
                [
                    'fabric_id'       => 223,
                    'fabric_code'     => 'FAB-SPDEX-CHRRY-RE001-58',
                    'fabric_old_code' => 'R01',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_e9611b23-1628-467e-8bc1-a947cfdcaf28_R01-Raspberry-mystique_500.jpg',
                    'fabric_name'     => 'Raspberry mystique',
                    
                ],
            35 =>
                [
                    'fabric_id'       => 224,
                    'fabric_code'     => 'FAB-SPDEX-01916-RE002-58',
                    'fabric_old_code' => 'R02',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_e9611b23-1628-467e-8bc1-a947cfdcaf28_R02-Red-mystique_500.jpg',
                    'fabric_name'     => 'Red mystique',
                    
                ],
            36 =>
                [
                    'fabric_id'       => 225,
                    'fabric_code'     => 'FAB-SPDEX-05896-RE003-58',
                    'fabric_old_code' => 'R03',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_e9611b23-1628-467e-8bc1-a947cfdcaf28_R03-Burgundy-mystique_500.jpg',
                    'fabric_name'     => 'Burgundy mystique',
                    
                ],
            37 =>
                [
                    'fabric_id'       => 226,
                    'fabric_code'     => 'FAB-SPDEX-05009-RE004-58',
                    'fabric_old_code' => 'R04',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_e9611b23-1628-467e-8bc1-a947cfdcaf28_R04-Red-hologram_500.jpg',
                    'fabric_name'     => 'Red hologram',
                    
                ],
            38 =>
                [
                    'fabric_id'       => 227,
                    'fabric_code'     => 'FAB-SPDEX-08612-RE017-58',
                    'fabric_old_code' => 'R17',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_e9611b23-1628-467e-8bc1-a947cfdcaf28_R17-Raspberry-hologram_500.jpg',
                    'fabric_name'     => 'Raspberry hologram',
                    
                ],
            39 =>
                [
                    'fabric_id'       => 228,
                    'fabric_code'     => 'FAB-SPDEX-MTHWI-TQ001-58',
                    'fabric_old_code' => 'TQ01',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_e9611b23-1628-467e-8bc1-a947cfdcaf28_TQ01-Hawaiian-Mint_500.jpg',
                    'fabric_name'     => 'Hawaiian Mint',
                    
                ],
            40 =>
                [
                    'fabric_id'       => 229,
                    'fabric_code'     => 'FAB-SPDEX-05929-TQ002-58',
                    'fabric_old_code' => 'TQ02',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_e9611b23-1628-467e-8bc1-a947cfdcaf28_TQ02-aqua-blue-spandex_500.jpg',
                    'fabric_name'     => 'Aqua blue spandex',
                    
                ],
            41 =>
                [
                    'fabric_id'       => 230,
                    'fabric_code'     => 'FAB-SPDEX-H1570-TQ003-58',
                    'fabric_old_code' => 'TQ03',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_e9611b23-1628-467e-8bc1-a947cfdcaf28_TQ03-Aqua-Reflect_500.jpg',
                    'fabric_name'     => 'Aqua Reflect',
                    
                ],
            42 =>
                [
                    'fabric_id'       => 231,
                    'fabric_code'     => 'FAB-SPDEX-09164-TQ005-58',
                    'fabric_old_code' => 'TQ05',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_e9611b23-1628-467e-8bc1-a947cfdcaf28_TQ05-baby-blue-spandex_500.jpg',
                    'fabric_name'     => 'Baby blue spandex',
                    
                ],
            43 =>
                [
                    'fabric_id'       => 232,
                    'fabric_code'     => 'FAB-SPDEX-06043-TQ009-58',
                    'fabric_old_code' => 'TQ09',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_e9611b23-1628-467e-8bc1-a947cfdcaf28_TQ09-Matte-mint_500.jpg',
                    'fabric_name'     => 'Matte mint',
                    
                ],
            44 =>
                [
                    'fabric_id'       => 233,
                    'fabric_code'     => 'FAB-SPDEX-12319-TQ012-58',
                    'fabric_old_code' => 'TQ12',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_33590d62-026c-43c0-b8f5-fae35dcf0a7d_TQ12-reflective-aqua-blue-spendex_500.jpg',
                    'fabric_name'     => 'Reflective aqua blue spendex',
                    
                ],
            45 =>
                [
                    'fabric_id'       => 213,
                    'fabric_code'     => 'FAB-SPDEX-04325-VI001-58',
                    'fabric_old_code' => 'PP01',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_e9611b23-1628-467e-8bc1-a947cfdcaf28_PP01-Royal-purple-mystique_500.jpg',
                    'fabric_name'     => 'Royal purple mystique',
                    
                ],
            46 =>
                [
                    'fabric_id'       => 214,
                    'fabric_code'     => 'FAB-SPDEX-LVDER-VI003-58',
                    'fabric_old_code' => 'PP03',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_e9611b23-1628-467e-8bc1-a947cfdcaf28_PP03-Lilac-mystique_500.jpg',
                    'fabric_name'     => 'Lilac mystique',
                    
                ],
            47 =>
                [
                    'fabric_id'       => 216,
                    'fabric_code'     => 'FAB-SPDEX-04306-VI007-58',
                    'fabric_old_code' => 'PP07',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_e9611b23-1628-467e-8bc1-a947cfdcaf28_PP07-Fuchsia_pueple-mystique_500.jpg',
                    'fabric_name'     => 'Fuchsia pueple mystique',
                    
                ],
            48 =>
                [
                    'fabric_id'       => 217,
                    'fabric_code'     => 'FAB-SPDEX-GRAPE-VI008-58',
                    'fabric_old_code' => 'PP08',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_e9611b23-1628-467e-8bc1-a947cfdcaf28_PP08-purple-blue-spandex_500.jpg',
                    'fabric_name'     => 'Purple blue spandex',
                    
                ],
            49 =>
                [
                    'fabric_id'       => 218,
                    'fabric_code'     => 'FAB-SPDEX-MLBRY-VI013-58',
                    'fabric_old_code' => 'PP13',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_e9611b23-1628-467e-8bc1-a947cfdcaf28_PP13-Mulberry-purple-mystique_500.jpg',
                    'fabric_name'     => 'Mulberry purple mystique',
                    
                ],
            50 =>
                [
                    'fabric_id'       => 419,
                    'fabric_code'     => 'FAB-SPDEX-GSPUR-VI015-58',
                    'fabric_old_code' => 'PP15',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_e9611b23-1628-467e-8bc1-a947cfdcaf28_PP15-Purple-hologram_500.jpg',
                    'fabric_name'     => 'Purple hologram',
                    
                ],
            51 =>
                [
                    'fabric_id'       => 219,
                    'fabric_code'     => 'FAB-SPDEX-13560-VI017-58',
                    'fabric_old_code' => 'PP17',
                    'fabric_status'   => 0,
                    'fabric_img'      => '',
                    'fabric_name'     => null,
                    
                ],
            52 =>
                [
                    'fabric_id'       => 220,
                    'fabric_code'     => 'FAB-SPDEX-00014-VI019-58',
                    'fabric_old_code' => 'PP19',
                    'fabric_status'   => 0,
                    'fabric_img'      => '',
                    'fabric_name'     => null,
                    
                ],
            53 =>
                [
                    'fabric_id'       => 221,
                    'fabric_code'     => 'FAB-SPDEX-03823-VI020-58',
                    'fabric_old_code' => 'PP20',
                    'fabric_status'   => 0,
                    'fabric_img'      => '',
                    'fabric_name'     => null,
                    
                ],
            54 =>
                [
                    'fabric_id'       => 222,
                    'fabric_code'     => 'FAB-SPDEX-RYPRP-VI021-58',
                    'fabric_old_code' => 'PP21',
                    'fabric_status'   => 0,
                    'fabric_img'      => '',
                    'fabric_name'     => null,
                    
                ],
            55 =>
                [
                    'fabric_id'       => 241,
                    'fabric_code'     => 'FAB-SPDEX-03406-WH001-58',
                    'fabric_old_code' => 'W01',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_33590d62-026c-43c0-b8f5-fae35dcf0a7d_W01-White-silver-mystique_500.jpg',
                    'fabric_name'     => 'White silver mystique',
                    
                ],
            56 =>
                [
                    'fabric_id'       => 242,
                    'fabric_code'     => 'FAB-SPDEX-01847-WH002-58',
                    'fabric_old_code' => 'W02',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_33590d62-026c-43c0-b8f5-fae35dcf0a7d_W02-white-hologram-spandex_500.jpg',
                    'fabric_name'     => 'White hologram spandex',
                    
                ],
            57 =>
                [
                    'fabric_id'       => 243,
                    'fabric_code'     => 'FAB-SPDEX-H1254-WH005-58',
                    'fabric_old_code' => 'W05',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_33590d62-026c-43c0-b8f5-fae35dcf0a7d_W05-Silver-hologram_500.jpg',
                    'fabric_name'     => 'Silver hologram',
                    
                ],
            58 =>
                [
                    'fabric_id'       => 244,
                    'fabric_code'     => 'FAB-SPDEX-04305-YL002-58',
                    'fabric_old_code' => 'Y02',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_33590d62-026c-43c0-b8f5-fae35dcf0a7d_Y02-yellow-spandex_500.jpg',
                    'fabric_name'     => 'Yellow spandex',
                    
                ],
            59 =>
                [
                    'fabric_id'       => 234,
                    'fabric_code'     => 'FAB-STSEQ-11090-SQ011-58',
                    'fabric_old_code' => 'SQ11',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_beb4784d-7910-4165-976d-43f486e572b3_SQ11-aquablue-sequin-fabric_500.jpg',
                    'fabric_name'     => 'Aquablue sequin fabric',
                    
                ],
            60 =>
                [
                    'fabric_id'       => 235,
                    'fabric_code'     => 'FAB-STSEQ-11088-SQ017-58',
                    'fabric_old_code' => 'SQ17',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_beb4784d-7910-4165-976d-43f486e572b3_SQ17_500.jpg',
                    'fabric_name'     => 'Royal blue holo sequin',
                    
                ],
            61 =>
                [
                    'fabric_id'       => 236,
                    'fabric_code'     => 'FAB-STSEQ-10033-SQ019-58',
                    'fabric_old_code' => 'SQ19',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_beb4784d-7910-4165-976d-43f486e572b3_SQ19-lavender-sequin-fabric_500.jpg',
                    'fabric_name'     => 'Lavender sequin fabric',
                    
                ],
            62 =>
                [
                    'fabric_id'       => 237,
                    'fabric_code'     => 'FAB-STSEQ-12335-SQ020-58',
                    'fabric_old_code' => 'SQ20',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_beb4784d-7910-4165-976d-43f486e572b3_SQ20-ivory-sequin-fabric_500.jpg',
                    'fabric_name'     => 'Ivory sequin fabric',
                    
                ],
            63 =>
                [
                    'fabric_id'       => 238,
                    'fabric_code'     => 'FAB-STSEQ-11087-SQ021-58',
                    'fabric_old_code' => 'SQ21',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_beb4784d-7910-4165-976d-43f486e572b3_SQ21-red-hologram-sequins_500.jpg',
                    'fabric_name'     => 'Red hologram sequins',
                    
                ],
            64 =>
                [
                    'fabric_id'       => 239,
                    'fabric_code'     => 'FAB-STSEQ-11989-SQ026-50',
                    'fabric_old_code' => 'SQ26',
                    'fabric_status'   => 0,
                    'fabric_img'      => '',
                    'fabric_name'     => null,
                    
                ],
            65 =>
                [
                    'fabric_id'       => 715,
                    'fabric_code'     => 'FAB-STSEQ-12014-SQ027-58',
                    'fabric_old_code' => 'SQ27',
                    'fabric_status'   => 0,
                    'fabric_img'      => '',
                    'fabric_name'     => null,
                    
                ],
            66 =>
                [
                    'fabric_id'       => 240,
                    'fabric_code'     => 'FAB-STSEQ-07751-SQ030-60',
                    'fabric_old_code' => 'SQ30',
                    'fabric_status'   => 0,
                    'fabric_img'      => '',
                    'fabric_name'     => null,
                    
                ],
            67 =>
                [
                    'fabric_id'       => 714,
                    'fabric_code'     => 'FAB-STSEQ-06093-SQ034-58',
                    'fabric_old_code' => 'SQ34',
                    'fabric_status'   => 0,
                    'fabric_img'      => '',
                    'fabric_name'     => null,
                    
                ],
            68 =>
                [
                    'fabric_id'       => 716,
                    'fabric_code'     => 'FAB-STSEQ-11100-SQ028-58',
                    'fabric_old_code' => 'SQ28',
                    'fabric_status'   => 0,
                    'fabric_img'      => '',
                    'fabric_name'     => null,
                    
                ],
            69 =>
                [
                    'fabric_id'       => 717,
                    'fabric_code'     => 'FAB-STSEQ-07749-SQ029-58',
                    'fabric_old_code' => 'SQ29',
                    'fabric_status'   => 0,
                    'fabric_img'      => '',
                    'fabric_name'     => null,
                    
                ],
            70 =>
                [
                    'fabric_id'       => 751,
                    'fabric_code'     => 'FAB-SPDEX-H1194-GD006-58',
                    'fabric_old_code' => 'GOLD06',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_e58b79df-5b4f-48a3-b061-0769afe56b89_GOLD06-Copper-mini-dots-on-black_500.jpg',
                    'fabric_name'     => 'Copper mini dots on black',
                    
                ],
            71 =>
                [
                    'fabric_id'       => 623,
                    'fabric_code'     => 'FAB-STSEQ-04744-SQ035-58',
                    'fabric_old_code' => 'SQ35',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'SQ35-aqua-blue-sequin-fabric_xxx.jpg',
                    'fabric_name'     => 'Aqua blue sequin fabric',
                    
                ],
            72 =>
                [
                    'fabric_id'       => 624,
                    'fabric_code'     => 'FAB-STSEQ-04087-SQ036-58',
                    'fabric_old_code' => 'SQ36',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_4c8fc24c-d837-4a24-9dad-15cf01619833_SQ36-pink-fuchsia-sequin-fabric_xxx.jpg',
                    'fabric_name'     => 'Pink fuchsia sequin fabric',
                    
                ],
            73 =>
                [
                    'fabric_id'       => 625,
                    'fabric_code'     => 'FAB-STSEQ-05074-SQ037-58',
                    'fabric_old_code' => 'SQ37',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_4c8fc24c-d837-4a24-9dad-15cf01619833_SQ37-red-sequin-fabric_xxx.jpg',
                    'fabric_name'     => 'Red sequin fabric',
                    
                ],
            74 =>
                [
                    'fabric_id'       => 626,
                    'fabric_code'     => 'FAB-STSEQ-04086-SQ038-58',
                    'fabric_old_code' => 'SQ38',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_4c8fc24c-d837-4a24-9dad-15cf01619833_SQ38-black-sequin-fabric_xxx.jpg',
                    'fabric_name'     => 'Black sequin fabric',
                    
                ],
            75 =>
                [
                    'fabric_id'       => 627,
                    'fabric_code'     => 'FAB-STSEQ-02890-SQ039-58',
                    'fabric_old_code' => 'SQ39',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_4c8fc24c-d837-4a24-9dad-15cf01619833_SQ39-royal-blue-sequin-fabric_xxx.jpg',
                    'fabric_name'     => 'Royal blue sequin fabric',
                    
                ],
            76 =>
                [
                    'fabric_id'       => 628,
                    'fabric_code'     => 'FAB-STSEQ-06366-SQ040-58',
                    'fabric_old_code' => 'SQ40',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_4c8fc24c-d837-4a24-9dad-15cf01619833_SQ40-green-sequin-fabric_xxx.jpg',
                    'fabric_name'     => 'Green sequin fabric',
                    
                ],
            77 =>
                [
                    'fabric_id'       => 629,
                    'fabric_code'     => 'FAB-STSEQ-04554-SQ041-58',
                    'fabric_old_code' => 'SQ41',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_4c8fc24c-d837-4a24-9dad-15cf01619833_SQ41-emerald-sequin-fabric_xxx.jpg',
                    'fabric_name'     => 'Emerald sequin fabric',
                    
                ],
            78 =>
                [
                    'fabric_id'       => 684,
                    'fabric_code'     => 'FAB-SPDEX-000-GR002-58',
                    'fabric_old_code' => 'G02',
                    'fabric_status'   => 1,
                    'fabric_img'      => 'app_images_resizable_e58b79df-5b4f-48a3-b061-0769afe56b89_G02-Matte-mint_green_xxx.jpg',
                    'fabric_name'     => 'Matte mint green',
                    
                ],
            79 =>
                [
                    'fabric_id'       => 685,
                    'fabric_code'     => 'FAB-SPDEX-0518-GR27-00',
                    'fabric_old_code' => 'G27',
                    'fabric_status'   => 0,
                    'fabric_img'      => '',
                    'fabric_name'     => null,
                    
                ],
            80 =>
                [
                    'fabric_id'       => 686,
                    'fabric_code'     => 'FAB-SPDEX-0512-RD23-00',
                    'fabric_old_code' => 'R23',
                    'fabric_status'   => 0,
                    'fabric_img'      => '',
                    'fabric_name'     => null,
                    
                ],
            81 =>
                [
                    'fabric_id'       => 687,
                    'fabric_code'     => 'FAB-SPDEX-0311-YL06-00',
                    'fabric_old_code' => 'Y06',
                    'fabric_status'   => 0,
                    'fabric_img'      => '',
                    'fabric_name'     => null,
                    
                ],
            82 =>
                [
                    'fabric_id'       => 688,
                    'fabric_code'     => 'FAB-SPDEX-0617-TQ18-00',
                    'fabric_old_code' => 'TQ18',
                    'fabric_status'   => 0,
                    'fabric_img'      => '',
                    'fabric_name'     => null,
                    
                ],
            83 =>
                [
                    'fabric_id'       => 689,
                    'fabric_code'     => 'FAB-SPDEX-0319-BK12-0',
                    'fabric_old_code' => 'BK12',
                    'fabric_status'   => 0,
                    'fabric_img'      => '',
                    'fabric_name'     => null,
                    
                ],
            84 =>
                [
                    'fabric_id'       => 690,
                    'fabric_code'     => 'FAB-SPDEX-410-WH08-00',
                    'fabric_old_code' => 'W08',
                    'fabric_status'   => 0,
                    'fabric_img'      => '',
                    'fabric_name'     => null,
                    
                ],
            85 =>
                [
                    'fabric_id'       => 1012,
                    'fabric_code'     => 'FAB-SPDEX-MTL001-G025-61',
                    'fabric_old_code' => 'G25',
                    'fabric_status'   => 0,
                    'fabric_img'      => '',
                    'fabric_name'     => null,
                    
                ],
            86 =>
                [
                    'fabric_id'       => 1033,
                    'fabric_code'     => 'FAB-STSEQ-ZSAZSA-SQ042-00',
                    'fabric_old_code' => 'SQ42',
                    'fabric_status'   => 0,
                    'fabric_img'      => '',
                    'fabric_name'     => null,
                    
                ],
        ]);
    }
}

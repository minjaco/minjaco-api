<?php

use Illuminate\Database\Seeder;

class CountryDsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        \DB::table('countries')->delete();

        \DB::table('countries')->insert([
            0 =>
            [
                'id' => 1,
                'country_name' => 'United States',
                'country_iso2' => 'US',
                'country_iso3' => 'USA',
                'country_ds' => 700,
                'country_ds_full' => 800,
            ],
            1 =>
            [
                'id' => 2,
                'country_name' => 'Australia',
                'country_iso2' => 'AU',
                'country_iso3' => 'AUS',
                'country_ds' => 700,
                'country_ds_full' => 810,
            ],
            2 =>
            [
                'id' => 3,
                'country_name' => 'United Kingdom',
                'country_iso2' => 'GB',
                'country_iso3' => 'GBR',
                'country_ds' => 50,
                'country_ds_full' => 186,
            ],
            3 =>
            [
                'id' => 4,
                'country_name' => 'Canada',
                'country_iso2' => 'CA',
                'country_iso3' => 'CAN',
                'country_ds' => 25,
                'country_ds_full' => 15,
            ],
            4 =>
            [
                'id' => 5,
                'country_name' => 'China',
                'country_iso2' => 'CN',
                'country_iso3' => 'CHN',
                'country_ds' => 25,
                'country_ds_full' => 8,
            ],
            5 =>
            [
                'id' => 6,
                'country_name' => 'Germany',
                'country_iso2' => 'DE',
                'country_iso3' => 'DEU',
                'country_ds' => 50,
                'country_ds_full' => 186,
            ],
            6 =>
            [
                'id' => 7,
                'country_name' => 'France',
                'country_iso2' => 'FR',
                'country_iso3' => 'FRA',
                'country_ds' => 50,
                'country_ds_full' => 186,
            ],
            7 =>
            [
                'id' => 8,
                'country_name' => 'New Zealand',
                'country_iso2' => 'NZ',
                'country_iso3' => 'NZL',
                'country_ds' => 50,
                'country_ds_full' => 295,
            ],
            8 =>
            [
                'id' => 9,
                'country_name' => 'Ireland',
                'country_iso2' => 'IE',
                'country_iso3' => 'IRL',
                'country_ds' => 50,
                'country_ds_full' => 186,
            ],
            9 =>
            [
                'id' => 10,
                'country_name' => 'Sweden',
                'country_iso2' => 'SE',
                'country_iso3' => 'SWE',
                'country_ds' => 50,
                'country_ds_full' => 186,
            ],
            10 =>
            [
                'id' => 11,
                'country_name' => 'Italy',
                'country_iso2' => 'IT',
                'country_iso3' => 'ITA',
                'country_ds' => 50,
                'country_ds_full' => 186,
            ],
            11 =>
            [
                'id' => 12,
                'country_name' => 'Singapore',
                'country_iso2' => 'SG',
                'country_iso3' => 'SGP',
                'country_ds' => 50,
                'country_ds_full' => 305,
            ],
            12 =>
            [
                'id' => 13,
                'country_name' => 'Switzerland',
                'country_iso2' => 'CH',
                'country_iso3' => 'CHE',
                'country_ds' => 25,
                'country_ds_full' => 5,
            ],
            13 =>
            [
                'id' => 14,
                'country_name' => 'Japan',
                'country_iso2' => 'JP',
                'country_iso3' => 'JPN',
                'country_ds' => 25,
                'country_ds_full' => 90,
            ],
            14 =>
            [
                'id' => 15,
                'country_name' => 'Philippines',
                'country_iso2' => 'PH',
                'country_iso3' => 'PHL',
                'country_ds' => 25,
                'country_ds_full' => 192,
            ],
            15 =>
            [
                'id' => 16,
                'country_name' => 'South Africa',
                'country_iso2' => 'ZA',
                'country_iso3' => 'ZAF',
                'country_ds' => 25,
                'country_ds_full' => 25,
            ],
            16 =>
            [
                'id' => 17,
                'country_name' => 'Romania',
                'country_iso2' => 'RO',
                'country_iso3' => 'ROM',
                'country_ds' => 50,
                'country_ds_full' => 186,
            ],
            17 =>
            [
                'id' => 18,
                'country_name' => 'Hong Kong',
                'country_iso2' => 'HK',
                'country_iso3' => 'HKG',
                'country_ds' => 25,
                'country_ds_full' => 25,
            ],
            18 =>
            [
                'id' => 19,
                'country_name' => 'Norway',
                'country_iso2' => 'NO',
                'country_iso3' => 'NOR',
                'country_ds' => 49,
                'country_ds_full' => 45,
            ],
            19 =>
            [
                'id' => 20,
                'country_name' => 'India',
                'country_iso2' => 'IN',
                'country_iso3' => 'IND',
                'country_ds' => 50,
                'country_ds_full' => 150,
            ],
            20 =>
            [
                'id' => 21,
                'country_name' => 'Netherlands',
                'country_iso2' => 'NL',
                'country_iso3' => 'NLD',
                'country_ds' => 50,
                'country_ds_full' => 186,
            ],
            21 =>
            [
                'id' => 22,
                'country_name' => 'United Arab Emirates',
                'country_iso2' => 'AE',
                'country_iso3' => 'ARE',
                'country_ds' => 25,
                'country_ds_full' => 25,
            ],
            22 =>
            [
                'id' => 23,
                'country_name' => 'Finland',
                'country_iso2' => 'FI',
                'country_iso3' => 'FIN',
                'country_ds' => 50,
                'country_ds_full' => 186,
            ],
            23 =>
            [
                'id' => 24,
                'country_name' => 'Malaysia',
                'country_iso2' => 'MY',
                'country_iso3' => 'MYS',
                'country_ds' => 50,
                'country_ds_full' => 128,
            ],
            24 =>
            [
                'id' => 25,
                'country_name' => 'French Polynesia',
                'country_iso2' => 'PF',
                'country_iso3' => 'PYF',
                'country_ds' => 19,
                'country_ds_full' => 19,
            ],
            25 =>
            [
                'id' => 26,
                'country_name' => 'Malta',
                'country_iso2' => 'MT',
                'country_iso3' => 'MLT',
                'country_ds' => 50,
                'country_ds_full' => 186,
            ],
            26 =>
            [
                'id' => 27,
                'country_name' => 'Austria',
                'country_iso2' => 'AT',
                'country_iso3' => 'AUT',
                'country_ds' => 50,
                'country_ds_full' => 186,
            ],
            27 =>
            [
                'id' => 28,
                'country_name' => 'Spain',
                'country_iso2' => 'ES',
                'country_iso3' => 'ESP',
                'country_ds' => 50,
                'country_ds_full' => 186,
            ],
            28 =>
            [
                'id' => 29,
                'country_name' => 'Korea, Republic of',
                'country_iso2' => 'KR',
                'country_iso3' => 'KOR',
                'country_ds' => 50,
                'country_ds_full' => 110,
            ],
            29 =>
            [
                'id' => 30,
                'country_name' => 'Denmark',
                'country_iso2' => 'DK',
                'country_iso3' => 'DNK',
                'country_ds' => 50,
                'country_ds_full' => 186,
            ],
            30 =>
            [
                'id' => 31,
                'country_name' => 'Thailand',
                'country_iso2' => 'TH',
                'country_iso3' => 'THA',
                'country_ds' => 10000,
                'country_ds_full' => 10000,
            ],
            31 =>
            [
                'id' => 32,
                'country_name' => 'New Caledonia',
                'country_iso2' => 'NC',
                'country_iso3' => 'NCL',
                'country_ds' => 25,
                'country_ds_full' => 25,
            ],
            32 =>
            [
                'id' => 33,
                'country_name' => 'Trinidad and Tobago',
                'country_iso2' => 'TT',
                'country_iso3' => 'TTO',
                'country_ds' => 25,
                'country_ds_full' => 25,
            ],
            33 =>
            [
                'id' => 34,
                'country_name' => 'Puerto Rico',
                'country_iso2' => 'PR',
                'country_iso3' => 'PRI',
                'country_ds' => 25,
                'country_ds_full' => 25,
            ],
            34 =>
            [
                'id' => 35,
                'country_name' => 'Reunion',
                'country_iso2' => 'RE',
                'country_iso3' => 'REU',
                'country_ds' => 25,
                'country_ds_full' => 25,
            ],
            35 =>
            [
                'id' => 36,
                'country_name' => 'Lithuania',
                'country_iso2' => 'LT',
                'country_iso3' => 'LTU',
                'country_ds' => 50,
                'country_ds_full' => 186,
            ],
            36 =>
            [
                'id' => 37,
                'country_name' => 'Belgium',
                'country_iso2' => 'BE',
                'country_iso3' => 'BEL',
                'country_ds' => 50,
                'country_ds_full' => 186,
            ],
            37 =>
            [
                'id' => 38,
                'country_name' => 'Israel',
                'country_iso2' => 'IL',
                'country_iso3' => 'ISR',
                'country_ds' => 50,
                'country_ds_full' => 100,
            ],
            38 =>
            [
                'id' => 39,
                'country_name' => 'Portugal',
                'country_iso2' => 'PT',
                'country_iso3' => 'PRT',
                'country_ds' => 50,
                'country_ds_full' => 186,
            ],
            39 =>
            [
                'id' => 40,
                'country_name' => 'Taiwan',
                'country_iso2' => 'TW',
                'country_iso3' => 'TWN',
                'country_ds' => 50,
                'country_ds_full' => 93,
            ],
            40 =>
            [
                'id' => 41,
                'country_name' => 'Guam',
                'country_iso2' => 'GU',
                'country_iso3' => 'GUM',
                'country_ds' => 25,
                'country_ds_full' => 25,
            ],
            41 =>
            [
                'id' => 42,
                'country_name' => 'Aruba',
                'country_iso2' => 'AW',
                'country_iso3' => 'ABW',
                'country_ds' => 25,
                'country_ds_full' => 25,
            ],
            42 =>
            [
                'id' => 43,
                'country_name' => 'Brunei Darussalam',
                'country_iso2' => 'BN',
                'country_iso3' => 'BRN',
                'country_ds' => 50,
                'country_ds_full' => 295,
            ],
            43 =>
            [
                'id' => 44,
                'country_name' => 'Bulgaria',
                'country_iso2' => 'BG',
                'country_iso3' => 'BGR',
                'country_ds' => 50,
                'country_ds_full' => 186,
            ],
            44 =>
            [
                'id' => 45,
                'country_name' => 'Brazil',
                'country_iso2' => 'BR',
                'country_iso3' => 'BRA',
                'country_ds' => 25,
                'country_ds_full' => 50,
            ],
            45 =>
            [
                'id' => 46,
                'country_name' => 'Georgia',
                'country_iso2' => 'GE',
                'country_iso3' => 'GEO',
                'country_ds' => 50,
                'country_ds_full' => 217,
            ],
            46 =>
            [
                'id' => 47,
                'country_name' => 'Antigua and Barbuda',
                'country_iso2' => 'AG',
                'country_iso3' => 'ATG',
                'country_ds' => 25,
                'country_ds_full' => 25,
            ],
            47 =>
            [
                'id' => 48,
                'country_name' => 'Nicaragua',
                'country_iso2' => 'NI',
                'country_iso3' => 'NIC',
                'country_ds' => 25,
                'country_ds_full' => 25,
            ],
            48 =>
            [
                'id' => 49,
                'country_name' => 'Bermuda',
                'country_iso2' => 'BM',
                'country_iso3' => 'BMU',
                'country_ds' => 19,
                'country_ds_full' => 25,
            ],
            49 =>
            [
                'id' => 50,
                'country_name' => 'Poland',
                'country_iso2' => 'PL',
                'country_iso3' => 'POL',
                'country_ds' => 50,
                'country_ds_full' => 186,
            ],
            50 =>
            [
                'id' => 51,
                'country_name' => 'El Salvador',
                'country_iso2' => 'SV',
                'country_iso3' => 'SLV',
                'country_ds' => 25,
                'country_ds_full' => 25,
            ],
            51 =>
            [
                'id' => 52,
                'country_name' => 'Kazakhstan',
                'country_iso2' => 'KZ',
                'country_iso3' => 'KAZ',
                'country_ds' => 25,
                'country_ds_full' => 25,
            ],
            52 =>
            [
                'id' => 53,
                'country_name' => 'Indonesia',
                'country_iso2' => 'ID',
                'country_iso3' => 'IDN',
                'country_ds' => 25,
                'country_ds_full' => 100,
            ],
            53 =>
            [
                'id' => 54,
                'country_name' => 'Macedonia, the Former Yugoslav Republic of',
                'country_iso2' => 'MK',
                'country_iso3' => 'MKD',
                'country_ds' => 25,
                'country_ds_full' => 25,
            ],
            54 =>
            [
                'id' => 55,
                'country_name' => 'Luxembourg',
                'country_iso2' => 'LU',
                'country_iso3' => 'LUX',
                'country_ds' => 50,
                'country_ds_full' => 186,
            ],
            55 =>
            [
                'id' => 56,
                'country_name' => 'Mexico',
                'country_iso2' => 'MX',
                'country_iso3' => 'MEX',
                'country_ds' => 25,
                'country_ds_full' => 50,
            ],
            56 =>
            [
                'id' => 57,
                'country_name' => 'Paraguay',
                'country_iso2' => 'PY',
                'country_iso3' => 'PRY',
                'country_ds' => 25,
                'country_ds_full' => 25,
            ],
            57 =>
            [
                'id' => 58,
                'country_name' => 'Greece',
                'country_iso2' => 'GR',
                'country_iso3' => 'GRC',
                'country_ds' => 25,
                'country_ds_full' => 186,
            ],
            58 =>
            [
                'id' => 59,
                'country_name' => 'Russian Federation',
                'country_iso2' => 'RU',
                'country_iso3' => 'RUS',
                'country_ds' => 25,
                'country_ds_full' => 89,
            ],
            59 =>
            [
                'id' => 60,
                'country_name' => 'Nepal',
                'country_iso2' => 'NP',
                'country_iso3' => 'NPL',
                'country_ds' => 25,
                'country_ds_full' => 25,
            ],
            60 =>
            [
                'id' => 61,
                'country_name' => 'TAHITI',
                'country_iso2' => 'PF',
                'country_iso3' => 'PYF',
                'country_ds' => 19,
                'country_ds_full' => 19,
            ],
            61 =>
            [
                'id' => 62,
                'country_name' => 'Cyprus',
                'country_iso2' => 'CY',
                'country_iso3' => 'CYP',
                'country_ds' => 50,
                'country_ds_full' => 170,
            ],
            62 =>
            [
                'id' => 63,
                'country_name' => 'Macedonia',
                'country_iso2' => 'MK',
                'country_iso3' => 'MKD',
                'country_ds' => 25,
                'country_ds_full' => 25,
            ],
            63 =>
            [
                'id' => 64,
                'country_name' => 'Jersey',
                'country_iso2' => 'JE',
                'country_iso3' => 'JEY',
                'country_ds' => 25,
                'country_ds_full' => 13,
            ],
            64 =>
            [
                'id' => 65,
                'country_name' => 'Zimbabwe',
                'country_iso2' => 'ZW',
                'country_iso3' => 'ZWE',
                'country_ds' => 25,
                'country_ds_full' => 10,
            ],
            65 =>
            [
                'id' => 66,
                'country_name' => 'Iceland',
                'country_iso2' => 'IS',
                'country_iso3' => 'ISL',
                'country_ds' => 25,
                'country_ds_full' => 5,
            ],
        ]);
    }
}

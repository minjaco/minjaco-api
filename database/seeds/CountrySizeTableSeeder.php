<?php

use Illuminate\Database\Seeder;

class CountrySizeTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        \DB::table('country_size')->truncate();
        
        \DB::table('country_size')->insert([
            0 =>
            [
                'country_id' => 1,
                'country_size' => '30A',
                'md_size' => '',
            ],
            1 =>
            [
                'country_id' => 1,
                'country_size' => '30B',
                'md_size' => '',
            ],
            2 =>
            [
                'country_id' => 1,
                'country_size' => '30C',
                'md_size' => '12A',
            ],
            3 =>
            [
                'country_id' => 1,
                'country_size' => '30D',
                'md_size' => '12B',
            ],
            4 =>
            [
                'country_id' => 1,
                'country_size' => '30DD',
                'md_size' => '12C',
            ],
            5 =>
            [
                'country_id' => 1,
                'country_size' => '30DDD',
                'md_size' => '12D',
            ],
            6 =>
            [
                'country_id' => 1,
                'country_size' => '32A',
                'md_size' => '',
            ],
            7 =>
            [
                'country_id' => 1,
                'country_size' => '32B',
                'md_size' => '12A',
            ],
            8 =>
            [
                'country_id' => 1,
                'country_size' => '32C',
                'md_size' => '12B',
            ],
            9 =>
            [
                'country_id' => 1,
                'country_size' => '32D',
                'md_size' => '12C',
            ],
            10 =>
            [
                'country_id' => 1,
                'country_size' => '32DD',
                'md_size' => '12D',
            ],
            11 =>
            [
                'country_id' => 1,
                'country_size' => '32DDD',
                'md_size' => '14D',
            ],
            12 =>
            [
                'country_id' => 1,
                'country_size' => '32F',
                'md_size' => '16D',
            ],
            13 =>
            [
                'country_id' => 1,
                'country_size' => '32G',
                'md_size' => '',
            ],
            14 =>
            [
                'country_id' => 1,
                'country_size' => '34A',
                'md_size' => '12A',
            ],
            15 =>
            [
                'country_id' => 1,
                'country_size' => '34B',
                'md_size' => '12B',
            ],
            16 =>
            [
                'country_id' => 1,
                'country_size' => '34C',
                'md_size' => '12C',
            ],
            17 =>
            [
                'country_id' => 1,
                'country_size' => '34D',
                'md_size' => '12D',
            ],
            18 =>
            [
                'country_id' => 1,
                'country_size' => '34DD',
                'md_size' => '14D',
            ],
            19 =>
            [
                'country_id' => 1,
                'country_size' => '34DDD',
                'md_size' => '16D',
            ],
            20 =>
            [
                'country_id' => 1,
                'country_size' => '34F',
                'md_size' => '',
            ],
            21 =>
            [
                'country_id' => 1,
                'country_size' => '34G',
                'md_size' => '',
            ],
            22 =>
            [
                'country_id' => 1,
                'country_size' => '36A',
                'md_size' => '12B',
            ],
            23 =>
            [
                'country_id' => 1,
                'country_size' => '36B',
                'md_size' => '12C',
            ],
            24 =>
            [
                'country_id' => 1,
                'country_size' => '36C',
                'md_size' => '12D',
            ],
            25 =>
            [
                'country_id' => 1,
                'country_size' => '36D',
                'md_size' => '14D',
            ],
            26 =>
            [
                'country_id' => 1,
                'country_size' => '36DD',
                'md_size' => '16D',
            ],
            27 =>
            [
                'country_id' => 1,
                'country_size' => '36DDD',
                'md_size' => '',
            ],
            28 =>
            [
                'country_id' => 1,
                'country_size' => '36F',
                'md_size' => '',
            ],
            29 =>
            [
                'country_id' => 1,
                'country_size' => '36G',
                'md_size' => '',
            ],
            30 =>
            [
                'country_id' => 1,
                'country_size' => '38A',
                'md_size' => '12C',
            ],
            31 =>
            [
                'country_id' => 1,
                'country_size' => '38B',
                'md_size' => '12D',
            ],
            32 =>
            [
                'country_id' => 1,
                'country_size' => '38C',
                'md_size' => '14D',
            ],
            33 =>
            [
                'country_id' => 1,
                'country_size' => '38D',
                'md_size' => '16D',
            ],
            34 =>
            [
                'country_id' => 1,
                'country_size' => '38DD',
                'md_size' => '',
            ],
            35 =>
            [
                'country_id' => 1,
                'country_size' => '38DDD',
                'md_size' => '',
            ],
            36 =>
            [
                'country_id' => 1,
                'country_size' => '38F',
                'md_size' => '',
            ],
            37 =>
            [
                'country_id' => 1,
                'country_size' => '38G',
                'md_size' => '',
            ],
            38 =>
            [
                'country_id' => 1,
                'country_size' => '40A',
                'md_size' => '12D',
            ],
            39 =>
            [
                'country_id' => 1,
                'country_size' => '40B',
                'md_size' => '14D',
            ],
            40 =>
            [
                'country_id' => 1,
                'country_size' => '40C',
                'md_size' => '16D',
            ],
            41 =>
            [
                'country_id' => 1,
                'country_size' => '40D',
                'md_size' => '',
            ],
            42 =>
            [
                'country_id' => 1,
                'country_size' => '40DD',
                'md_size' => '',
            ],
            43 =>
            [
                'country_id' => 1,
                'country_size' => '40DDD',
                'md_size' => '',
            ],
            44 =>
            [
                'country_id' => 1,
                'country_size' => '40E',
                'md_size' => '',
            ],
            45 =>
            [
                'country_id' => 1,
                'country_size' => '40F',
                'md_size' => '',
            ],
            46 =>
            [
                'country_id' => 2,
                'country_size' => '8A',
                'md_size' => '',
            ],
            47 =>
            [
                'country_id' => 2,
                'country_size' => '8B',
                'md_size' => '',
            ],
            48 =>
            [
                'country_id' => 2,
                'country_size' => '8C',
                'md_size' => '12A',
            ],
            49 =>
            [
                'country_id' => 2,
                'country_size' => '8D',
                'md_size' => '12B',
            ],
            50 =>
            [
                'country_id' => 2,
                'country_size' => '8DD',
                'md_size' => '12C',
            ],
            51 =>
            [
                'country_id' => 2,
                'country_size' => '8E',
                'md_size' => '12D',
            ],
            52 =>
            [
                'country_id' => 2,
                'country_size' => '10A',
                'md_size' => '',
            ],
            53 =>
            [
                'country_id' => 2,
                'country_size' => '10B',
                'md_size' => '12A',
            ],
            54 =>
            [
                'country_id' => 2,
                'country_size' => '10C',
                'md_size' => '12B',
            ],
            55 =>
            [
                'country_id' => 2,
                'country_size' => '10D',
                'md_size' => '12C',
            ],
            56 =>
            [
                'country_id' => 2,
                'country_size' => '10DD',
                'md_size' => '12D',
            ],
            57 =>
            [
                'country_id' => 2,
                'country_size' => '10E',
                'md_size' => '14D',
            ],
            58 =>
            [
                'country_id' => 2,
                'country_size' => '10F',
                'md_size' => '16D',
            ],
            59 =>
            [
                'country_id' => 2,
                'country_size' => '12A',
                'md_size' => '12A',
            ],
            60 =>
            [
                'country_id' => 2,
                'country_size' => '12B',
                'md_size' => '12B',
            ],
            61 =>
            [
                'country_id' => 2,
                'country_size' => '12C',
                'md_size' => '12C',
            ],
            62 =>
            [
                'country_id' => 2,
                'country_size' => '12D',
                'md_size' => '12D',
            ],
            63 =>
            [
                'country_id' => 2,
                'country_size' => '12DD',
                'md_size' => '14D',
            ],
            64 =>
            [
                'country_id' => 2,
                'country_size' => '12E',
                'md_size' => '16D',
            ],
            65 =>
            [
                'country_id' => 2,
                'country_size' => '12F',
                'md_size' => '',
            ],
            66 =>
            [
                'country_id' => 2,
                'country_size' => '14A',
                'md_size' => '12B',
            ],
            67 =>
            [
                'country_id' => 2,
                'country_size' => '14B',
                'md_size' => '12C',
            ],
            68 =>
            [
                'country_id' => 2,
                'country_size' => '14C',
                'md_size' => '12D',
            ],
            69 =>
            [
                'country_id' => 2,
                'country_size' => '14D',
                'md_size' => '14D',
            ],
            70 =>
            [
                'country_id' => 2,
                'country_size' => '14DD',
                'md_size' => '16D',
            ],
            71 =>
            [
                'country_id' => 2,
                'country_size' => '14E',
                'md_size' => '',
            ],
            72 =>
            [
                'country_id' => 2,
                'country_size' => '14F',
                'md_size' => '',
            ],
            73 =>
            [
                'country_id' => 2,
                'country_size' => '16A',
                'md_size' => '12C',
            ],
            74 =>
            [
                'country_id' => 2,
                'country_size' => '16B',
                'md_size' => '12D',
            ],
            75 =>
            [
                'country_id' => 2,
                'country_size' => '16C',
                'md_size' => '14D',
            ],
            76 =>
            [
                'country_id' => 2,
                'country_size' => '16D',
                'md_size' => '16D',
            ],
            77 =>
            [
                'country_id' => 2,
                'country_size' => '16DD',
                'md_size' => '',
            ],
            78 =>
            [
                'country_id' => 2,
                'country_size' => '16E',
                'md_size' => '',
            ],
            79 =>
            [
                'country_id' => 2,
                'country_size' => '18F',
                'md_size' => '',
            ],
            80 =>
            [
                'country_id' => 2,
                'country_size' => '18A',
                'md_size' => '12D',
            ],
            81 =>
            [
                'country_id' => 2,
                'country_size' => '18B',
                'md_size' => '14D',
            ],
            82 =>
            [
                'country_id' => 2,
                'country_size' => '18C',
                'md_size' => '16D',
            ],
            83 =>
            [
                'country_id' => 2,
                'country_size' => '18D',
                'md_size' => '',
            ],
            84 =>
            [
                'country_id' => 2,
                'country_size' => '18DD',
                'md_size' => '',
            ],
            85 =>
            [
                'country_id' => 2,
                'country_size' => '18E',
                'md_size' => '',
            ],
            86 =>
            [
                'country_id' => 2,
                'country_size' => '18F',
                'md_size' => '',
            ],
            87 =>
            [
                'country_id' => 3,
                'country_size' => '30A',
                'md_size' => '',
            ],
            88 =>
            [
                'country_id' => 3,
                'country_size' => '30B',
                'md_size' => '',
            ],
            89 =>
            [
                'country_id' => 3,
                'country_size' => '30C',
                'md_size' => '12A',
            ],
            90 =>
            [
                'country_id' => 3,
                'country_size' => '30D',
                'md_size' => '12B',
            ],
            91 =>
            [
                'country_id' => 3,
                'country_size' => '30DD',
                'md_size' => '12C',
            ],
            92 =>
            [
                'country_id' => 3,
                'country_size' => '30E',
                'md_size' => '12D',
            ],
            93 =>
            [
                'country_id' => 3,
                'country_size' => '32A',
                'md_size' => '',
            ],
            94 =>
            [
                'country_id' => 3,
                'country_size' => '32B',
                'md_size' => '12A',
            ],
            95 =>
            [
                'country_id' => 3,
                'country_size' => '32C',
                'md_size' => '12B',
            ],
            96 =>
            [
                'country_id' => 3,
                'country_size' => '32D',
                'md_size' => '12C',
            ],
            97 =>
            [
                'country_id' => 3,
                'country_size' => '32DD',
                'md_size' => '12D',
            ],
            98 =>
            [
                'country_id' => 3,
                'country_size' => '32E',
                'md_size' => '14D',
            ],
            99 =>
            [
                'country_id' => 3,
                'country_size' => '32F',
                'md_size' => '16D',
            ],
            100 =>
            [
                'country_id' => 3,
                'country_size' => '32G',
                'md_size' => '',
            ],
            101 =>
            [
                'country_id' => 3,
                'country_size' => '34A',
                'md_size' => '12A',
            ],
            102 =>
            [
                'country_id' => 3,
                'country_size' => '34B',
                'md_size' => '12B',
            ],
            103 =>
            [
                'country_id' => 3,
                'country_size' => '34C',
                'md_size' => '12C',
            ],
            104 =>
            [
                'country_id' => 3,
                'country_size' => '34D',
                'md_size' => '12D',
            ],
            105 =>
            [
                'country_id' => 3,
                'country_size' => '34DD',
                'md_size' => '14D',
            ],
            106 =>
            [
                'country_id' => 3,
                'country_size' => '34E',
                'md_size' => '16D',
            ],
            107 =>
            [
                'country_id' => 3,
                'country_size' => '34F',
                'md_size' => '',
            ],
            108 =>
            [
                'country_id' => 3,
                'country_size' => '34G',
                'md_size' => '',
            ],
            109 =>
            [
                'country_id' => 3,
                'country_size' => '36A',
                'md_size' => '12B',
            ],
            110 =>
            [
                'country_id' => 3,
                'country_size' => '36B',
                'md_size' => '12C',
            ],
            111 =>
            [
                'country_id' => 3,
                'country_size' => '36C',
                'md_size' => '12D',
            ],
            112 =>
            [
                'country_id' => 3,
                'country_size' => '36D',
                'md_size' => '14D',
            ],
            113 =>
            [
                'country_id' => 3,
                'country_size' => '36DD',
                'md_size' => '16D',
            ],
            114 =>
            [
                'country_id' => 3,
                'country_size' => '36E',
                'md_size' => '',
            ],
            115 =>
            [
                'country_id' => 3,
                'country_size' => '36F',
                'md_size' => '',
            ],
            116 =>
            [
                'country_id' => 3,
                'country_size' => '36G',
                'md_size' => '',
            ],
            117 =>
            [
                'country_id' => 3,
                'country_size' => '38A',
                'md_size' => '12C',
            ],
            118 =>
            [
                'country_id' => 3,
                'country_size' => '38B',
                'md_size' => '12D',
            ],
            119 =>
            [
                'country_id' => 3,
                'country_size' => '38C',
                'md_size' => '14D',
            ],
            120 =>
            [
                'country_id' => 3,
                'country_size' => '38D',
                'md_size' => '16D',
            ],
            121 =>
            [
                'country_id' => 3,
                'country_size' => '38DD',
                'md_size' => '',
            ],
            122 =>
            [
                'country_id' => 3,
                'country_size' => '38E',
                'md_size' => '',
            ],
            123 =>
            [
                'country_id' => 3,
                'country_size' => '38F',
                'md_size' => '',
            ],
            124 =>
            [
                'country_id' => 3,
                'country_size' => '38G',
                'md_size' => '',
            ],
            125 =>
            [
                'country_id' => 3,
                'country_size' => '40A',
                'md_size' => '12D',
            ],
            126 =>
            [
                'country_id' => 3,
                'country_size' => '40B',
                'md_size' => '14D',
            ],
            127 =>
            [
                'country_id' => 3,
                'country_size' => '40C',
                'md_size' => '16D',
            ],
            128 =>
            [
                'country_id' => 3,
                'country_size' => '40D',
                'md_size' => '',
            ],
            129 =>
            [
                'country_id' => 3,
                'country_size' => '40DD',
                'md_size' => '',
            ],
            130 =>
            [
                'country_id' => 3,
                'country_size' => '40E',
                'md_size' => '',
            ],
            131 =>
            [
                'country_id' => 3,
                'country_size' => '40E',
                'md_size' => '',
            ],
            132 =>
            [
                'country_id' => 3,
                'country_size' => '40F',
                'md_size' => '',
            ],
            133 =>
            [
                'country_id' => 7,
                'country_size' => '80B',
                'md_size' => '',
            ],
            134 =>
            [
                'country_id' => 7,
                'country_size' => '80C',
                'md_size' => '',
            ],
            135 =>
            [
                'country_id' => 7,
                'country_size' => '80D',
                'md_size' => '12A',
            ],
            136 =>
            [
                'country_id' => 7,
                'country_size' => '80E',
                'md_size' => '12B',
            ],
            137 =>
            [
                'country_id' => 7,
                'country_size' => '80F',
                'md_size' => '12C',
            ],
            138 =>
            [
                'country_id' => 7,
                'country_size' => '80G',
                'md_size' => '12D',
            ],
            139 =>
            [
                'country_id' => 7,
                'country_size' => '85B',
                'md_size' => '',
            ],
            140 =>
            [
                'country_id' => 7,
                'country_size' => '85C',
                'md_size' => '12A',
            ],
            141 =>
            [
                'country_id' => 7,
                'country_size' => '85D',
                'md_size' => '12B',
            ],
            142 =>
            [
                'country_id' => 7,
                'country_size' => '85E',
                'md_size' => '12C',
            ],
            143 =>
            [
                'country_id' => 7,
                'country_size' => '85F',
                'md_size' => '12D',
            ],
            144 =>
            [
                'country_id' => 7,
                'country_size' => '85G',
                'md_size' => '14D',
            ],
            145 =>
            [
                'country_id' => 7,
                'country_size' => '85H',
                'md_size' => '16D',
            ],
            146 =>
            [
                'country_id' => 7,
                'country_size' => '85I',
                'md_size' => '',
            ],
            147 =>
            [
                'country_id' => 7,
                'country_size' => '90B',
                'md_size' => '12A',
            ],
            148 =>
            [
                'country_id' => 7,
                'country_size' => '90C',
                'md_size' => '12B',
            ],
            149 =>
            [
                'country_id' => 7,
                'country_size' => '90D',
                'md_size' => '12C',
            ],
            150 =>
            [
                'country_id' => 7,
                'country_size' => '90E',
                'md_size' => '12D',
            ],
            151 =>
            [
                'country_id' => 7,
                'country_size' => '90F',
                'md_size' => '14D',
            ],
            152 =>
            [
                'country_id' => 7,
                'country_size' => '90G',
                'md_size' => '16D',
            ],
            153 =>
            [
                'country_id' => 7,
                'country_size' => '90H',
                'md_size' => '',
            ],
            154 =>
            [
                'country_id' => 7,
                'country_size' => '90I',
                'md_size' => '',
            ],
            155 =>
            [
                'country_id' => 7,
                'country_size' => '95B',
                'md_size' => '12B',
            ],
            156 =>
            [
                'country_id' => 7,
                'country_size' => '95C',
                'md_size' => '12C',
            ],
            157 =>
            [
                'country_id' => 7,
                'country_size' => '95D',
                'md_size' => '12D',
            ],
            158 =>
            [
                'country_id' => 7,
                'country_size' => '95E',
                'md_size' => '14D',
            ],
            159 =>
            [
                'country_id' => 7,
                'country_size' => '95E',
                'md_size' => '16D',
            ],
            160 =>
            [
                'country_id' => 7,
                'country_size' => '95G',
                'md_size' => '',
            ],
            161 =>
            [
                'country_id' => 7,
                'country_size' => '95H',
                'md_size' => '',
            ],
            162 =>
            [
                'country_id' => 7,
                'country_size' => '95I',
                'md_size' => '',
            ],
            163 =>
            [
                'country_id' => 7,
                'country_size' => '100C',
                'md_size' => '12D',
            ],
            164 =>
            [
                'country_id' => 7,
                'country_size' => '100D',
                'md_size' => '14D',
            ],
            165 =>
            [
                'country_id' => 7,
                'country_size' => '100E',
                'md_size' => '16D',
            ],
            166 =>
            [
                'country_id' => 7,
                'country_size' => '100F',
                'md_size' => '',
            ],
            167 =>
            [
                'country_id' => 7,
                'country_size' => '100G',
                'md_size' => '',
            ],
            168 =>
            [
                'country_id' => 7,
                'country_size' => '100H',
                'md_size' => '',
            ],
            169 =>
            [
                'country_id' => 7,
                'country_size' => '100I',
                'md_size' => '',
            ],
            170 =>
            [
                'country_id' => 7,
                'country_size' => '105C',
                'md_size' => '14D',
            ],
            171 =>
            [
                'country_id' => 7,
                'country_size' => '105D',
                'md_size' => '16D',
            ],
            172 =>
            [
                'country_id' => 7,
                'country_size' => '105E',
                'md_size' => '',
            ],
            173 =>
            [
                'country_id' => 7,
                'country_size' => '105F',
                'md_size' => '',
            ],
            174 =>
            [
                'country_id' => 7,
                'country_size' => '105G',
                'md_size' => '',
            ],
            175 =>
            [
                'country_id' => 7,
                'country_size' => '105H',
                'md_size' => '',
            ],
            176 =>
            [
                'country_id' => 7,
                'country_size' => '105I',
                'md_size' => '',
            ],
            177 =>
            [
                'country_id' => 28,
                'country_size' => '80B',
                'md_size' => '',
            ],
            178 =>
            [
                'country_id' => 28,
                'country_size' => '80C',
                'md_size' => '',
            ],
            179 =>
            [
                'country_id' => 28,
                'country_size' => '80D',
                'md_size' => '12A',
            ],
            180 =>
            [
                'country_id' => 28,
                'country_size' => '80E',
                'md_size' => '12B',
            ],
            181 =>
            [
                'country_id' => 28,
                'country_size' => '80F',
                'md_size' => '12C',
            ],
            182 =>
            [
                'country_id' => 28,
                'country_size' => '80G',
                'md_size' => '12D',
            ],
            183 =>
            [
                'country_id' => 28,
                'country_size' => '85B',
                'md_size' => '',
            ],
            184 =>
            [
                'country_id' => 28,
                'country_size' => '85C',
                'md_size' => '12A',
            ],
            185 =>
            [
                'country_id' => 28,
                'country_size' => '85D',
                'md_size' => '12B',
            ],
            186 =>
            [
                'country_id' => 28,
                'country_size' => '85E',
                'md_size' => '12C',
            ],
            187 =>
            [
                'country_id' => 28,
                'country_size' => '85F',
                'md_size' => '12D',
            ],
            188 =>
            [
                'country_id' => 28,
                'country_size' => '85G',
                'md_size' => '14D',
            ],
            189 =>
            [
                'country_id' => 28,
                'country_size' => '85H',
                'md_size' => '16D',
            ],
            190 =>
            [
                'country_id' => 28,
                'country_size' => '85I',
                'md_size' => '',
            ],
            191 =>
            [
                'country_id' => 28,
                'country_size' => '90B',
                'md_size' => '12A',
            ],
            192 =>
            [
                'country_id' => 28,
                'country_size' => '90C',
                'md_size' => '12B',
            ],
            193 =>
            [
                'country_id' => 28,
                'country_size' => '90D',
                'md_size' => '12C',
            ],
            194 =>
            [
                'country_id' => 28,
                'country_size' => '90E',
                'md_size' => '12D',
            ],
            195 =>
            [
                'country_id' => 28,
                'country_size' => '90F',
                'md_size' => '14D',
            ],
            196 =>
            [
                'country_id' => 28,
                'country_size' => '90G',
                'md_size' => '16D',
            ],
            197 =>
            [
                'country_id' => 28,
                'country_size' => '90H',
                'md_size' => '',
            ],
            198 =>
            [
                'country_id' => 28,
                'country_size' => '90I',
                'md_size' => '',
            ],
            199 =>
            [
                'country_id' => 28,
                'country_size' => '95B',
                'md_size' => '12B',
            ],
            200 =>
            [
                'country_id' => 28,
                'country_size' => '95C',
                'md_size' => '12C',
            ],
            201 =>
            [
                'country_id' => 28,
                'country_size' => '95D',
                'md_size' => '12D',
            ],
            202 =>
            [
                'country_id' => 28,
                'country_size' => '95E',
                'md_size' => '14D',
            ],
            203 =>
            [
                'country_id' => 28,
                'country_size' => '95E',
                'md_size' => '16D',
            ],
            204 =>
            [
                'country_id' => 28,
                'country_size' => '95G',
                'md_size' => '',
            ],
            205 =>
            [
                'country_id' => 28,
                'country_size' => '95H',
                'md_size' => '',
            ],
            206 =>
            [
                'country_id' => 28,
                'country_size' => '95I',
                'md_size' => '',
            ],
            207 =>
            [
                'country_id' => 28,
                'country_size' => '100C',
                'md_size' => '12D',
            ],
            208 =>
            [
                'country_id' => 28,
                'country_size' => '100D',
                'md_size' => '14D',
            ],
            209 =>
            [
                'country_id' => 28,
                'country_size' => '100E',
                'md_size' => '16D',
            ],
            210 =>
            [
                'country_id' => 28,
                'country_size' => '100F',
                'md_size' => '',
            ],
            211 =>
            [
                'country_id' => 28,
                'country_size' => '100G',
                'md_size' => '',
            ],
            212 =>
            [
                'country_id' => 28,
                'country_size' => '100H',
                'md_size' => '',
            ],
            213 =>
            [
                'country_id' => 28,
                'country_size' => '100I',
                'md_size' => '',
            ],
            214 =>
            [
                'country_id' => 28,
                'country_size' => '105C',
                'md_size' => '14D',
            ],
            215 =>
            [
                'country_id' => 28,
                'country_size' => '105D',
                'md_size' => '16D',
            ],
            216 =>
            [
                'country_id' => 28,
                'country_size' => '105E',
                'md_size' => '',
            ],
            217 =>
            [
                'country_id' => 28,
                'country_size' => '105F',
                'md_size' => '',
            ],
            218 =>
            [
                'country_id' => 28,
                'country_size' => '105G',
                'md_size' => '',
            ],
            219 =>
            [
                'country_id' => 28,
                'country_size' => '105H',
                'md_size' => '',
            ],
            220 =>
            [
                'country_id' => 28,
                'country_size' => '105I',
                'md_size' => '',
            ],
            221 =>
            [
                'country_id' => 37,
                'country_size' => '80B',
                'md_size' => '',
            ],
            222 =>
            [
                'country_id' => 37,
                'country_size' => '80C',
                'md_size' => '',
            ],
            223 =>
            [
                'country_id' => 37,
                'country_size' => '80D',
                'md_size' => '12A',
            ],
            224 =>
            [
                'country_id' => 37,
                'country_size' => '80E',
                'md_size' => '12B',
            ],
            225 =>
            [
                'country_id' => 37,
                'country_size' => '80F',
                'md_size' => '12C',
            ],
            226 =>
            [
                'country_id' => 37,
                'country_size' => '80G',
                'md_size' => '12D',
            ],
            227 =>
            [
                'country_id' => 37,
                'country_size' => '85B',
                'md_size' => '',
            ],
            228 =>
            [
                'country_id' => 37,
                'country_size' => '85C',
                'md_size' => '12A',
            ],
            229 =>
            [
                'country_id' => 37,
                'country_size' => '85D',
                'md_size' => '12B',
            ],
            230 =>
            [
                'country_id' => 37,
                'country_size' => '85E',
                'md_size' => '12C',
            ],
            231 =>
            [
                'country_id' => 37,
                'country_size' => '85F',
                'md_size' => '12D',
            ],
            232 =>
            [
                'country_id' => 37,
                'country_size' => '85G',
                'md_size' => '14D',
            ],
            233 =>
            [
                'country_id' => 37,
                'country_size' => '85H',
                'md_size' => '16D',
            ],
            234 =>
            [
                'country_id' => 37,
                'country_size' => '85I',
                'md_size' => '',
            ],
            235 =>
            [
                'country_id' => 37,
                'country_size' => '90B',
                'md_size' => '12A',
            ],
            236 =>
            [
                'country_id' => 37,
                'country_size' => '90C',
                'md_size' => '12B',
            ],
            237 =>
            [
                'country_id' => 37,
                'country_size' => '90D',
                'md_size' => '12C',
            ],
            238 =>
            [
                'country_id' => 37,
                'country_size' => '90E',
                'md_size' => '12D',
            ],
            239 =>
            [
                'country_id' => 37,
                'country_size' => '90F',
                'md_size' => '14D',
            ],
            240 =>
            [
                'country_id' => 37,
                'country_size' => '90G',
                'md_size' => '16D',
            ],
            241 =>
            [
                'country_id' => 37,
                'country_size' => '90H',
                'md_size' => '',
            ],
            242 =>
            [
                'country_id' => 37,
                'country_size' => '90I',
                'md_size' => '',
            ],
            243 =>
            [
                'country_id' => 37,
                'country_size' => '95B',
                'md_size' => '12B',
            ],
            244 =>
            [
                'country_id' => 37,
                'country_size' => '95C',
                'md_size' => '12C',
            ],
            245 =>
            [
                'country_id' => 37,
                'country_size' => '95D',
                'md_size' => '12D',
            ],
            246 =>
            [
                'country_id' => 37,
                'country_size' => '95E',
                'md_size' => '14D',
            ],
            247 =>
            [
                'country_id' => 37,
                'country_size' => '95E',
                'md_size' => '16D',
            ],
            248 =>
            [
                'country_id' => 37,
                'country_size' => '95G',
                'md_size' => '',
            ],
            249 =>
            [
                'country_id' => 37,
                'country_size' => '95H',
                'md_size' => '',
            ],
            250 =>
            [
                'country_id' => 37,
                'country_size' => '95I',
                'md_size' => '',
            ],
            251 =>
            [
                'country_id' => 37,
                'country_size' => '100C',
                'md_size' => '12D',
            ],
            252 =>
            [
                'country_id' => 37,
                'country_size' => '100D',
                'md_size' => '14D',
            ],
            253 =>
            [
                'country_id' => 37,
                'country_size' => '100E',
                'md_size' => '16D',
            ],
            254 =>
            [
                'country_id' => 37,
                'country_size' => '100F',
                'md_size' => '',
            ],
            255 =>
            [
                'country_id' => 37,
                'country_size' => '100G',
                'md_size' => '',
            ],
            256 =>
            [
                'country_id' => 37,
                'country_size' => '100H',
                'md_size' => '',
            ],
            257 =>
            [
                'country_id' => 37,
                'country_size' => '100I',
                'md_size' => '',
            ],
            258 =>
            [
                'country_id' => 37,
                'country_size' => '105C',
                'md_size' => '14D',
            ],
            259 =>
            [
                'country_id' => 37,
                'country_size' => '105D',
                'md_size' => '16D',
            ],
            260 =>
            [
                'country_id' => 37,
                'country_size' => '105E',
                'md_size' => '',
            ],
            261 =>
            [
                'country_id' => 37,
                'country_size' => '105F',
                'md_size' => '',
            ],
            262 =>
            [
                'country_id' => 37,
                'country_size' => '105G',
                'md_size' => '',
            ],
            263 =>
            [
                'country_id' => 37,
                'country_size' => '105H',
                'md_size' => '',
            ],
            264 =>
            [
                'country_id' => 37,
                'country_size' => '105I',
                'md_size' => '',
            ],
            265 =>
            [
                'country_id' => 0,
                'country_size' => '65B',
                'md_size' => '',
            ],
            266 =>
            [
                'country_id' => 0,
                'country_size' => '65C',
                'md_size' => '',
            ],
            267 =>
            [
                'country_id' => 0,
                'country_size' => '65D',
                'md_size' => '12A',
            ],
            268 =>
            [
                'country_id' => 0,
                'country_size' => '65E',
                'md_size' => '12B',
            ],
            269 =>
            [
                'country_id' => 0,
                'country_size' => '65F',
                'md_size' => '12C',
            ],
            270 =>
            [
                'country_id' => 0,
                'country_size' => '65G',
                'md_size' => '12D',
            ],
            271 =>
            [
                'country_id' => 0,
                'country_size' => '70B',
                'md_size' => '',
            ],
            272 =>
            [
                'country_id' => 0,
                'country_size' => '70C',
                'md_size' => '12A',
            ],
            273 =>
            [
                'country_id' => 0,
                'country_size' => '70D',
                'md_size' => '12B',
            ],
            274 =>
            [
                'country_id' => 0,
                'country_size' => '70E',
                'md_size' => '12C',
            ],
            275 =>
            [
                'country_id' => 0,
                'country_size' => '70F',
                'md_size' => '12D',
            ],
            276 =>
            [
                'country_id' => 0,
                'country_size' => '70G',
                'md_size' => '14D',
            ],
            277 =>
            [
                'country_id' => 0,
                'country_size' => '70H',
                'md_size' => '16D',
            ],
            278 =>
            [
                'country_id' => 0,
                'country_size' => '70I',
                'md_size' => '',
            ],
            279 =>
            [
                'country_id' => 0,
                'country_size' => '75B',
                'md_size' => '12A',
            ],
            280 =>
            [
                'country_id' => 0,
                'country_size' => '75C',
                'md_size' => '12B',
            ],
            281 =>
            [
                'country_id' => 0,
                'country_size' => '75D',
                'md_size' => '12C',
            ],
            282 =>
            [
                'country_id' => 0,
                'country_size' => '75E',
                'md_size' => '12D',
            ],
            283 =>
            [
                'country_id' => 0,
                'country_size' => '75F',
                'md_size' => '14D',
            ],
            284 =>
            [
                'country_id' => 0,
                'country_size' => '75G',
                'md_size' => '16D',
            ],
            285 =>
            [
                'country_id' => 0,
                'country_size' => '75H',
                'md_size' => '',
            ],
            286 =>
            [
                'country_id' => 0,
                'country_size' => '75I',
                'md_size' => '',
            ],
            287 =>
            [
                'country_id' => 0,
                'country_size' => '80B',
                'md_size' => '12B',
            ],
            288 =>
            [
                'country_id' => 0,
                'country_size' => '80C',
                'md_size' => '12C',
            ],
            289 =>
            [
                'country_id' => 0,
                'country_size' => '80D',
                'md_size' => '12D',
            ],
            290 =>
            [
                'country_id' => 0,
                'country_size' => '80E',
                'md_size' => '14D',
            ],
            291 =>
            [
                'country_id' => 0,
                'country_size' => '80F',
                'md_size' => '16D',
            ],
            292 =>
            [
                'country_id' => 0,
                'country_size' => '80G',
                'md_size' => '',
            ],
            293 =>
            [
                'country_id' => 0,
                'country_size' => '80H',
                'md_size' => '',
            ],
            294 =>
            [
                'country_id' => 0,
                'country_size' => '80I',
                'md_size' => '',
            ],
            295 =>
            [
                'country_id' => 0,
                'country_size' => '85C',
                'md_size' => '12D',
            ],
            296 =>
            [
                'country_id' => 0,
                'country_size' => '85D',
                'md_size' => '14D',
            ],
            297 =>
            [
                'country_id' => 0,
                'country_size' => '85E',
                'md_size' => '16D',
            ],
            298 =>
            [
                'country_id' => 0,
                'country_size' => '85F',
                'md_size' => '',
            ],
            299 =>
            [
                'country_id' => 0,
                'country_size' => '85G',
                'md_size' => '',
            ],
            300 =>
            [
                'country_id' => 0,
                'country_size' => '85H',
                'md_size' => '',
            ],
            301 =>
            [
                'country_id' => 0,
                'country_size' => '85I',
                'md_size' => '',
            ],
            302 =>
            [
                'country_id' => 0,
                'country_size' => '90C',
                'md_size' => '14D',
            ],
            303 =>
            [
                'country_id' => 0,
                'country_size' => '90D',
                'md_size' => '16D',
            ],
            304 =>
            [
                'country_id' => 0,
                'country_size' => '90E',
                'md_size' => '',
            ],
            305 =>
            [
                'country_id' => 0,
                'country_size' => '90F',
                'md_size' => '',
            ],
            306 =>
            [
                'country_id' => 0,
                'country_size' => '90G',
                'md_size' => '',
            ],
            307 =>
            [
                'country_id' => 0,
                'country_size' => '90H',
                'md_size' => '',
            ],
            308 =>
            [
                'country_id' => 0,
                'country_size' => '90I',
                'md_size' => '',
            ],
        ]);
    }
}

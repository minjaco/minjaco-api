<?php
/**
 * Formats money with shorten abbreviation
 * @param $number
 * @return string
 */

if (!function_exists('number_abbreviation')) {
    function number_abbreviation($number)
    {
        $abbrevs = [12 => "T", 9 => "B", 6 => "M", 3 => "K", 0 => ""];
        foreach ($abbrevs as $exponent => $abbrev) {
            if ($number >= pow(10, $exponent)) {
                $display_num = $number / pow(10, $exponent);
                $decimals    = ($exponent >= 3 && round($display_num) < 100) ? 1 : 0;
                return number_format($display_num, $decimals) . $abbrev;
            }
        }
    }
}

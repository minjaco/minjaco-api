<?php

namespace App\Mail;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendReportEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $file_path;
    public $report_name;

    /**
     * Create a new message instance.
     *
     * @param $file_path
     * @param $report_name
     */
    public function __construct($report_name, $file_path)
    {
        $this->file_path   = $file_path;
        $this->report_name = $report_name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('it@muscledazzle.com', 'Minjaco IT')
            ->subject($this->report_name .'( '. Carbon::now()->format('d-m-Y').' )')
            ->view('maileclipse::templates.reportEmail')
            ->attachFromStorage('reports/'.$this->file_path)
            ->with([
                'report_name' => $this->report_name,
            ]);
    }
}

<?php

namespace App\Http\Controllers;

use App\Exports\InvoiceExport;
use App\Imports\BikiniImageImport;
use App\Imports\ImageImport;
use App\Imports\SizingImport;
use App\Models\Customer;
use App\Models\Product;
use App\Models\Status;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Spatie\Analytics\Period;

class HelperController extends Controller
{
    public function invoice($invid)
    {
        return \Excel::download(new InvoiceExport($invid), 'invoices.xlsx');
    }

    public function matrix()
    {
        \Excel::import(new SizingImport(), storage_path('sizing_charts.xlsx'));
    }

    public function rename()
    {
        \Excel::import(new ImageImport(), storage_path('new_names.xlsx'));
    }

    public function cbRename()
    {
        \Excel::import(new BikiniImageImport(), storage_path('cb_images.xlsx'));
    }

    public function fetchProductsFromBigCommerce()
    {
        $filter = ['page' => 1, 'limit' => 10];

        $products = \Bigcommerce::getProducts($filter);

        foreach ($products as $product) {
            $p              = new Product();
            $p->product_id  = $product->id;
            $p->name        = $product->name;
            $p->sku         = $product->sku;
            $p->price       = $product->price;
            $p->is_visible  = $product->is_visible;
            $p->is_featured = $product->is_featured;
            $p->total_sold  = $product->total_sold;
            $p->view_count  = $product->view_count;
            $p->custom_url  = $product->custom_url;
            $p->image_url   = $product->primary_image->standard_url;
            $p->description = $product->description;
            $p->save();
        }

        // https://docs.spatie.be/laravel-activitylog/v3/introduction
        /*activity()
            ->performedOn($product)
            ->causedBy(123)
            ->withProperties(['laravel' => 'awesome'])
            ->log('The subject name is :subject.name, the causer name is :causer.name and Laravel is :properties.laravel');*/

        activity('api-fetch')->log('fetchCustomersFromBigCommerce finished');
    }

    public function productImages()
    {
        $images = \Bigcommerce::getCollection('/products/1236/images');

        $subset = collect($images)->map(function ($image) {
            return $image->thumbnail_url;
        });

        //dd($subset);
    }

    public function fetchCustomersFromBigCommerce()
    {
        for ($i = 1; $i <= 44; $i++) {
            $filter    = ['page' => $i, 'min_date_created' => '2018-01-01'];
            $customers = \Bigcommerce::getCustomers($filter);

            foreach ($customers as $customer) {
                $c                = new Customer();
                $c->customer_id   = $customer->id;
                $c->uuid          = Str::uuid();
                $c->name          = $customer->first_name.' '.$customer->last_name;
                $c->phone_number  = $customer->phone;
                $c->email         = $customer->email;
                $c->registered_at = Carbon::parse($customer->date_created)->format('Y-m-d H:i:s');

                $address = collect(\Bigcommerce::getCustomerAddresses($customer->id))->first();

                if ($address) {
                    $c->address = $address->street_1.' '.$address->street_2.' '.$address->city.' '.$address->state.' '.$address->zip;
                    $c->country = $address->country;
                }

                $c->save();
            }
        }

        activity('api-fetch')->log('fetchCustomersFromBigCommerce finished');
    }

    public function fetchCustomersFromActiveCampaign()
    {
        $contact = \AC::contactViewByEmail('acarvalhal1@gmail.com');

        $fields = collect($contact->fields);

        $federation       = $fields->get(10)->val;
        $next_competation = $fields->get(11)->val;
        $competed_before  = $fields->get(12)->val;
        $facebook         = $fields->get(17)->val;
        $instagram        = $fields->get(18)->val;
        $division         = $fields->get(31)->val;

        //echo $instagram->val;

        //dd($instagram);

        //foreach ($fields as $field) {
        //    echo $field->title . '----' . $field->val . '<br>';
        //}

        dd($contact);
        //dd(compact('federation', 'next_competation', 'competed_before', 'next_competation', 'facebook', 'instagram', 'division'));
    }

    public function try()
    {
        $statuses = app(Status::class)->order_by('order_by')->all();


        foreach ($statuses as $status) {
            echo 'const '.str_replace(
                ' ',
                '_',
                strtoupper($status->status_name)
                ).' = '.$status->status_id.';<br>';
        }
    }

    public function oldDb()
    {
        $latest_order = \DB::connection('mysql_old')->table('tb_orders')->orderBy(
            'order_id',
            'desc'
            )->first()->order_id + 1;

        echo $latest_order;
    }

    public function search()
    {
        $customers = Customer::search('icloud')->get();

        dd($customers);
    }

    public function viewa()
    {
        $user_id = 7676; // Erhan id

        $startDate = Carbon::now()->subMonth();
        $endDate   = Carbon::now();

        $analyticsData = \Analytics::performQuery(
            Period::create($startDate, $endDate),
            'ga:sessions',
            [
                'metrics'    => 'ga:timeOnPage, ga:pageviews',
                'dimensions' => 'ga:dimension1, ga:pagePath, ga:date, ga:dateHourMinute',
                'filters'    => 'ga:dimension1=='.$user_id,
                'sort'       => '-ga:dateHourMinute',
            ]
        );

        dd(collect($analyticsData->rows));
    }

    public function enc()
    {
        echo encrypt(env('BIGCOMMERCE_WEBHOOK_SECRET'));
    }

    public function denc()
    {
        echo decrypt('eyJpdiI6IklKajNvVnFYQk90Y0FKUitxZGdvOVE9PSIsInZhbHVlIjoiamtNZzA3NFVZUkRQQ2V4TWRQQTdTTVBXUVhHVTNKVWhZXC80Z1d5V2dxYjQ9IiwibWFjIjoiZTAxODc0NWIwYzcwMjkzMzEzMDg1ZjM4MDI5NTE3MjY0ZjdkNjY5NTJkZDM5ZjFhZTIyMTRkNmExYWMzMTM1NyJ9') === env('BIGCOMMERCE_WEBHOOK_SECRET') ? 'Yep' : 'Nope';
    }

    public function convert()
    {
        //return \Conversion::convert('5.2', 'foot')->to('centimetre');
        return $this->convert_to_cm('5.2');
    }

    // set up for users image uploader
    public function createUserImageUrl()
    {
        return 'http://www.muscledazzle.com/customer-images/'.encrypt('19344');
    }

    public function decodeUserOrder($token)
    {
        return decrypt($token);
    }

    /**
     * Converts a height value given in feet/inches to cm
     *
     * @param  int  $feet
     * @param  int  $inches
     * @return int
     */
    public static function convert_to_cm($feet, $inches = 0)
    {
        $inches = ($feet * 12) + $inches;

        return (int) round($inches / 0.393701);
    }
}

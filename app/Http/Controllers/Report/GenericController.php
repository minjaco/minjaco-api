<?php

namespace App\Http\Controllers\Report;

use App\Exports\CrystalReport;
use App\Exports\FinishedSuitsReport;
use App\Exports\FinishGoodReport;
use App\Exports\PackingReport;
use App\Exports\SewingByStatusReport;
use App\Exports\SewingReport;
use App\Exports\ShippingReport;
use App\Http\Controllers\Controller;
use App\Jobs\ReportReadyJob;
use Carbon\Carbon;

class GenericController extends Controller
{
    public function sewn()
    {
        $file = 'sewing_report_'.Carbon::now()->timestamp.'.xlsx';

        (new SewingReport(
            \request('begin_date'),
            \request('end_date')
        ))->queue('reports/'.$file, 'local')
            ->chain([
                new ReportReadyJob('Sewn Report', $file, env('ACCOUNTING_EMAIL')),
            ]);

        return ['success' => true, 'mail' => env('ACCOUNTING_EMAIL')];
    }

    public function sewnByStatus()
    {
        $file = 'sewing_by_status_report_'.Carbon::now()->timestamp.'.xlsx';

        (new SewingByStatusReport(
            \request('begin_date'),
            \request('end_date'),
            \request('status')
        ))->queue('reports/'.$file, 'local')
            ->chain([
                new ReportReadyJob('Sewn Report By Status', $file, env('ACCOUNTING_EMAIL')),
            ]);

        return ['success' => true, 'mail' => env('ACCOUNTING_EMAIL')];
    }

    public function crystal()
    {
        $file = 'crystal_report_'.Carbon::now()->timestamp.'.xlsx';

        (new CrystalReport(
            \request('begin_date'),
            \request('end_date')
        ))->queue('reports/'.$file, 'local')
            ->chain([
                new ReportReadyJob('Crystal Report', $file, env('ACCOUNTING_EMAIL')),
            ]);

        return ['success' => true, 'mail' => env('ACCOUNTING_EMAIL')];
    }

    public function packing()
    {
        $file = 'packing_report_'.Carbon::now()->timestamp.'.xlsx';

        (new PackingReport(
            \request('begin_date'),
            \request('end_date')
        ))->queue('reports/'.$file, 'local')
            ->chain([
                new ReportReadyJob('Packing Report', $file, env('ACCOUNTING_EMAIL')),
            ]);

        return ['success' => true, 'mail' => env('ACCOUNTING_EMAIL')];
    }

    public function finishedSuits()
    {
        $file = 'finished_suits_'.Carbon::now()->timestamp.'.xlsx';

        (new FinishedSuitsReport(
            \request('begin_date'),
            \request('end_date')
        ))->queue('reports/'.$file, 'local')
            ->chain([
                new ReportReadyJob('Finished Suits Report', $file, env('ACCOUNTING_EMAIL')),
            ]);

        return ['success' => true, 'mail' => env('ACCOUNTING_EMAIL')];
    }

    public function finishGoods()
    {
        $file = 'finis_goods_'.Carbon::now()->timestamp.'.xlsx';

        (new FinishGoodReport(
            \request('begin_date'),
            \request('end_date')
        ))->queue('reports/'.$file, 'local')
            ->chain([
                new ReportReadyJob('Finish Good Report', $file, env('ACCOUNTING_EMAIL')),
            ]);

        return ['success' => true, 'mail' => env('ACCOUNTING_EMAIL')];
    }

    public function shipping()
    {
        $file = 'shipping_list_'.Carbon::now()->timestamp.'.xlsx';

        (new ShippingReport(
            \request('begin_date'),
            \request('end_date')
        ))->queue('reports/'.$file, 'local')
            ->chain([
                new ReportReadyJob('Shipping List Report', $file, env('ACCOUNTING_EMAIL')),
            ]);

        return ['success' => true, 'mail' => env('ACCOUNTING_EMAIL')];
    }
}

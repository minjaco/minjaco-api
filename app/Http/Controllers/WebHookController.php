<?php

namespace App\Http\Controllers;

use App\Enums\OrderStatus;
use App\Enums\OrderType;
use App\Enums\PaymentOption;
use App\Events\OrderReceived;
use App\Models\Customer;
use App\Models\Order;
use App\Models\OrderBilling;
use App\Models\OrderProduct;
use App\Models\OrderProductOption;
use App\Models\OrderShipping;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

//https://developer.bigcommerce.com/api-docs/getting-started/webhooks/webhook-events#webhook-events_products
class WebHookController extends Controller
{
    public function __construct()
    {
        config(['app.timezone' => 'GMT']);
    }

    /**
     * Hook for order created
     * @param  Request  $request
     */
    /*public function orderCreated(Request $request)
    {
        $result = $this->_getData($request, 'webhook-order-created');

        if ($result) {
            $order_exist = app(Order::class)->where('order_id', $result['id'])->first();
            if (!$order_exist) {
                $order = \Bigcommerce::getOrder($result['id']);

                if ($order->status === 'Incomplete') {
                    \Log::info('Order created hook received but could not processed');
                    return;
                }

                $id = $order->id;

                $o = [
                    'order_id'            => $id,
                    'minja_id'            => $this->_generateMinjaOrderId($id),
                    'customer_id'         => $order->customer_id,
                    'order_from'          => 'BC-EN',
                    'order_source'        => $order->order_source,
                    'items_total'         => $order->items_total,
                    'country'             => $order->geoip_country,
                    'payment_method'      => $order->payment_method,
                    'payment_option'      => PaymentOption::FULL,
                    'payment_provider_id' => $order->payment_provider_id ?? '',
                    'payment_status'      => $order->payment_status,
                    'external_source'     => $order->external_source,
                    'bc_status_id'        => $order->status_id,
                    'bc_status'           => $order->status, // Order status is 0 (incompleted)
                    'mc_status_id'        => OrderStatus::WATING_FOR_SIZING_INFO,
                    'coupon_discount'     => $order->coupon_discount,
                    'discount_amount'     => $order->discount_amount,
                    'base_shipping_cost'  => $order->base_shipping_cost,
                    'subtotal_inc_tax'    => $order->subtotal_inc_tax,
                    'total_inc_tax'       => $order->total_inc_tax,
                    'customer_message'    => $order->customer_message,
                    'staff_notes'         => $order->staff_notes,
                    'date_created'        => Carbon::parse($order->date_created)->format('Y-m-d H:i:s'),
                    'date_modified'       => Carbon::parse($order->date_modified)->format('Y-m-d H:i:s'),
                    'date_shipped'        => Carbon::parse($order->date_shipped)->format('Y-m-d H:i:s'),
                ];

                $ob               = new OrderBilling();
                $ob->order_id     = $id;
                $ob->first_name   = $order->billing_address->first_name;
                $ob->last_name    = $order->billing_address->last_name;
                $ob->company      = $order->billing_address->company;
                $ob->street_1     = $order->billing_address->street_1;
                $ob->street_2     = $order->billing_address->street_2;
                $ob->city         = $order->billing_address->city;
                $ob->zip          = $order->billing_address->zip;
                $ob->state        = $order->billing_address->state;
                $ob->country      = $order->billing_address->country;
                $ob->country_iso2 = $order->billing_address->country_iso2;
                $ob->phone        = $order->billing_address->phone;
                $ob->email        = $order->billing_address->email;

                $ob->save();

                $o_shipping = collect(\Bigcommerce::getOrderShippingAddresses($id))->first();

                if ($o_shipping) {
                    $os                     = new OrderShipping();
                    $os->order_id           = $o_shipping->order_id;
                    $os->first_name         = $o_shipping->first_name;
                    $os->last_name          = $o_shipping->last_name;
                    $os->company            = $o_shipping->company;
                    $os->street_1           = $o_shipping->street_1;
                    $os->street_2           = $o_shipping->street_2;
                    $os->city               = $o_shipping->city;
                    $os->zip                = $o_shipping->zip;
                    $os->country            = $o_shipping->country;
                    $os->country_iso2       = $o_shipping->country_iso2;
                    $os->state              = $o_shipping->state;
                    $os->phone              = $o_shipping->phone;
                    $os->email              = $o_shipping->email;
                    $os->items_total        = $o_shipping->items_total;
                    $os->shipping_method    = $o_shipping->shipping_method;
                    $os->base_cost          = $o_shipping->base_cost;
                    $os->cost_inc_tax       = $o_shipping->cost_inc_tax;
                    $os->shipping_zone_id   = $o_shipping->shipping_zone_id;
                    $os->shipping_zone_name = $o_shipping->shipping_zone_name;
                    $os->save();
                }

                $products = collect(\Bigcommerce::getOrderProducts($id));

                if ($products) {
                    if ($products->count() > 1) { // check the order has multiple pruducts for linking
                        $o['has_multiple_products'] = 1;
                    }

                    foreach ($products as $product) {
                        $p                    = new OrderProduct();
                        $p->order_id          = $id;
                        $p->product_id        = $product->product_id;
                        $p->name              = $product->name;
                        $p->sku               = $product->sku;
                        $p->total_inc_tax     = $product->total_inc_tax;
                        $p->quantity          = $product->quantity;
                        $p->is_refunded       = $product->is_refunded;
                        $p->quantity_refunded = $product->quantity_refunded;

                        $p->save();

                        foreach ($product->product_options as $option) {
                            $po                = new OrderProductOption();
                            $po->product_id    = $product->product_id;
                            $po->display_name  = $option->display_name;
                            $po->display_value = $option->display_value;
                            $po->name          = $option->name;

                            $po->save();

                            if ($option->name === 'Show Date') {
                                if ($this->_checkIsUrgent($option->display_value)) {
                                    $o['urgent_order'] = OrderType::URGENT;
                                }

                                $o['show_date'] = Carbon::parse($option->display_value)->format('Y-m-d');
                            } elseif ($option->name === 'Payment option' && (int)$option->value === 656) {// 656 stands for partial payment
                                $o['payment_option'] = PaymentOption::PARTIAL;
                            }
                        }
                    }
                }

                app(Order::class)->insertIgnore($o);

                //BigCommerce double order updated fire fix
                $dontDeleteThisRow = app(Order::class)->where('order_id', $id)->first();

                app(Order::class)->where('order_id', $id)->where('id', '!=', $dontDeleteThisRow->id)->delete();
                //End fix

                $order = app(Order::class)->where('order_id', $id)->first();

                broadcast(new OrderReceived($order));

                \Log::info('Order created on minjaco: ' . $result['id']);
            }
        }
    }*/

    /**
     * Hook for order updated
     * @param  Request  $request
     */
    public function orderUpdated(Request $request)
    {
        $result = $this->_getData($request, 'webhook-order-updated');

        if ($result) {
            $order_exist = app(Order::class)->where('order_id', $result['id'])->first();

            $bc_order = \Bigcommerce::getOrder($result['id']);

            if ($order_exist) {

                if ($bc_order->status === 'Incomplete') {
                    \Log::info('Order updated hook received but could not processed');

                    return;
                }

                $id = $bc_order->id;

                $o = [
                    'order_source'        => $bc_order->order_source,
                    'items_total'         => $bc_order->items_total,
                    'country'             => $bc_order->geoip_country,
                    'payment_method'      => $bc_order->payment_method,
                    'payment_provider_id' => $bc_order->payment_provider_id ?? '',
                    'payment_status'      => $bc_order->payment_status,
                    'external_source'     => $bc_order->external_source,
                    'bc_status_id'        => $bc_order->status_id,
                    'bc_status'           => $bc_order->status, // Order status is 0 (incompleted)
                    'coupon_discount'     => $bc_order->coupon_discount,
                    'discount_amount'     => $bc_order->discount_amount,
                    'base_shipping_cost'  => $bc_order->base_shipping_cost,
                    'subtotal_inc_tax'    => $bc_order->subtotal_inc_tax,
                    'total_inc_tax'       => $bc_order->total_inc_tax,
                    'customer_message'    => $bc_order->customer_message,
                    'staff_notes'         => $bc_order->staff_notes,
                    'date_modified'       => Carbon::parse($bc_order->date_modified)->format('Y-m-d H:i:s'),
                ];

                $ob               = app(OrderBilling::class)->where('order_id', $id)->first();
                $ob->first_name   = $bc_order->billing_address->first_name;
                $ob->last_name    = $bc_order->billing_address->last_name;
                $ob->company      = $bc_order->billing_address->company;
                $ob->street_1     = $bc_order->billing_address->street_1;
                $ob->street_2     = $bc_order->billing_address->street_2;
                $ob->city         = $bc_order->billing_address->city;
                $ob->zip          = $bc_order->billing_address->zip;
                $ob->state        = $bc_order->billing_address->state;
                $ob->country      = $bc_order->billing_address->country;
                $ob->country_iso2 = $bc_order->billing_address->country_iso2;
                $ob->phone        = $bc_order->billing_address->phone;
                $ob->email        = $bc_order->billing_address->email;

                $ob->save();

                $o_shipping = collect(\Bigcommerce::getOrderShippingAddresses($id))->first();

                if ($o_shipping) {
                    $os                     = app(OrderShipping::class)->where('order_id', $id)->first();
                    $os->first_name         = $o_shipping->first_name;
                    $os->last_name          = $o_shipping->last_name;
                    $os->company            = $o_shipping->company;
                    $os->street_1           = $o_shipping->street_1;
                    $os->street_2           = $o_shipping->street_2;
                    $os->city               = $o_shipping->city;
                    $os->zip                = $o_shipping->zip;
                    $os->country            = $o_shipping->country;
                    $os->country_iso2       = $o_shipping->country_iso2;
                    $os->state              = $o_shipping->state;
                    $os->phone              = $o_shipping->phone;
                    $os->email              = $o_shipping->email;
                    $os->items_total        = $o_shipping->items_total;
                    $os->shipping_method    = $o_shipping->shipping_method;
                    $os->base_cost          = $o_shipping->base_cost;
                    $os->cost_inc_tax       = $o_shipping->cost_inc_tax;
                    $os->shipping_zone_id   = $o_shipping->shipping_zone_id;
                    $os->shipping_zone_name = $o_shipping->shipping_zone_name;
                    $os->save();
                }

                $products = collect(\Bigcommerce::getOrderProducts($id));

                if ($products) {
                    if ($products->count() > 1) { // check the order has multiple products for linking
                        $o['has_multiple_products'] = 1;
                    }

                    //Has observer to delete options
                    $order_exist->products()->delete();

                    foreach ($products as $product) {

                        $p                    = new OrderProduct();
                        $p->order_id          = $id;
                        $p->product_id        = $product->product_id;
                        $p->name              = $product->name;
                        $p->sku               = $product->sku;
                        $p->total_inc_tax     = $product->total_inc_tax;
                        $p->quantity          = $product->quantity;
                        $p->is_refunded       = $product->is_refunded;
                        $p->quantity_refunded = $product->quantity_refunded;

                        $p->save();

                        foreach ($product->product_options as $option) {
                            $po                = new OrderProductOption();
                            $po->product_id    = $product->product_id;
                            $po->display_name  = $option->display_name;
                            $po->display_value = $option->display_value;
                            $po->name          = $option->name;

                            $po->save();

                            if ($option->name === 'Show Date') {
                                if ($this->_checkIsUrgent($option->display_value)) {
                                    $o['urgent_order'] = OrderType::URGENT;
                                }

                                $o['show_date'] = Carbon::parse($option->display_value)->format('Y-m-d');
                            } elseif ($option->name === 'Payment option' && (int) $option->value === 656) {// 656 stands for partial payment
                                $o['payment_option'] = PaymentOption::PARTIAL;
                            }
                        }
                    }
                }

                app(Order::class)->update($o);

                \Log::info('Order updated on minjaco: '.$result['id']);

            } else {

                $order_exist = app(Order::class)->where('order_id', $result['id'])->first();

                if ($order_exist) {
                    return;
                }

                if ($bc_order) {

                    $id = $bc_order->id;

                    $o = [
                        'order_id'            => $id,
                        'minja_id'            => $this->_generateMinjaOrderId($id),
                        'customer_id'         => $bc_order->customer_id,
                        'order_from'          => 'BC-EN',
                        'order_source'        => $bc_order->order_source,
                        'items_total'         => $bc_order->items_total,
                        'country'             => $bc_order->geoip_country,
                        'payment_method'      => $bc_order->payment_method,
                        'payment_option'      => PaymentOption::FULL,
                        'payment_provider_id' => $bc_order->payment_provider_id ?? '',
                        'payment_status'      => $bc_order->payment_status,
                        'external_source'     => $bc_order->external_source,
                        'bc_status_id'        => $bc_order->status_id,
                        'bc_status'           => $bc_order->status, // Order status is 0 (incompleted)
                        'mc_status_id'        => OrderStatus::WAITING_FOR_SIZING_INFO,
                        'coupon_discount'     => $bc_order->coupon_discount,
                        'discount_amount'     => $bc_order->discount_amount,
                        'base_shipping_cost'  => $bc_order->base_shipping_cost,
                        'subtotal_inc_tax'    => $bc_order->subtotal_inc_tax,
                        'total_inc_tax'       => $bc_order->total_inc_tax,
                        'customer_message'    => $bc_order->customer_message,
                        'staff_notes'         => $bc_order->staff_notes,
                        'date_created'        => Carbon::parse($bc_order->date_created)->format('Y-m-d H:i:s'),
                        'date_modified'       => Carbon::parse($bc_order->date_modified)->format('Y-m-d H:i:s'),
                        'date_shipped'        => Carbon::parse($bc_order->date_shipped)->format('Y-m-d H:i:s'),
                    ];

                    $ob               = new OrderBilling();
                    $ob->order_id     = $id;
                    $ob->first_name   = $bc_order->billing_address->first_name;
                    $ob->last_name    = $bc_order->billing_address->last_name;
                    $ob->company      = $bc_order->billing_address->company;
                    $ob->street_1     = $bc_order->billing_address->street_1;
                    $ob->street_2     = $bc_order->billing_address->street_2;
                    $ob->city         = $bc_order->billing_address->city;
                    $ob->zip          = $bc_order->billing_address->zip;
                    $ob->state        = $bc_order->billing_address->state;
                    $ob->country      = $bc_order->billing_address->country;
                    $ob->country_iso2 = $bc_order->billing_address->country_iso2;
                    $ob->phone        = $bc_order->billing_address->phone;
                    $ob->email        = $bc_order->billing_address->email;

                    $ob->save();

                    $o_shipping = collect(\Bigcommerce::getOrderShippingAddresses($id))->first();

                    if ($o_shipping) {
                        $os                     = new OrderShipping();
                        $os->order_id           = $o_shipping->order_id;
                        $os->first_name         = $o_shipping->first_name;
                        $os->last_name          = $o_shipping->last_name;
                        $os->company            = $o_shipping->company;
                        $os->street_1           = $o_shipping->street_1;
                        $os->street_2           = $o_shipping->street_2;
                        $os->city               = $o_shipping->city;
                        $os->zip                = $o_shipping->zip;
                        $os->country            = $o_shipping->country;
                        $os->country_iso2       = $o_shipping->country_iso2;
                        $os->state              = $o_shipping->state;
                        $os->phone              = $o_shipping->phone;
                        $os->email              = $o_shipping->email;
                        $os->items_total        = $o_shipping->items_total;
                        $os->shipping_method    = $o_shipping->shipping_method;
                        $os->base_cost          = $o_shipping->base_cost;
                        $os->cost_inc_tax       = $o_shipping->cost_inc_tax;
                        $os->shipping_zone_id   = $o_shipping->shipping_zone_id;
                        $os->shipping_zone_name = $o_shipping->shipping_zone_name;
                        $os->save();
                    }

                    $products = collect(\Bigcommerce::getOrderProducts($id));

                    if ($products) {
                        if ($products->count() > 1) { // check the order has multiple products for linking
                            $o['has_multiple_products'] = 1;
                        }

                        foreach ($products as $product) {
                            $p                    = new OrderProduct();
                            $p->order_id          = $id;
                            $p->product_id        = $product->product_id;
                            $p->name              = $product->name;
                            $p->sku               = $product->sku;
                            $p->total_inc_tax     = $product->total_inc_tax;
                            $p->quantity          = $product->quantity;
                            $p->is_refunded       = $product->is_refunded;
                            $p->quantity_refunded = $product->quantity_refunded;

                            $p->save();

                            foreach ($product->product_options as $option) {
                                $po                = new OrderProductOption();
                                $po->product_id    = $product->product_id;
                                $po->display_name  = $option->display_name;
                                $po->display_value = $option->display_value;
                                $po->name          = $option->name;

                                $po->save();

                                if ($option->name === 'Show Date') {
                                    if ($this->_checkIsUrgent($option->display_value)) {
                                        $o['urgent_order'] = OrderType::URGENT;
                                    }

                                    $o['show_date'] = Carbon::parse($option->display_value)->format('Y-m-d');
                                } elseif ($option->name === 'Payment option' && (int) $option->value === 656) {// 656 stands for partial payment
                                    $o['payment_option'] = PaymentOption::PARTIAL;
                                }
                            }
                        }
                    }

                    app(Order::class)->create($o);

                    $order = app(Order::class)->where('order_id', $id)->first();

                    broadcast(new OrderReceived($order));

                    \Log::info('Order created on minjaco: '.$result['id']);
                }

                \Log::info('Order on BC could not be found: '.$result['id']);
            }
        }
    }

    /**
     * Hook for order status updated
     * @param  Request  $request
     */
    public function orderStatusUpdated(Request $request)
    {
        $result = $this->_getData($request, 'webhook-order-status-updated');
        if ($result) {
            \Log::info('Order Status Hook was received order ID: '.$result['id'].'
                        status : '.$result['data']['previous_status_id'].' - '.$result['data']['new_status_id']);

            try {
                $order               = app(Order::class)->where('order_id', $result['id'])->firstOrFail();
                $order->bc_status_id = $result['data']['new_status_id'];
                $order->save();
            } catch (ModelNotFoundException $exception) {
                \Log::error('Order can not be found ID: '.$result['id'].' message : '.$exception->getMessage());
            }
        }
    }

    /**
     * Hook for customer created
     * @param  Request  $request
     */
    public function customerCreated(Request $request)
    {
        $result = $this->_getData($request, 'webhook-customer-created');

        if ($result) {
            $customer = \Bigcommerce::getCustomer($result['id']);
            //Check customer exits
            $check = app(Customer::class)->where('email', $customer->email)->first();

            if ($check) {
                return;
            }

            $c                    = new Customer();
            $c->customer_id       = $customer->id;
            $c->uuid              = Str::uuid();
            $c->name              = $customer->first_name.' '.$customer->last_name;
            $c->phone_number      = $customer->phone;
            $c->email             = $customer->email;
            $c->registered_at     = Carbon::parse($customer->date_created)->format('Y-m-d H:i:s');
            $c->accepts_marketing = $customer->accepts_marketing ? 1 : 0;

            $address = collect(\Bigcommerce::getCustomerAddresses($customer->id))->first();

            if ($address) {
                $c->address = $address->street_1.' '.$address->street_2.' '.$address->city.' '.$address->state.' '.$address->zip;
                $c->country = $address->country;
            }

            $c->save();
            \Log::info('Customer info has been saved Customer ID : '.$result['id']);
        } else {
            \Log::error('Customer ID not received (token not provided)');
        }
    }

    /**
     * Hook for customer created
     * @param  Request  $request
     */
    public function customerUpdated(Request $request)
    {
        $result = $this->_getData($request, 'webhook-customer-updated');

        if ($result) {
            $customer = \Bigcommerce::getCustomer($result['id']);
            //Check customer exits
            $check = app(Customer::class)->where('email', $customer->email)->first();

            if ($check) {
                $check->customer_id       = $customer->id;
                $check->uuid              = Str::uuid();
                $check->name              = $customer->first_name.' '.$customer->last_name;
                $check->phone_number      = $customer->phone;
                $check->email             = $customer->email;
                $check->registered_at     = Carbon::parse($customer->date_created)->format('Y-m-d H:i:s');
                $check->accepts_marketing = $customer->accepts_marketing ? 1 : 0;

                $address = collect(\Bigcommerce::getCustomerAddresses($customer->id))->first();

                if ($address) {
                    $check->address = $address->street_1.' '.$address->street_2.' '.$address->city.' '.$address->state.' '.$address->zip;
                    $check->country = $address->country;
                }

                $check->save();

                \Log::info('Customer info has been updated Customer ID : '.$result['id']);
            }
        } else {
            \Log::error('Customer ID not received (token not provided)');
        }
    }

    /**
     * Hook for product created
     * @param  Request  $request
     */
    public function productCreated(Request $request)
    {
        $result = $this->_getData($request, 'webhook-product-created');

        if ($result) {
            $product = \Bigcommerce::getProduct($result['id']);

            $p              = new Product();
            $p->product_id  = $product->id;
            $p->name        = $product->name;
            $p->sku         = $product->sku;
            $p->price       = $product->price;
            $p->is_visible  = $product->is_visible;
            $p->is_featured = $product->is_featured;
            $p->total_sold  = $product->total_sold;
            $p->view_count  = $product->view_count;
            $p->custom_url  = $product->custom_url;
            $p->image_url   = $product->primary_image->standard_url ?? '';
            $p->description = $product->description;

            $p->save();
        }
    }

    /**
     * Hook for product updated
     * @param  Request  $request
     */
    public function productUpdated(Request $request)
    {
        $result = $this->_getData($request, 'webhook-product-updated');
        if ($result) {
            $product = \Bigcommerce::getProduct($result['id']);

            $p              = app(Product::class)->where('product_id', $product->id)->first();
            $p->name        = $product->name;
            $p->sku         = $product->sku;
            $p->price       = $product->price;
            $p->is_visible  = $product->is_visible;
            $p->is_featured = $product->is_featured;
            $p->total_sold  = $product->total_sold;
            $p->view_count  = $product->view_count;
            $p->custom_url  = $product->custom_url;
            $p->image_url   = $product->primary_image->standard_url ?? '';
            $p->description = $product->description;

            $p->save();

            \Log::info('Product Updated Hook was received BD ID: '.$result['id']);
        }
    }

    /**
     * Checks provided header and gets id of item
     * @param  Request  $request
     * @param  string  $logName
     * @return int
     */
    private function _getData(Request $request, string $logName): array
    {
        try {
            if (
                $request->headers->has('X-MJ-HASH')
                &&
                decrypt($request->header('X-MJ-HASH')) === env('BIGCOMMERCE_WEBHOOK_SECRET')
            ) {
                return ['id' => $request->data['id'], 'data' => $request->data['status'] ?? null];
            }
        } catch (DecryptException $exception) {
            activity($logName)->log($exception->getMessage());
        }

        return 0;
    }

    private function _checkIsUrgent(string $date): bool
    {
        $diff = Carbon::parse($date)->diffInDays(Carbon::now());

        return $diff <= (int) env('MINIMUM_ORDER_DAYS') ?? false;
    }

    private function _generateMinjaOrderId($order_id)
    {
        $order = app(Order::class)->orderBy('minja_id', 'desc')->first();

        return $order->minja_id + 1;
    }
}

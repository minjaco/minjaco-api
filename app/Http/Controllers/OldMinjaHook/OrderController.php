<?php

namespace App\Http\Controllers\OldMinjaHook;

use App\Enums\BigCommerceStatus;
use App\Enums\IsWellness;
use App\Enums\OrderType;
use App\Enums\PaymentOption;
use App\Http\Controllers\Controller;
use App\Models\OldApiBcOrder;
use App\Models\OldApiBcOrderBilling;
use App\Models\OldApiBcOrderIncomplete;
use App\Models\OldApiBcOrderIncompleteProduct;
use App\Models\OldApiBcOrderProduct;
use App\Models\OldApiBcOrderShipping;
use App\Models\OldOrder;
use Carbon\Carbon;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    //Todo: check order product type such as RB etc and update comments. api_bc.php line 449

    private $rb_sku = [
        '663' => 'BK01',
        '664' => 'PK01',
        '665' => 'BL02',
        '666' => 'RD01',
        '667' => 'BL01',
    ];

    private $robes = [
        'RB001' => 'Figure Competitor',
        'RB002' => 'Bikini Diva',
        'RB004' => 'Bridesmaid',
        'RB005' => 'Figure Girl',
        'RB006' => 'Fit mom',
        'RB007' => 'Bikini Competitor',
    ];

    private $urgent_order = 0;
    private $payment_option = PaymentOption::FULL;
    private $is_wellness = IsWellness::NO;
    private $show_date = '';
    private $comment = '';

    /**
     * Handle the incoming request.
     *
     * @param  Request  $request
     * @return void
     */
    public function __invoke(Request $request)
    {
        $result = $this->getData($request, 'webhook-order-updated');

        //$result['id'] = $request->id; // for local dev

        if ($result) {

            $bc_order = \Bigcommerce::getOrder($result['id']);
            $id       = $bc_order->id;

            if ($bc_order->status_id === BigCommerceStatus::INCOMPLETE) {
                $this->createInCompletedOrder($id, $bc_order);

                return;
            }

            $order_exist = app(OldOrder::class)->where('order_id', $result['id'])->first();

            if ($order_exist && $bc_order->is_deleted === 'true') {
                $this->cleanUpOrder($id);
            }

            if ((int) $bc_order->status_id === BigCommerceStatus::BEING_PROCESSED) {

                $this->createApiOrder($id, $bc_order);

                $this->createBillingRecord($id, $bc_order);

                $this->createShippingRecord($id, $bc_order);

                $this->createOrderAndProducts($id, $bc_order);

                $this->lastCheckUp($id);

                \Log::info('Order is received and processed successfully :'.$id);

                //Todo : emailing process
            }
        }
    }

    /**
     * Checks provided header and gets id of item
     * @param  Request  $request
     * @param  string  $logName
     * @return int
     */
    private function getData(Request $request, string $logName): array
    {
        try {
            if (
                $request->headers->has('X-MJ-HASH')
                &&
                decrypt($request->header('X-MJ-HASH')) === env('BIGCOMMERCE_WEBHOOK_SECRET')
            ) {
                return ['id' => $request->data['id'], 'data' => $request->data['status'] ?? null];
            }
        } catch (DecryptException $exception) {
            activity($logName)->log($exception->getMessage());
        }

        return 0;
    }

    /**
     *Api Billing Record
     *
     * @param $id
     * @param $bc_order
     */
    private function createBillingRecord($id, $bc_order): void
    {
        app(OldApiBcOrderBilling::class)->firstOrCreate(
            [
                'bc_id' => $id,
            ],
            [
                'bc_id'             => $id,
                'bill_first_name'   => $bc_order->billing_address->first_name,
                'bill_last_name'    => $bc_order->billing_address->last_name,
                'bill_company'      => $bc_order->billing_address->company,
                'bill_street_1'     => $bc_order->billing_address->street_1,
                'bill_street_2'     => $bc_order->billing_address->street_2,
                'bill_city'         => $bc_order->billing_address->city,
                'bill_state'        => $bc_order->billing_address->state,
                'bill_zip'          => $bc_order->billing_address->zip,
                'bill_country'      => $bc_order->billing_address->country,
                'bill_country_iso2' => $bc_order->billing_address->country_iso2,
                'bill_phone'        => $bc_order->billing_address->phone,
                'bill_email'        => $bc_order->billing_address->email,
            ]
        );
    }

    /**
     * Api Shipping Record
     * @param $id
     * @param $bc_order
     */
    private function createShippingRecord($id, $bc_order): void
    {
        $o_shipping = collect($bc_order->shipping_addresses())->first();

        app(OldApiBcOrderShipping::class)->firstOrCreate(
            [
                'bc_id' => $id,
            ],
            [
                'bc_id'                     => $id,
                'ship_first_name'           => $o_shipping->first_name,
                'ship_last_name'            => $o_shipping->last_name,
                'ship_company'              => $o_shipping->company,
                'ship_street_1'             => $o_shipping->street_1,
                'ship_street_2'             => $o_shipping->street_2,
                'ship_city'                 => $o_shipping->city,
                'ship_zip'                  => $o_shipping->zip,
                'ship_country'              => $o_shipping->country,
                'ship_country_iso2'         => $o_shipping->country_iso2,
                'ship_state'                => $o_shipping->state,
                'ship_email'                => $o_shipping->email,
                'ship_phone'                => $o_shipping->phone,
                'ship_items_total'          => $o_shipping->items_total,
                'ship_method'               => $o_shipping->shipping_method,
                'ship_base_cost'            => $o_shipping->base_cost,
                'ship_cost_ex_tax'          => $o_shipping->cost_ex_tax,
                'ship_base_handling_cost'   => $o_shipping->base_handling_cost,
                'ship_handling_cost_ex_tax' => $o_shipping->handling_cost_ex_tax,
                'ship_zone_id'              => $o_shipping->shipping_zone_id,
                'ship_zone_name'            => $o_shipping->shipping_zone_name,
            ]
        );
    }

    /**
     * Create order products record
     *
     * @param $id
     * @param $bc_order
     * @return array
     */
    private function createOrderAndProducts($id, $bc_order): array
    {
        $products = collect($bc_order->products());

        if ($products && $bc_order->payment_provider_id !== 'NULL') {
            foreach ($products as $product) {
                foreach ($product->product_options as $option) {
                    $this->checkOptions($option);
                }

                app(OldApiBcOrderProduct::class)->firstOrCreate(
                    [
                        'bc_id' => $id,
                        'p_id'  => $product->product_id,
                    ],
                    [
                        'bc_id'               => $id,
                        'p_id'                => $product->product_id,
                        'p_name'              => $product->name,
                        'p_sku'               => $product->sku,
                        'p_type'              => $product->type,
                        'p_base_price'        => $product->base_price,
                        'p_total_ex_tax'      => $product->total_ex_tax,
                        'p_weight'            => $product->weight,
                        'p_quantity'          => $product->quantity,
                        'p_is_refunded'       => (string) $product->is_refunded,
                        'p_quantity_refunded' => $product->quantity_refunded,
                        'p_show_date'         => $this->show_date ?? '',
                        'p_special_request'   => trim($this->comment),
                        'p_payment_option'    => $this->payment_option,
                    ]
                );

                $this->createOrder($id, $bc_order, $product);
            }
        }

        return [];
    }

    /**
     * Creates incomplete api order
     * @param $id
     * @param $bc_order
     */
    private function createInCompletedOrder($id, $bc_order): void
    {
        app(OldApiBcOrderIncomplete::class)->firstOrCreate(
            [
                'bc_id' => $id,
            ],
            [
                'bc_id'               => $id,
                'in_order_date'       => date('Y-m-d', strtotime($bc_order->date_created)),
                'in_customer_message' => $bc_order->customer_message,
                'in_first_name'       => $bc_order->billing_address->first_name,
                'in_last_name'        => $bc_order->billing_address->last_name,
                'in_country'          => $bc_order->billing_address->country,
                'in_country_iso2'     => $bc_order->billing_address->country_iso2,
                'in_phone'            => $bc_order->billing_address->phone,
                'in_email'            => $bc_order->billing_address->email,
                'in_status'           => 0,
                'in_comment'          => '',
                'in_follow_up'        => '',
                'in_staff'            => 'Web Hook',
                'in_order_from'       => 'BC',
                'in_user_name'        => '',
                'in_status_edit'      => 0,
            ]
        );

        $products = collect($bc_order->products());

        if ($products) {
            foreach ($products as $product) {
                foreach ($product->product_options as $option) {
                    $this->checkOptions($option);
                }

                app(OldApiBcOrderIncompleteProduct::class)->firstOrCreate(
                    [
                        'bc_id'          => $id,
                        'in_product_sku' => $product->sku,
                    ],
                    [
                        'bc_id'              => $id,
                        'in_product_sku'     => $product->sku,
                        'in_show_date'       => $this->show_date,
                        'in_product_comment' => trim($this->comment),
                        'in_payment_option'  => $this->payment_option,
                    ]
                );
            }
        }
    }

    /**
     * Cleanups incomplete order
     * @param $id
     */
    private function cleanUpOrder($id): void
    {
        app(OldApiBcOrderIncomplete::class)->where('bc_id', $id)->delete();
        app(OldApiBcOrderIncompleteProduct::class)->where('bc_id', $id)->delete();
    }

    /**
     * Create tb_orders record
     * @param $id
     * @param $bc_order
     * @param $product
     */
    private function createOrder($id, $bc_order, $product): void
    {
        [$key1, $key2] = $this->determineKeys($product);

        $order_type = $this->orderType($key1, $key2);

        $status = $this->determineOrderStatusByOrderType($order_type);

        app(OldOrder::class)->firstOrCreate(
            [
                'order_ref_no' => $id,
                'product_code' => $product->sku,
            ],
            [
                'order_id'          => $this->generateMinjaOrderId(),
                'id_encode'         => '',
                'order_from'        => 'BC',
                'order_ref_no'      => $id,
                'client_name'       => $bc_order->billing_address->first_name,
                'client_last_name'  => $bc_order->billing_address->last_name,
                'client_email'      => $bc_order->billing_address->email,
                'urgent_order'      => $this->urgent_order,
                'product_code'      => $product->sku,
                'order_type'        => $order_type,
                'order_type_ref_id' => '',
                'order_remake_type' => '',
                'order_date'        => date('Y-m-d', strtotime($bc_order->date_created)),
                'show_date'         => $this->show_date,
                'ship_date'         => '',
                'client_note'       => trim($this->comment.' '.$bc_order->customer_message),
                'staff_note'        => $bc_order->staff_notes,
                'sub_total'         => $product->base_price,
                'shipping_cost'     => $bc_order->base_shipping_cost,
                'grand_total'       => $product->total_ex_tax,
                'status_id'         => $status,
                'client_email_nd'   => '',
                'payment_option'    => $this->payment_option,
                'time_stamp'        => Carbon::now(),
                'is_wellness'       => $this->is_wellness,
            ]
        );
    }

    /**
     * Determines order type by keys
     * @param $productAbbreviation
     * @param $identifier
     * @return mixed
     */
    private function orderType($productAbbreviation, $identifier)
    {
        $sel_product                = [];
        $sel_product['CD']['0']     = '22';
        $sel_product['CB']['0']     = '1';
        $sel_product['CB']['1']     = '2';
        $sel_product['FS']['0']     = '3';
        $sel_product['FS']['1']     = '15';
        $sel_product['CON']['0']    = '4';
        $sel_product['CBRM']['0']   = '5';
        $sel_product['FSRM']['0']   = '6';
        $sel_product['Fabric']['0'] = '12';
        $sel_product['RB']['0']     = '13';
        $sel_product['DV']['0']     = '20';
        $sel_product['SH']['0']     = '21';
        $sel_product['ShipUp']['0'] = '19';
        $sel_product['GIFT']['0']   = '18';
        $sel_product['PB']['0']     = '23';
        $sel_product['PF']['0']     = '25';
        $sel_product['CJ']['0']     = '26';
        $sel_product['CJ']['1']     = '26';
        $sel_product['TR']['0']     = '28';
        $sel_product['AC']['0']     = '34';
        $sel_product['RUSH']['0']   = '29';

        return $sel_product[$productAbbreviation][$identifier];
    }

    /**
     * Predefined order status
     * @param $orderType
     * @return int|null
     */
    private function determineOrderStatusByOrderType($orderType): ?int
    {
        switch ($orderType) {
            case '4':
            case '5':
            case '6':
            case '12':
            case '13':
            case '21':
            case '26':
            case '27':
            case '28':
                return 4;
                break;
            case '18':
            case '19':
                return 22;
                break;
            default:
                return 1;
                break;
        }
    }

    /**
     * Determines keys for orderType
     * @param $product
     * @return array
     */
    private function determineKeys($product): array
    {
        $key1 = '';
        $key2 = 0;

        if ($product->type !== 'giftcertificate') {
            $bc_code = $product->sku;
            $p_code  = substr($bc_code, 0, 2);
            $pc_ex   = explode('-', $bc_code.'-');
            if ($this->checkCode($p_code)) {
                $key1 = $p_code;
                if (isset($pc_ex[1]) && ($pc_ex[1] !== '' && $pc_ex[1] !== 'RM')) {
                    if ($pc_ex[1] === 'DEPOSIT' || $pc_ex[1] === 'FULL') {
                        $key2 = 0;
                    } else {
                        $key2 = 1;
                    }
                } elseif ((isset($pc_ex[1]) && $pc_ex[1] === 'RM')
                    || (isset($pc_ex[2]) && $pc_ex[2] === 'RM')) {
                    $key1 = $p_code.$pc_ex[1];
                    $key2 = 0;
                } else {
                    $key2 = 0;
                }
            } elseif ($p_code === 'CO') {
                $key1 = 'CON';
            } elseif ($p_code === 'TR') {
                $key1 = 'TR';
            } elseif ($p_code === 'AC') {
                $key1 = 'AC';
            } elseif ($p_code === 'RU') {
                $key1 = 'RUSH';
            } else {
                $key1 = $pc_ex[0];
            }
        } else {
            $key1 = 'GIFT';
        }

        return [$key1, $key2];
    }

    /**
     * Check product code in products array
     * @param  string  $p_code
     * @return mixed
     */
    private function checkCode(string $p_code)
    {
        return in_array($p_code, ['CB', 'FS', 'DV', 'SH', 'CD', 'RB', 'PB', 'PF', 'CJ', 'TR']);
    }

    /**
     * Checks product options and generates necessary values
     * @param $option
     * @return void
     */
    private function checkOptions($option): void
    {
        if ($option->name === 'Show Date') {
            if ($this->checkIsUrgent($option->display_value)) {
                $this->urgent_order = OrderType::URGENT;
            }

            $this->show_date = Carbon::parse($option->display_value)->format('Y-m-d');
        }

        if ($option->name === 'Payment option' && (int) $option->value === 656) {// 656 stands for partial payment
            $this->payment_option = PaymentOption::PARTIAL;
        }

        if ($option->name === 'Special Request') {
            $this->comment = $option->display_value;
        }

        if ($option->name === 'Fabric Swatches') {
            $this->comment .= $option->display_value;
        }

        if ($option->name === 'Is Wellness Suit' && (int) $option->value === 929) {
            $this->is_wellness = IsWellness::YES;
        }

        if ($option->name === 'Fabric - Posing practice bikini') {
            $this->comment .= $option->display_value;
        }

        if ($option->name === 'Edge decoration') {
            $this->comment .= ', Want trim decorated? : '.$option->display_value;
        }
    }

    /**
     * Check order is urgent
     * @param  string  $date
     * @return bool
     */
    private function checkIsUrgent(string $date): bool
    {
        $diff = Carbon::parse($date)->diffInDays(Carbon::now());

        return $diff <= (int) env('MINIMUM_ORDER_DAYS') ?? false;
    }

    /**
     * Generates minja id
     * @return int
     */
    private function generateMinjaOrderId(): int
    {
        $order     = app(OldOrder::class)->select('order_id')->orderBy('order_id', 'desc')->first();
        $converted = (int) substr(($order->order_id + 1), -4, 4);

        return (int) date('y').sprintf('%04d', $converted);
    }

    /**
     * Creates api order record
     * @param $id
     * @param $bc_order
     */
    private function createApiOrder($id, $bc_order): void
    {
        app(OldApiBcOrder::class)->firstOrCreate(
            [
                'bc_id' => $id,
            ],
            [
                'bc_id'               => $id,
                'customer_id'         => $bc_order->customer_id,
                'status_id'           => $bc_order->status_id,
                'subtotal_ex_tax'     => $bc_order->subtotal_ex_tax,
                'base_shipping_cost'  => $bc_order->base_shipping_cost,
                'total_ex_tax'        => $bc_order->total_ex_tax,
                'items_total'         => $bc_order->items_total,
                'payment_method'      => $bc_order->payment_method,
                'payment_provider_id' => $bc_order->payment_provider_id,
                'geoip_country'       => $bc_order->geoip_country,
                'geoip_country_iso2'  => $bc_order->geoip_country_iso2,
                'customer_message'    => $bc_order->customer_message,
                'gift_amount'         => $bc_order->gift_certificate_amount,
                'coupon_discount'     => $bc_order->coupon_discount,
                'coupons'             => $this->getCoupon($bc_order),
                'order_date'          => date('Y-m-d', strtotime($bc_order->date_created)),
                'time_stamp'          => Carbon::now(),
            ]
        );
    }

    /**
     * @param $id
     * @return mixed
     */
    private function lastCheckUp($id): void
    {
        $orders = app(OldOrder::class)
            ->where('order_ref_no', $id)
            ->get();

        if ($orders->contains('payment_option', 1)) {
            foreach ($orders as $order) {
                $order->update(['payment_option' => 1]);
            }
        }
    }

    /**
     * Get coupon if provided
     * @param $bc_order
     * @return mixed
     */
    private function getCoupon($bc_order): string
    {
        $coupons = collect($bc_order->coupons());

        if (!$coupons->isEmpty()) {
            return $coupons->first()->code;
        }

        return '';
    }
}

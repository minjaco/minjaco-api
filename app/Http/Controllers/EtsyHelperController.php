<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Gentor\Etsy\Facades\Etsy;
use Illuminate\Http\Request;

class EtsyHelperController extends Controller
{
    public function index()
    {
        // The $callbackUrl is the url of your app where Etsy sends the data needed for getting token credentials
        $callbackUrl = 'http://minjaco-api.test/etsy/callback';

        // The $authorizationUrl is the Etsy url where the user approves your app
        $authorizationUrl = Etsy::authorize($callbackUrl);

        return redirect($authorizationUrl);
    }

    public function store(Request $request)
    {
        // On the callback endpoint run this code to get the token credentials and add them to your config
        $tokenCredentials = Etsy::approve($request->get('oauth_token'), $request->get('oauth_verifier'));

        $tokens = [
            'access_token'        => $tokenCredentials->getIdentifier(),
            'access_token_secret' => $tokenCredentials->getSecret(),
        ];

        dd($tokens);
    }

    public function data()
    {
        $args = [
            'params'       => [
                'receipt_id' => 1364661379,
            ],
            'associations' => [
                'Transactions',
                'Country',
                'Buyer',
            ],
        ];

        $data = Etsy::getShop_Receipt2($args);

        dd($data);
    }

    /*public function data()
    {
        $args = [
            'params'       => [
                'shop_id'     => env('ETSY_SHOP_ID'),
                //'min_created' => Carbon::now()->subDays(12)->timestamp,
                //'limit'       => env('ETSY_LIMIT'), //max 100
                'limit'       => 100,
            ],
            'associations' => [
                'Transactions',
                'Country',
                'Buyer',
            ],
        ];

        $data = Etsy::findAllShopReceipts($args);


        $args = [
            'params'       => [
                'user_id' => $data['results'][0]['buyer_user_id'],
            ],
            'associations' => [
                'Profile',
            ],
        ];

        $customer = Etsy::getUser($args);

        dd($data, $customer);

        /*$args = [
            'params'          => [
                'receipt_id' => 1453916585,
            ], 'associations' => [
                'Transactions',
                'Country',
                'Buyer',
            ],
        ];

        $data = Etsy::getShop_Receipt2($args);

        dd($data);*/
    //}
}

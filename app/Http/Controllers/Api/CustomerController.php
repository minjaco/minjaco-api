<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\BasicCustomerResource;
use App\Http\Resources\CustomerResource;
use App\Models\Customer;
use App\Models\CustomerExtraInformation;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return array|\Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        if (!$request->sort) {
            return [];
        }
        $sort = explode('|', $request->sort);
        if ($request->filter) {
            $filter = trim($request->filter);
            $date   = explode('&', $filter);
            if (count($date) === 2) {
                $customers = app(Customer::class)
                    ->with('extra_info')
                    ->whereBetween('registered_at', [$date[0], $date[1]])
                    ->orderBy($sort[0], $sort[1])
                    ->paginate((int) $request->per_page);
            } else {
                $customers = app(Customer::class)
                    ->with('extra_info')
                    ->where('email', 'LIKE', '%'.$filter.'%')
                    ->orWhere('name', 'LIKE', '%'.$filter.'%')
                    ->orderBy($sort[0], $sort[1])
                    ->paginate((int) $request->per_page);
            }
        } else {
            $customers = app(Customer::class)
                ->with('extra_info')
                ->orderBy($sort[0], $sort[1])
                ->paginate((int) $request->per_page);
        }

        return BasicCustomerResource::collection($customers);
    }


    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \App\Http\Resources\CustomerResource
     */
    public function show($id)
    {
        $customer = app(Customer::class)
            ->where('customer_id', $id)
            ->with(['orders.products', 'orders.size'])
            ->firstOrFail();

        \Cache::remember(
            md5($customer->email).'-active-campaign-data',
            env('LONG_TTL'), // one day cache
            static function () use ($customer) {
                $contact = \AC::contactViewByEmail($customer->email);

                if ($contact) {
                    $fields = $contact->fields;

                    $customer->federations           = $fields->{10}->val;
                    $customer->next_competition_date = $fields->{11}->val;
                    $customer->competed_before       = $fields->{12}->val;
                    $customer->facebook              = $fields->{17}->val;
                    $customer->instagram             = $fields->{18}->val;
                    $customer->division              = $fields->{31}->val;
                    $customer->linkedin              = $fields->{39}->val;
                    $customer->website               = $fields->{40}->val;
                    $customer->profile               = $fields->{41}->val;
                    $customer->team_name             = $fields->{42}->val;
                    $customer->coach_name            = $fields->{43}->val;
                    $customer->is_team_leader        = $fields->{44}->val === 'Yes' ? 1 : 0;
                    $customer->is_coach              = $fields->{45}->val === 'Yes' ? 1 : 0;

                    $customer->save();
                }
            }
        );

        $customer = \Cache::remember(
            md5($customer->id).'-customer-data',
            env('LONG_TTL'), // one day cache
            static function () use ($customer) {
                return CustomerResource::make($customer)->hide(['activities']);
            }
        );

        return $customer;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param $id
     *
     * @return \App\Http\Resources\CustomerResource|\Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $customer = app(Customer::class)->where('customer_id', $id)->firstOrFail();

        $data = [
            'email'       => $customer->email,
            'field[10,0]' => $request->federations,
            'field[11,0]' => $request->next_competition_date,
            'field[12,0]' => $request->competed_before,
            'field[17,0]' => $request->facebook,
            'field[18,0]' => $request->instagram,
            'field[31,0]' => $request->division,
            'field[39,0]' => $request->linkedin,
            'field[40,0]' => $request->website,
            'field[41,0]' => $request->profile,
            'field[42,0]' => $request->team_name,
            'field[43,0]' => $request->coach_name,
            'field[44,0]' => $request->is_team_leader ? 'Yes' : 'No',
            'field[45,0]' => $request->is_coach ? 'Yes' : 'No',
        ];

        $synced = \AC::contactSync($data);

        if ($synced) {
            $customer->update(
                $request->except(
                    [
                        'customer_id',
                        'emails',
                        'name',
                        'orders',
                        'registered_at',
                        'is_team_leader',
                        'is_coach',
                    ]
                ) +
                [
                    'is_team_leader' => $request->is_team_leader ? 1 : 0,
                    'is_coach'       => $request->is_coach ? 1 : 0,
                ]
            );


            $emails = app(CustomerExtraInformation::class)
                ->where('customer_id', $id)
                ->where('title', 'email')
                ->get();

            foreach ($emails as $email) {
                $email->delete();
            }

            foreach ($request->emails as $key => $value) {
                if ($key > 0) {
                    app(CustomerExtraInformation::class)->create([
                        'customer_id' => $id,
                        'title'       => 'email',
                        'value'       => $value,
                    ]);
                }
            }

            \Cache::forget(md5($customer->email).'-active-campaign-data');

            $customer = $customer->load(['orders.products']);

            return new CustomerResource($customer);
        }

        return response()->json(['status' => 'error', 'message' => 'Active Campaign sync problem'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return void
     */
    public function destroy($id)
    {
        //
    }
}

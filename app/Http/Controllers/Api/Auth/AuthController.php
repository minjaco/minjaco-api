<?php

    namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    /**
     * Create user
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $request->validate(
            [
                    'name'     => 'required|string',
                    'email'    => 'required|string|email|unique:users',
                    'password' => 'required|string',
                ]
            );
        $user = new User(
            [
                    'uuid'     => Str::uuid(),
                    'name'     => $request->name,
                    'email'    => $request->email,
                    'password' => bcrypt($request->password),
                ]
            );
        $user->save();

        return response()->json(
            [
                    'message' => 'Successfully created user!',
                ],
            201
            );
    }

    /**
     * Login user and create token
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $request->validate(
            [
                    'email'       => 'required|string|email',
                    'password'    => 'required|string',
                    'remember_me' => 'boolean',
                ]
            );
        $credentials = request(['email', 'password']);
        if (!\Auth::attempt($credentials)) {
            return response()->json(
                [
                        'message' => 'Unauthorized',
                    ],
                401
                );
        }
        $user        = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token       = $tokenResult->token;

        $token->save();

        return response()->json(
            [
                    'access_token' => $tokenResult->accessToken,
                    'token_type'   => 'Bearer',
                    'expires_at'   => Carbon::parse(
                        $tokenResult->token->expires_at
                    )->toDateTimeString(),
                ]
            );
    }

    /**
     * Logout user (Revoke the token)
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return response()->json(
            [
                    'message' => 'Successfully logged out',
                ]
            );
    }

    /**
     * Get the authenticated User
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Http\Resources\UserResource [json] user object
     */
    public function user(Request $request)
    {
        return new UserResource($request->user());
    }
}

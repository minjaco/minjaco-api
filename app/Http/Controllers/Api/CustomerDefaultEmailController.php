<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\CustomerResource;
use App\Models\Customer;
use App\Models\CustomerExtraInformation;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CustomerDefaultEmailController extends Controller
{


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return CustomerResource
     */
    public function update(Request $request, $id)
    {
        try {
            $data = app(CustomerExtraInformation::class)
                ->where('customer_id', $id)
                ->get();

            foreach ($data as $d) {
                $d->is_default = 0;

                if ($d->value === $request->email) {
                    $d->is_default = 1;
                }
                $d->save();
            }
            $customer = app(Customer::class)->where('customer_id', $id)->with(['orders.products'])->firstOrFail();

            \Cache::forget(md5($customer->email) . '-active-campaign-data');

            return new CustomerResource($customer);

        } catch (ModelNotFoundException $exception) {
            return response()->json(['status' => 'error', 'message' => $exception->getMessage()]);
        }
    }

}

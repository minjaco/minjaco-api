<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class OrderHelperController extends Controller
{
    public function replicate(Request $request)
    {
        /*//https://stackoverflow.com/questions/36021239/laravel-eloquent-orm-replicate

        try {
            $order = app(Order::class)->where('minja_id', $request->order_id)->firstOrFail();

            \DB::beginTransaction();

            $order->load('products', 'billings', 'shipping', 'infoReceived', 'determineSize'); // Tim tablolar dolu olunca calisacak

            $newOrder = $order->replicate();
            $newOrder->save();

            foreach ($order->getRelations() as $relation => $items) {
                foreach ($items as $item) {
                    unset($item->id);
                    $newOrder->{$relation}()->create($item->toArray());
                }
            }

            \DB::commit();

            return response()->json(['status' => 'success', 'new_order_id' => $newOrder->id]);
        } catch (ModelNotFoundException $exception) {
            \DB::rollBack();
            return response()->json(['status' => 'error', 'message' => $exception->getMessage()]);
        }*/


        try {
            $order = app(Order::class)->where('minja_id', $request->order_id)->firstOrFail();
            $duplicatedModel = $order->saveAsDuplicate();// returns the newly duplicated model instance
        } catch (ModelNotFoundException $exception) {
            return response()->json(['status' => 'error', 'message' => $exception->getMessage()]);
        }

        return response()->json(['status' => 'success', 'new_order_id' => $duplicatedModel->id]);
    }
}

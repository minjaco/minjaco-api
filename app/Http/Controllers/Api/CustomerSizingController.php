<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\CustomerSizing;
use Illuminate\Http\Request;

class CustomerSizingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\CustomerSizing $customerSizing
     * @return void
     */
    public function show(CustomerSizing $customerSizing)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\CustomerSizing $customerSizing
     * @return void
     */
    public function edit(CustomerSizing $customerSizing)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param \App\Models\CustomerSizing $customerSizing
     * @return void
     */
    public function update(Request $request, CustomerSizing $customerSizing)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\CustomerSizing $customerSizing
     * @return void
     */
    public function destroy(CustomerSizing $customerSizing)
    {
        //
    }
}

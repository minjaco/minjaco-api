<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\BasicOrderResource;
use App\Http\Resources\OrderDetailResource;
use App\Models\Order;
use App\Models\Status;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        if (!$request->sort) {
            return [];
        }
        // Check sort parameter
        $sort    = explode('|', $request->sort);
        $filter  = trim($request->filter);
        $filter2 = trim($request->filter2);
        if ($filter2) {
            $orders = app(Order::class)
                ->has('customer')
                ->with(
                    [
                        'customer:customer_id,name',
                        'status',
                    ]
                )
                ->where('bc_status', '!=', 'Incomplete')
                ->where('mc_status_id', $request->filter2)
                ->orderBy($sort[0], $sort[1])
                ->paginate((int) $request->per_page);
        } elseif ($filter) {
            // Check date parameters
            $date = explode('&', $filter);
            if (count($date) === 2) {
                // Query by date parameters
                $orders = app(Order::class)
                    ->with(
                        [
                            'customer:customer_id,name',
                            'status',
                        ]
                    )
                    ->where('bc_status', '!=', 'Incomplete')
                    ->whereBetween('date_created', [$date[0], $date[1]])
                    ->orderBy($sort[0], $sort[1])
                    ->paginate((int) $request->per_page);
            } else {
                $orders = app(Order::class)
                    ->whereHas(
                        'customer',
                        function ($query) use ($filter) {
                            $query
                                ->where('name', 'LIKE', '%'.$filter.'%')
                                ->orWhere('email', 'LIKE', '%'.$filter.'%');
                        }
                    )
                    ->with(
                        [
                            'customer:customer_id,name',
                            'status',
                        ]
                    )
                    ->where('bc_status', '!=', 'Incomplete')
                    ->orWhere('order_id', 'LIKE', '%'.$filter.'%')
                    ->orWhere('minja_id', 'LIKE', '%'.$filter.'%')
                    ->orderBy($sort[0], $sort[1])
                    ->paginate((int) $request->per_page);
            }
        } else {
            $orders = app(Order::class)
                ->has('customer')
                ->with(
                    [
                        'customer:customer_id,name',
                        'status',
                    ]
                )
                ->where('bc_status', '!=', 'Incomplete')
                ->orderBy($sort[0], $sort[1])
                ->paginate((int) $request->per_page);
        }

        return BasicOrderResource::collection($orders);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \App\Http\Resources\OrderDetailResource
     */
    public function show($id)
    {

        $order = app(Order::class)
            ->where('minja_id', $id)
            ->with(['status', 'notes.user.department', 'products', 'swatches.user', 'customer.orders'])
            ->firstOrFail();

        activity('order-activity')
            ->by(auth()->user())
            ->on($order)
            ->withProperties(
                [
                    'order_id' => $order->minja_id,
                    'type'     => 'seen',
                ]
            )
            ->log('Order ##LINK## has been seen');

        return new OrderDetailResource($order);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return void
     */
    public function destroy(Order $order)
    {
        //
    }
}

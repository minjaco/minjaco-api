<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\OrderCollection;
use App\Models\Order;

class OrdersOfTodayController extends Controller
{
    public function index()
    {
        $orders = app(Order::class)
            ->with(['customer'])
            ->today()
            ->where('bc_status', '!=', 'Incomplete')
            ->get();
        return new OrderCollection($orders);
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Events\OrderActivityCreated;
use App\Http\Controllers\Controller;
use App\Http\Resources\OrderDetailResource;
use App\Models\Order;
use Illuminate\Http\Request;
use Spatie\Activitylog\Models\Activity;

class OrderStatusController extends Controller
{
    public function update(Request $request, $id)
    {
        $order                   = app(Order::class)->where('minja_id', $id)->firstOrFail();
        $order->mc_old_status_id = $order->getOriginal('mc_status_id');
        $order->mc_status_id     = $request->mc_status_id;
        $order->save();

        $lastLoggedActivity = Activity::all()->last();

        broadcast(new OrderActivityCreated($lastLoggedActivity))->toOthers();

        return new OrderDetailResource($order);
    }
}

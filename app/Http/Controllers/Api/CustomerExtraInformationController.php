<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CustomerResource;
use App\Models\Customer;
use App\Models\CustomerExtraInformation;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class CustomerExtraInformationController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $id
     * @return \App\Http\Resources\CustomerResource|\Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $data = app(CustomerExtraInformation::class)
                ->where('customer_id', $id)
                ->where('value', $request->email)
                ->firstOrFail();

            $data->delete();

            $customer = app(Customer::class)->where('customer_id', $id)->with(['orders.products'])->firstOrFail();

            \Cache::forget(md5($customer->email) . '-active-campaign-data');

            return new CustomerResource($customer);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['status' => 'error', 'message' => $exception->getMessage()]);
        }
    }
}

<?php

namespace App\Http\Controllers\Api\CustomerService;

use App\Http\Controllers\Controller;
use App\Http\Resources\CalenderEventResource;
use Spatie\GoogleCalendar\Event;

class CalenderController extends Controller
{
    /**
     * User calender events by user email
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function events()
    {
        $calender_id = auth()->user()->email;

        $events = \Cache::remember(
            md5($calender_id) . '-events',
            5,
            static function () use ($calender_id) {
                return Event::get(\Carbon\Carbon::now(), \Carbon\Carbon::now()->addDays(7), [], $calender_id);
            }
        );

        return CalenderEventResource::collection($events)
            ->additional(
                [
                    'event_count' => $events->count(),
                ]
            );
    }
}

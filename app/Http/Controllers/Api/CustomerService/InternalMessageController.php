<?php

namespace App\Http\Controllers\Api\CustomerService;

use App\Enums\DashboardMessageType;
use App\Events\InternalMessageSent;
use App\Events\InternalMessageToUserSent;
use App\Http\Controllers\Controller;
use App\Http\Resources\MessageResource;
use App\Models\DashboardMessage;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class InternalMessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $internalMessages = \Cache::tags(['internal-messages'])->remember(
            auth()->user()->getRoleNames()->first() . '-internal-messages',
            env('LONG_TTL'),
            static function () {
                $internalMessages = app(DashboardMessage::class)
                    ->with('user')
                    ->where('type', DashboardMessageType::DEPARTMENT)
                    ->whereIn('group', [auth()->user()->getRoleNames()])
                    ->latest()
                    ->get();

                return $internalMessages;
            }
        );

        return MessageResource::collection($internalMessages);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \App\Http\Resources\MessageResource|\Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $message = app(DashboardMessage::class)
            ->create(
                [
                    'user_id' => auth()->user()->id,
                    'to'      => $request->user_id,
                    'group'   => auth()->user()->getRoleNames()->first(),
                    'type'    => DashboardMessageType::DEPARTMENT,
                    'content' => $request->content,
                ]
            );

        \Cache::tags(['internal-messages'])->flush();

        if ($request->user_id === 0) {
            broadcast(new InternalMessageSent(auth()->user(), $message))->toOthers();
        } else {
            broadcast(new InternalMessageToUserSent(auth()->user(), $message))->toOthers();
        }

        return new MessageResource($message->load('user'));
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function show($id)
    {
        try {
            $message = app(DashboardMessage::class)->findOrFail($id);
            $replies = $message->threads;
        } catch (ModelNotFoundException $exception) {
            return response()->json(['status' => 'error', 'message' => $exception->getMessage()]);
        }

        return MessageResource::collection($replies);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return void
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\Api\CustomerService;

use App\Events\OrderActivityCreated;
use App\Http\Controllers\Controller;
use App\Models\OrderNote;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Spatie\Activitylog\Models\Activity;

class OrderNoteController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $note = app(OrderNote::class)
            ->create($request->all() + ['user_id' => auth()->user()->id]);

        $lastLoggedActivity = Activity::all()->last();
        broadcast(new OrderActivityCreated($lastLoggedActivity));

        return response()->json(['status' => 'success', 'note' => $note->load('user')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $note = app(OrderNote::class)->findOrFail($id);
            $note->delete();

            $lastLoggedActivity = Activity::all()->last();

            broadcast(new OrderActivityCreated($lastLoggedActivity));

            return response()->json(['status' => 'success', 'note' => $note->load('user')]);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['status' => 'error', 'message' => $exception->getMessage()]);
        }
    }
}

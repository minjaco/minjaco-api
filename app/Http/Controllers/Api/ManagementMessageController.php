<?php

namespace App\Http\Controllers\Api;

use App\Enums\DashboardMessageType;
use App\Http\Controllers\Controller;
use App\Http\Resources\MessageResource;
use App\Models\DashboardMessage;
use Illuminate\Http\Request;

class ManagementMessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $managementMessages = \Cache::tags(['management-messages'])->remember(
            auth()->user()->getRoleNames()->first() . '-management-messages',
            env('LONG_TTL'),
            static function () {
                $managementMessages = app(DashboardMessage::class)
                    ->with('user')
                    ->where('type', DashboardMessageType::MANAGEMENT)
                    ->whereIn('group', [auth()->user()->getRoleNames(), 'everybody'])
                    ->latest()
                    ->get();

                return $managementMessages;
            }
        );

        return MessageResource::collection($managementMessages);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Enums\PatternPlace;
use App\Http\Controllers\Controller;
use App\Http\Resources\DeterminedSizingResource;
use App\Http\Resources\SizingResource;
use App\Models\DetermineSize;
use App\Models\Fabric;
use App\Models\InfoReceived;
use Illuminate\Http\Request;

class SizingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $determine_size = app(DetermineSize::class)
            ->where('order_id', $request->order_id)
            ->first();

        if (!$determine_size) { // Update existing record
            $determine_size = new DetermineSize();
        }

        $fabric = app(Fabric::class)->where('fabric_old_code', $request->order_fabric)->first();

        $determine_size->order_id             = $request->order_id;
        $determine_size->customer_id          = $request->customer_id;
        $determine_size->current_w            = $request->currentWeight;
        $determine_size->expected_w           = $request->expectedWeight;
        $determine_size->height               = $request->currentHeight;
        $determine_size->bra_cup_size         = $request->bra_cup;
        $determine_size->m_bra_cup_size       = $request->minja_cup_size ?? $request->customer_minja_cup_size;
        $determine_size->top_style            = $request->order_top_style;
        $determine_size->bottom_style         = $request->order_bottom_style;
        $determine_size->coverage_level       = $request->order_coverage_level;
        $determine_size->top_size             = $request->size_top;
        $determine_size->waistline            = $request->size_waistline;
        $determine_size->top_comment          = '';
        $determine_size->extra_padding        = 1;
        $determine_size->bottom_front_size_no = $request->sewing_bottom_front_combination;
        $determine_size->bottom_front_letter  = $request->sewing_bottom_front_letter;
        $determine_size->bottom_front_comment = '';
        $determine_size->bottom_back_size_no  = $request->sewing_bottom_back_combination;
        $determine_size->bottom_back_letter   = $request->sewing_bottom_back_letter;
        $determine_size->bottom_back_comment  = '';
        $determine_size->cco_fabric           = $fabric->fabric_id;
        $determine_size->con_top              = $request->order_top_connector;
        $determine_size->con_mid              = $request->order_middle_connector;
        $determine_size->con_bot              = $request->order_bottom_connector;
        $determine_size->gap_between_cups     = 0;
        $determine_size->special_instruction  = '';
        $determine_size->implants             = $request->implant;
        $determine_size->ok_rm                = 1;
        $determine_size->is_new               = 1;

        $determine_size->save();

        $determine_size->sewingModifications()->delete();

        foreach ($request->sewing_top_modifications as $tm) {
            $determine_size->sewingModifications->create([
                'place'        => PatternPlace::TOP,
                'modification' => $tm,
            ]);
        }

        foreach ($request->sewing_bottom_front_modifications as $bfm) {
            $determine_size->sewingModifications->create([
                'place'        => PatternPlace::BOTTOM_FRONT,
                'modification' => $bfm,
            ]);
        }

        foreach ($request->sewing_bottom_back_modifications as $bbm) {
            $determine_size->sewingModifications->create([
                'place'        => PatternPlace::BOTTOM_BACK,
                'modification' => $bbm,
            ]);
        }

        return ['status' => true];
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return DeterminedSizingResource|SizingResource
     */
    public function show($id)
    {

        //todo : important implementation
        //todo : duplicate sizing info

        /*$info = app(DetermineSize::class)
            ->with(
                [
                    'order.products',
                    'order.customer',
                    'order.notes',
                    'sewingModifications',
                ]
            )
            ->where('order_id', $id)
            ->where('is_new', 1)
            ->first();

        if ($info) {
            return new DeterminedSizingResource($info);
        }*/

        $info = app(InfoReceived::class)->with(
            [
                'order.customer',
                'order.notes',
                'determineSize.sewingModifications',
                'order.products.item_card.sewing_materials' => static function ($q) {
                    $q->with('fabric')->has('fabric');
                },
            ]
        )
            ->where('order_id', $id)
            ->firstOrFail();

        if ($info) {
            return new SizingResource($info);
        }


    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\SwatchResource;
use App\Jobs\SendSwatchEmailJob;
use App\Models\CustomerExtraInformation;
use App\Models\Order;
use App\Models\OrderSwatch;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class DataHandleController extends Controller
{

    /**
     * Save / update customer profile picture
     * @param Request $request
     * @return array
     */
    public function customerProfilePicture(Request $request)
    {
        if ($request->hasFile('file')) {

            //Get file
            $file = $request->file('file');

            $name = Str::slug($request->name) . '.' . $file->getClientOriginalExtension();

            $img = \Image::make($file)->fit(240)->encode('jpg', 80);

            //Get resource of image
            $resource = $img->stream()->detach();

            //Upload to digital ocean space
            \Storage::cloud()
                ->put('files/customers/' . $name, $resource, 'public');

            \Storage::delete('temp_files/' . $name);

            try {

                $value = env('DO_SPACES_CDN_ENDPOINT') . '/files/customers/' . $name;

                app(CustomerExtraInformation::class)
                    ->updateOrCreate(
                        ['customer_id' => $request->id],
                        [
                            'title' => 'profile-picture',
                            'value' => $value,
                        ]
                    );

                return response()->json(
                    [
                        'status'  => 'success',
                        'message' => $value,
                    ]
                );

            } catch (ModelNotFoundException $exception) {
                return response()->json(
                    ['status'  => 'error',
                     'message' => $exception->getMessage(),
                    ]
                );
            }


        }

        return ['status' => 'error', 'message' => 'Image file was not provided'];
    }


    /**
     * Save order swatches
     * @param Request $request
     * @return array
     */
    public function orderSwatches(Request $request)
    {
        if ($request->hasFile('files')) {

            //Get files
            $files = $request->file('files');

            $result_files = [];

            foreach ($files as $file) {

                //Rename file
                $name = Str::uuid() . '.' . $file->getClientOriginalExtension();

                $photo_thumb = \Image::make($file)
                    ->fit(165, 110, function ($constraint) {
                        $constraint->upsize();
                    })->encode('jpg', 80);

                $photo_thumb_resource = $photo_thumb->stream()->detach();

                $photo = \Image::make($file)
                    ->fit(800, 600, function ($constraint) {
                        $constraint->upsize();
                    })->encode('jpg', 80);

                $photo_resource = $photo->stream()->detach();

                //Upload to digital ocean space
                \Storage::cloud()
                    ->put('files/orders/' . $request->id . '/swatches/' . $name, $photo_resource, 'public');

                \Storage::cloud()
                    ->put('files/orders/' . $request->id . '/swatches/thumb-' . $name, $photo_thumb_resource, 'public');


                $result_files [] = [
                    'thumb' => $value = env('DO_SPACES_CDN_ENDPOINT') . '/files/orders/' . $request->id . '/swatches/thumb-' . $name,
                    'image' => $value = env('DO_SPACES_CDN_ENDPOINT') . '/files/orders/' . $request->id . '/swatches/' . $name,
                ];

            }

            try {

                $order = app(Order::class)->where('minja_id', $request->id)->first();

                $order->swatches()->delete();

                foreach ($result_files as $files) {

                    $order->swatches()->create([
                        'user_id'    => auth()->id(),
                        'path'       => $files['image'],
                        'thumb_path' => $files['thumb'],
                    ]);

                }

                //todo : send mail to design team
                //https://itsolutionstuff.com/post/how-to-send-mail-using-queue-in-laravel-57example.html

                //$details['email'] = 'your_email@gmail.com';
                //dispatch(new SendSwatchEmailJob($details));

                return response()->json(
                    [
                        'status'  => 'success',
                        'message' => $result_files,
                    ]
                );

            } catch (ModelNotFoundException $exception) {
                return response()->json(
                    ['status'  => 'error',
                     'message' => $exception->getMessage(),
                    ]
                );
            }


        }

        return ['status' => 'error', 'message' => 'Image file was not provided'];
    }

    /**
     * Delete order's swatch
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function deleteOrderSwatch(Request $request)
    {

        $swatch = app(OrderSwatch::class)->find($request->id);

        if ($swatch) {

            //find image url on cloud disk
            $path = ltrim(str_replace(env('DO_SPACES_CDN_ENDPOINT'), '', $request->file), '/');

            //delete image
            \Storage::cloud()->delete($path);

            //find thumb image
            $thumb_path = 'thumb-' . basename($path);

            $thumb_path = str_replace(basename($path), $thumb_path, $path);

            //delete thumb image
            \Storage::cloud()->delete($thumb_path);

            $order_id = $swatch->order_id;

            //delete selected swatch
            $swatch->delete();

            //find all swatches of order
            $swatches = app(OrderSwatch::class)->where('order_id', $order_id)->get();

            return SwatchResource::collection($swatches);

        }

    }
}

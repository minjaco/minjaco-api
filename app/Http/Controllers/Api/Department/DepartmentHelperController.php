<?php

namespace App\Http\Controllers\Api\Department;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class DepartmentHelperController extends Controller
{
    public function users(Request $request)
    {
        $department_users = app(User::class)
            ->whereNotIn('id', [auth()->id()])
            ->role($request->role)
            ->get(['id', 'name']);

        return $department_users;
    }
}

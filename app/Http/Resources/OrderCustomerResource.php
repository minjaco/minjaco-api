<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderCustomerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'customer_id'     => $this->customer_id,
            'email'           => $this->email,
            'facebook'        => $this->facebook,
            'twitter'         => $this->twitter,
            'instagram'       => $this->instagram,
            'link'            => $this->web_link,
            'name'            => ucwords($this->name),
            'phone_number'    => $this->phone_number,
            'country'         => $this->country,
            'registered_at'   => $this->registered_at->format('d-m-Y H:i'),
            //'previous_orders' => OrderResource::collection($this->orders),
            'previous_orders' => BasicOrderResource::collection($this->orders),
        ];
    }
}

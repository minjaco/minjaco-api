<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Spatie\Analytics\Period;

class CustomerResource extends JsonResource
{

    /**
     * @var array
     */
    protected $withoutFields = [];

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return $this->filterFields([
            'customer_id'           => $this->customer_id,
            'profile_image'         => $this->profileImage($this),
            'email'                 => $this->email,
            'emails'                => $this->emails($this),
            'name'                  => $this->name,
            'phone_number'          => $this->phone_number,
            'country'               => $this->country,
            'address'               => $this->address,
            'instagram'             => $this->instagram,
            'facebook'              => $this->facebook,
            'linkedin'              => $this->linkedin,
            'website'               => $this->website,
            'federations'           => $this->federations,
            'division'              => $this->division,
            'team_name'             => $this->team_name,
            'is_team_leader'        => $this->is_team_leader,
            'coach_name'            => $this->coach_name,
            'is_coach'              => $this->is_coach,
            'competed_before'       => $this->competed_before,
            'next_competition_date' => $this->next_competition_date,
            'profile'               => $this->profile,
            'accept_marketing'      => $this->accept_marketing,
            'status'                => $this->status,
            'registered_at'         => $this->registered_at->format('Y-m-d'),
            //todo : check here
            'orders'                => new OrderCollection($this->whenLoaded('orders')),
            //'activities'            => $this->activities($this->customer_id),
            'activities'            => $this->when(
                $this->relationLoaded('activities'),
                function () {
                    return $this->activities($this->customer_id);
                }
            ),
            'shipping'              => $this->orders->first()->shipping

        ]);
    }

    /**
     * Set the keys that are supposed to be filtered out.
     *
     * @param  array  $fields
     * @return $this
     */
    public function hide(array $fields)
    {
        $this->withoutFields = $fields;

        return $this;
    }

    /**
     * Remove the filtered keys.
     *
     * @param $array
     * @return array
     */
    protected function filterFields($array)
    {
        return collect($array)->forget($this->withoutFields)->toArray();
    }

    private function activities($id)
    {
        $activities = \Cache::remember(
            md5($id).'-analytics-activity',
            env('LONG_TTL'), // one day cache
            function () use ($id) {
                $startDate = Carbon::now()->subMonth();
                $endDate   = Carbon::now();

                $analyticsData = \Analytics::performQuery(
                    Period::create($startDate, $endDate),
                    'ga:sessions',
                    [
                        'metrics'    => 'ga:timeOnPage, ga:pageviews',
                        'dimensions' => 'ga:dimension1, ga:pagePath, ga:date, ga:dateHourMinute',
                        'filters'    => 'ga:dimension1=='.$id,
                        'sort'       => '-ga:dateHourMinute',
                    ]
                );

                return new AnalyticsCollection(collect($analyticsData->rows));
            }
        );

        return $activities;
    }

    /**
     * Customer's emails
     * @param $customer
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    private function emails($customer)
    {
        $emails = $customer->extra_info->filter(function ($item) {
            return $item->title === 'email';
        })
            ->values()
            ->all();

        return CustomerExtraInformationResource::collection(collect($emails));
    }

    private function profileImage($customer)
    {
        $info = $customer->extra_info
            ->where('title', 'profile-picture')
            ->first();

        return $info ? $info->value : '';
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'product_id'    => $this->product_id,
            'name'          => $this->name,
            'sku'           => $this->sku,
            'total_inc_tax' => $this->total_inc_tax,
            'quantity'      => $this->quantity,
        ];
    }
}

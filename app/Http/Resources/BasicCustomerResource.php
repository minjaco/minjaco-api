<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class BasicCustomerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'customer_id'   => $this->customer_id,
            'email'         => rtrim($this->email . ', ' . $this->emails($this), ', '),
            'name'          => $this->name,
            'phone_number'  => $this->phone_number,
            'country'       => $this->country,
            //Used carbon otherwise gives format exception
            'registered_at' => Carbon::parse($this->registered_at)->format('d-m-Y H:i'),
        ];
    }

    /**
     * Customer's emails
     * @param $customer
     * @return string
     */
    private function emails($customer)
    {
        $emails = $customer->extra_info->filter(function ($item) {
            return $item->title === 'email';
        })
            ->values()
            ->pluck('value')
            ->toArray();

        return implode(', ', array_values($emails));
    }
}

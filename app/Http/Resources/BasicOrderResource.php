<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BasicOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'order_id'               => $this->minja_id,
            'source'                 => $this->order_from . '-' . $this->order_id,
            'order_source'           => $this->order_source,
            'order_platform'         => $this->order_source,
            'order_from'             => $this->order_from,
            'status'                 => ucwords($this->status->name),
            'status_id'              => $this->mc_status_id,
            'client_name'            => $this->customer->name,
            'order_date'             => $this->date_created ? $this->date_created->format('d-m-Y H:i') : '-',
            'unformatted_order_date' => $this->date_created->toDateTimeString() ?? '-',
            'comp_date'              => $this->show_date ? $this->show_date->format('d-m-Y') : '-',
        ];
    }
}

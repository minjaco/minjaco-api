<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class CalenderEventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name'  => $this->name,
            'link'  => $this->htmlLink,
            'date'  => Carbon::parse($this->startDateTime)->format('D, d M Y'),
            'start' => Carbon::parse($this->start->dateTime)->format('H:i'),
            'end'   => Carbon::parse($this->end->dateTime)->format('H:i'),
        ];
    }
}

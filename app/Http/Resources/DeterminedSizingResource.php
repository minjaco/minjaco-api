<?php

namespace App\Http\Resources;

use App\Models\Country;
use App\Models\CountrySize;
use App\Models\InfoReceived;
use App\Models\Order;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Resources\Json\JsonResource;

class DeterminedSizingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'order_id'            => $this->order->minja_id,
            'order_ref_id'        => $this->order->order_from . '-' . $this->order->order_id,
            'product'             => new OrderItemResource($this->order->products->first()),
            'customer'            => [
                'id'    => $this->order->customer->customer_id,
                'name'  => $this->order->customer->name,
                'email' => $this->order->customer->email,
            ],
            'anything_else'       => $this->anything_else,
            'show_date'           => $this->order->show_date ? $this->order->show_date->format('Y-m-d') : '-',
            'date_created'        => $this->order->date_created->format('Y-m-d'),
            'diff_weeks'          => $this->order->show_date ? $this->order->show_date->diffInWeeks($this->order->date_created) : 0,
            'participant'         => $this->how_com,
            'bra_cup_size'        => $this->bra_cup_size,
            'confidence'          => $this->how_confi,
            'height'              => $this->tall,
            'current_weight'      => $this->weight_c,
            'expected_weight'     => $this->weight_e,
            'top_style'           => $this->top_style,
            'bottom_style'        => $this->bottom_style,
            'coverage_level'      => $this->coverage_level,
            'top_connector_id'    => $this->con_top,
            'middle_connector_id' => $this->con_mid,
            'bottom_connector_id' => $this->con_bot,
            'fabric_code'         => $this->cco_fabric,
            'implant'             => $this->implants,
            'notes'               => $this->order->notes,
        ];
    }
}

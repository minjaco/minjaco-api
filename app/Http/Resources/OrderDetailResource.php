<?php

namespace App\Http\Resources;

use App\Models\Status;
use Illuminate\Http\Resources\Json\JsonResource;
use Spatie\Activitylog\Models\Activity;

class OrderDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'order_id'         => $this->minja_id,
            'order_from'       => $this->order_from,
            'is_urgent'        => $this->urgent_order === 1 ?? false,
            'status'           => ucwords($this->status->name),
            'customer_message' => $this->customer_message,
            'items'            => OrderItemResource::collection($this->products),
            'customer'         => new OrderCustomerResource($this->customer),
            'notes'            => $this->notes,
            'swatches'         => SwatchResource::collection($this->swatches),
            'activities'       => $this->activities($this->minja_id),
            'statuses'         => StatusSimpleResource::collection(app(Status::class)->orderBy('order_by')->get()),
        ];
    }

    private function notesWithDepartments($notes){

    }

    private function activities($id)
    {
        $activities = app(Activity::class)
            ->with('causer')
            ->where('properties->order_id', $id)
            ->where('properties->type', '!=', 'seen')
            ->latest()
            ->get();
        return $activities;
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Carbon;

class AnalyticsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'path'       => $this[1],
            'date_time'  => Carbon::parse($this[2])->format('d-m-Y'),
            'spent_time' => (int) $this[4],
            'views'      => (int) $this[5]
        ];
    }
}

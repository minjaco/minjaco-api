<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'           => $this->product_id,
            'image'        => $this->image($this->product_id),
            'name'         => $this->name,
            'quantity'     => $this->quantity,
            'price'        => $this->total_inc_tax,
            'product_type' => $this->product_type($this->sku),
        ];
    }


    private function image($product_id)
    {
        $image = \Cache::remember(
            md5($product_id) . '-image',
            env('LONG_TTL'), // one day cache
            function () use ($product_id) {
                $images = \Bigcommerce::getCollection('/products/' . $product_id . '/images');

                if ($images) {
                    $subset = collect($images)->map(function ($image) {
                        return $image->thumbnail_url;
                    });
                } else {
                    return [];
                }

                return $subset;
            }
        );

        return count($image) > 0 ? $image[3] ?? $image->first() : '';
    }

    private function product_type($sku)
    {
        return substr($sku, 0, 2);
    }
}

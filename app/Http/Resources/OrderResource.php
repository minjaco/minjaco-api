<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'order_id'       => $this->order_id,
            'minja_id'       => $this->minja_id,
            'items_total'    => $this->items_total,
            'total_price'    => $this->total_inc_tax,
            'order_from'     => $this->order_from,
            'order_source'   => $this->order_source,
            'order_platform' => $this->order_source,
            'status'         => $this->status->name,
            'payment_status' => $this->payment_option,
            'date_created'   => $this->date_created->format('Y-m-d H:i:s'),
            //'products'     => new OrderProductCollection($this->whenLoaded('products'))// with extra data
            'products'       => OrderProductResource::collection($this->whenLoaded('products')),// classic version
            'customer'       => new BasicCustomerResource($this->whenLoaded('customer')),
            'size'           => $this->size,
        ];
    }
}

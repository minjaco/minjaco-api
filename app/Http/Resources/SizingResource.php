<?php

namespace App\Http\Resources;

use App\Models\Country;
use App\Models\CountrySize;
use App\Models\InfoReceived;
use App\Models\Order;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Resources\Json\JsonResource;

class SizingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'order_id'            => $this->order->minja_id,
            'order_ref_id'        => $this->order->order_from . '-' . $this->order->order_id,
            'product'             => new OrderItemResource($this->order->products->first()),
            'customer'            => [
                'id'    => $this->order->customer->customer_id,
                'name'  => $this->order->customer->name,
                'email' => $this->order->customer->email,
            ],
            'anything_else'       => $this->anything_else,
            'show_date'           => $this->order->show_date ? $this->order->show_date->format('Y-m-d') : '-',
            'date_created'        => $this->order->date_created->format('Y-m-d'),
            'diff_weeks'          => $this->order->show_date ? $this->order->show_date->diffInWeeks($this->order->date_created) : 0,
            'participant'         => $this->how_com,
            'bra_cup_size'        => $this->_determine_bra($this->order->country, $this->bra_cup_size_c),
            'confidence'          => $this->how_confi,
            'height'              => $this->tall,
            'current_weight'      => $this->weight_c,
            'expected_weight'     => $this->weight_e,
            'top_style'           => $this->_top_style($this->top_style),
            'bottom_style'        => $this->_bottom_style($this->bot_style),
            'coverage_level'      => $this->_cleanUp($this->bot_back_cov_level),
            'top_connector_id'    => $this->top_id,
            'middle_connector_id' => $this->mid_id,
            'bottom_connector_id' => $this->bot_id,
            'fabric_code'         => $this->order->products->first()->item_card !== null ? $this->order->products->first()->item_card->sewing_materials->first()->fabric->fabric_old_code : null,
            'implant'             => $this->_implant($this->implants),
            //'previous_order_information' => $this->_previous_order($this->order->customer->customer_id, $this->order->minja_id),
            'notes'               => $this->order->notes,
            'sewing_patterns'     => $this->determineSize->sewingModifications ?? null,
        ];
    }

    private function _bottom_style($style)
    {
        $result = '';

        if (strpos($style, 'Regular') !== false && strpos($style, 'High') !== false) {
            $result = 'High Cut';
        } elseif (strpos($style, 'Regular') !== false && strpos($style, 'V') !== false) {
            $result = 'Regular Style (V)';
        } elseif (strpos($style, 'Russian') !== false && strpos($style, 'High') !== false) {
            $result = 'Russian - Euro (High Waist)';
        } elseif (strpos($style, 'Russian') !== false && strpos($style, 'Regular') !== false) {
            $result = 'Russian - Euro (Regular)';
        } elseif (strpos($style, 'Russian') !== false && strpos($style, 'connectors') !== false) {
            $result = 'Russian - Euro (Regular)';
        } elseif (strpos($style, 'High') !== false) {
            $result = 'High Cut';
        } elseif (strpos($style, 'Scoop') !== false) {
            $result = 'Regular (Scoop)';
        } elseif (strpos($style, 'Straight') !== false) {
            $result = 'Regular (Straight)';
        }

        return $result;
    }

    private function _top_style($style)
    {
        $result = '';

        if (strpos($style, 'Adjustable') !== false && strpos(ucwords($style), 'Drawstring') !== false) {
            $result = 'เต้้าดอกบัว (Adjustable Drawstring)';
        } elseif (strpos($style, 'Drawstring') !== false && strpos(ucwords($style), 'Molded') !== false) {
            $result = 'เต้าเลื่อน (Molded Cup - Drawstring)';
        } elseif (strpos($style, 'Soft') !== false && strpos(ucwords($style), 'Triangle') !== false) {
            $result = 'เต้้าดอกบัว (Adjustable Drawstring)';
        } elseif (strpos($style, 'Push-up') !== false && strpos(ucwords($style), 'Underwire') !== false) {
            $result = 'เต้าดัันทรงแบบบาง (Underwire - Thin)';
        } elseif (strpos($style, 'Regular') !== false && strpos(ucwords($style), 'Moulded') !== false) {
            $result = 'เต้าธรรมดา (Molded Cup - Non-Drawstring)';
        } elseif (strpos($style, 'Underwire') !== false) {
            $result = 'เต้าดัันทรงแบบหนา (Underwire - Thin)';
        }

        return $result;
    }

    /**
     * Get customer's previous order
     * @param $customer_id
     * @param $order_id
     * @return \Illuminate\Support\Collection|null |null
     */
    private function _previous_order($customer_id, $order_id)
    {
        try {
            $previous_order = app(Order::class)
                ->where('customer_id', $customer_id)
                ->whereNotIn('minja_id', [$order_id])
                ->latest()
                ->firstOrFail();

            try {
                $sizing_information = app(InfoReceived::class)
                    ->with('determine_size')
                    ->where('order_id', $previous_order->minja_id)
                    ->firstOrFail();
            } catch (ModelNotFoundException $exception) {
                return null;
            }
        } catch (ModelNotFoundException $exception) {
            return null;
        }

        return collect([
            'order_id'             => $sizing_information->order_id,
            'top_style'            => $this->_top_style($sizing_information->top_style),
            'bottom_style'         => $this->_bottom_style($sizing_information->bot_style),
            'coverage_level'       => $this->_cleanUp($sizing_information->bot_back_cov_level),
            'top_size'             => optional($sizing_information->determine_size)->top_size,
            'bottom_front_size_no' => optional($sizing_information->determine_size)->bottom_front_size_no,
            'bottom_front_letter'  => optional($sizing_information->determine_size)->bottom_front_letter,
            'bottom_front_comment' => trim(optional($sizing_information->determine_size)->bottom_front_comment, "\t"),
            'bottom_back_size_no'  => optional($sizing_information->determine_size)->bottom_front_size_no,
            'bottom_back_letter'   => optional($sizing_information->determine_size)->bottom_front_letter,
            'bottom_back_comment'  => trim(optional($sizing_information->determine_size)->bottom_front_comment, "\t"),
        ]);
    }

    /*
     * Clean up text for coverage level
     */
    private function _cleanUp($text)
    {
        $text = trim(preg_replace('/\s*\([^)]*\)/', '', $text));

        return $text;
    }

    /*
     * Clean up implants for better match
     */
    private function _implant($text)
    {
        $text = explode(',', $text);

        return trim($text[0]);
    }

    /**
     * Determines bra text
     * @param $country
     * @param $cup_size
     * @return string
     */
    private function _determine_bra($country, $cup_size): ?string
    {
        $cup_size = $this->clean(strtoupper($cup_size));

        try {
            $data = Country::with(['sizes' => function ($size) use ($cup_size) {
                $size->where('country_size', $cup_size);
            }])
                ->where('country_name', $country)->firstOrFail();

            if ($data->sizes->count() > 0) {
                return $country . '|' . $cup_size . '|' . $data->sizes[0]->md_size;
            }

            $check_by_size = app(CountrySize::class)->where('country_size', $cup_size)->first();

            if ($check_by_size) {
                return $country . '|' . $cup_size . '|' . $check_by_size->md_size;
            }

            return $country . '|' . $cup_size;

        } catch (ModelNotFoundException $exception) {
            return $cup_size;
        }
    }

    /**
     * Get the first match of bra cup in sentences based sizing (FE : 32B/C (XXXXXXXXX))
     * @param $input
     * @return string
     */
    private function clean($input): string
    {
        //https://regex101.com/r/NprNMT/1
        preg_match_all('/[\d{2}[A-Z]+|\/[A-Z]+/m', $input, $matches, PREG_SET_ORDER, 0);

        return $matches[0][0];
    }
}

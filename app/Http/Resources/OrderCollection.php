<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class OrderCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => OrderResource::collection($this->collection),
            'meta' => [
                'total_revenue'      => number_abbreviation($this->collection->sum('total_inc_tax')) ?? 0,
                'total_orders'       => $this->collection->count(),
                'purchased_products' => $this->collection->pluck('products')->count()
            ]
        ];
    }
}

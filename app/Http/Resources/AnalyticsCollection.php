<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class AnalyticsCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => AnalyticsResource::collection($this->collection),
            'meta' => [
                'total_spent_time' => $this->collection->reduce(function ($carry, $item) {
                    return $carry + (int)$item[4];
                }, 0),
                'total_views'      => $this->collection->reduce(function ($carry, $item) {
                    return $carry + (int)$item[5];
                }, 0),
                'count'            => $this->collection->count()
            ],
        ];
    }
}

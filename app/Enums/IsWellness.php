<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class IsWellness extends Enum
{
    public const YES = 1;
    public const NO = 0;
}

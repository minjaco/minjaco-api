<?php

    namespace App\Enums;

use BenSampo\Enum\Enum;

final class OrderType extends Enum
{
    public const URGENT  = 1;
    public const REGULAR = 0;
}

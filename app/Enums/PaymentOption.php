<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class PaymentOption extends Enum
{
    public const FULL = 0;
    public const PARTIAL = 1;
}

<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class DashboardMessageType extends Enum
{
    public const MANAGEMENT = 1;
    public const DEPARTMENT = 2;
}

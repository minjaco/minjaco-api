<?php

    namespace App\Enums;

use BenSampo\Enum\Enum;

final class CustomerStatus extends Enum
{
    public const PASSIVE = 0;
    public const ACTIVE  = 1;
}

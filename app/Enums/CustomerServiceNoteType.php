<?php

    namespace App\Enums;

use BenSampo\Enum\Enum;

final class CustomerServiceNoteType extends Enum
{
    public const URGENT = 1;
    public const NORMAL = 2;
}

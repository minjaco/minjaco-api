<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class BigCommerceStatus extends Enum
{
    public const INCOMPLETE = 0;
    //public const AWAITING_FULFILMENT = 11;
    public const BEING_PROCESSED = 11;
}

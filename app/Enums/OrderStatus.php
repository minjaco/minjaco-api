<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class OrderStatus extends Enum
{
    public const WAITING_FOR_SIZING_INFO = 1;
    public const SIZING_INFO_RECEIVED = 2;
    public const TIME_TOGET_IN_TOUCH_WITH_CLIENTS = 3;
    public const PREPARE_FOR_PRODUCTION = 4;
    public const READY_FOR_PRODUCTION = 5;
    public const IN_QUEUE_FOR_SEWING = 6;
    public const PATTERN = 7;
    public const BEGIN_SEWN = 8;
    public const QC_SEWING = 9;
    public const FAILED_QC_BEING_FIXED_BY_SEWING = 10;
    public const IN_QUEUE_FOR_DECORATION = 11;
    public const BLOCKING = 12;
    public const DECORATION_25 = 13;
    public const DECORATION_50 = 14;
    public const DECORATION_75 = 15;
    public const QC_BY_DECORATION_TEAM = 16;
    public const ATTACHING_CONNECTORS = 17;
    public const FINAL_QC_ENTER_RATINGS = 18;
    public const FAILED_QC_BEING_FIXED_BY_DECORATIVE = 19;
    public const TAKE_PHOTOS_AND_PASTE_LINK = 20;
    public const FINISH_GOODS = 21;
    public const AWAITING_PACKAGING = 22;
    public const AWAITING_PICKUP_BY_DHL = 23;
    public const COMPLETE = 24;
    public const ON_HOLD = 25;
    public const CANCELLED = 26;
    public const PREPARE_MATERIAL = 27;
    public const DECORATION_100 = 28;
    public const AWAITING_DRAFT_SUIT_FEEDBACK = 29;
    public const DRAFT_SUIT_FEEDBACK_RECEIVED = 30;
    public const AWAITING_PICTURE_FOR_SIZING = 31;
    public const AWAITING_CONFIRM = 32;
    public const SHIPPED = 33;
    public const AWAITING_APPROVE_REFUND = 34;
    public const REFUNDING = 35;
    public const REFUNDED = 36;
    public const MOVED_TO_STOCK_RM = 37;
}

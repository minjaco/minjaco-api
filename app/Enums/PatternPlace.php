<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class PatternPlace extends Enum
{
    public const TOP = 'top';
    public const BOTTOM_FRONT = 'bottom_front';
    public const BOTTOM_BACK = 'bottom_back';
}

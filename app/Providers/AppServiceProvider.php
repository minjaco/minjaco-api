<?php

namespace App\Providers;

use App\Models\Order;
use App\Models\OrderNote;
use App\Models\OrderProduct;
use App\Observers\OrderNoteObserver;
use App\Observers\OrderObserver;
use App\Observers\OrderProductObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Order::observe(OrderObserver::class);
        OrderNote::observe(OrderNoteObserver::class);
        OrderProduct::observe(OrderProductObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() === 'local') {
            $this->app->register(\Reliese\Coders\CodersServiceProvider::class);
        }
    }
}

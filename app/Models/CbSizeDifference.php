<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 05 Aug 2019 15:10:13 +0700.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class CbSizeDifference
 *
 * @property int $id
 * @property int $top_size
 * @property int $bottom_size
 * @property string $bottom_cut
 * @property string $pattern_code_one
 * @property string $pattern_code_two
 * @property string $pattern_code_three
 * @property int $top_sqrcm
 * @property int $bottom_front_sqrcm
 * @property int $bottom_back_sqrcm
 * @property int $bottom_total_sqrcm
 * @property int $top_bottom_total_sqrcm
 * @property int $top_percent
 * @property int $bottom_front_percent
 * @property int $bottom_back_percent
 * @property int $bottom_total_percent
 * @property int $top_bottom_total_percent
 * @property int $total_crystals_as_pcs
 * @property string $total_pack_to_distribute
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class CbSizeDifference extends Eloquent
{
	protected $casts = [
		'top_size' => 'int',
		'bottom_size' => 'int',
		'top_sqrcm' => 'int',
		'bottom_front_sqrcm' => 'int',
		'bottom_back_sqrcm' => 'int',
		'bottom_total_sqrcm' => 'int',
		'top_bottom_total_sqrcm' => 'int',
		'top_percent' => 'int',
		'bottom_front_percent' => 'int',
		'bottom_back_percent' => 'int',
		'bottom_total_percent' => 'int',
		'top_bottom_total_percent' => 'int',
		'total_crystals_as_pcs' => 'int'
	];

	protected $fillable = [
		'top_size',
		'bottom_size',
		'bottom_cut',
		'pattern_code_one',
		'pattern_code_two',
		'pattern_code_three',
		'top_sqrcm',
		'bottom_front_sqrcm',
		'bottom_back_sqrcm',
		'bottom_total_sqrcm',
		'top_bottom_total_sqrcm',
		'top_percent',
		'bottom_front_percent',
		'bottom_back_percent',
		'bottom_total_percent',
		'top_bottom_total_percent',
		'total_crystals_as_pcs',
		'total_pack_to_distribute'
	];
}

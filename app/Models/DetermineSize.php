<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 29 Jan 2019 03:36:17 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class DetermineSize
 *
 * @property int $id
 * @property int $order_id
 * @property int $customer_id
 * @property string $current_w
 * @property string $expected_w
 * @property string $height
 * @property string $bra_cup_size
 * @property string $m_bra_cup_size
 * @property string $top_style
 * @property string $bottom_style
 * @property string $coverage_level
 * @property string $top_size
 * @property string $top_comment
 * @property int $extra_padding
 * @property string $bottom_front_size_no
 * @property string $bottom_front_letter
 * @property string $bottom_front_comment
 * @property string $bottom_back_size_no
 * @property string $bottom_back_letter
 * @property string $bottom_back_comment
 * @property int $waistline
 * @property int $cco_fabric
 * @property int $con_top
 * @property int $con_mid
 * @property int $con_bot
 * @property string $gap_between_cups
 * @property string $special_instruction
 * @property int $implants
 * @property int $ok_rm
 * @property int $is_new
 *
 * @package App\Models
 */
class DetermineSize extends Eloquent
{
    protected $table = 'determine_size';
    public $timestamps = false;

    protected $casts = [
        'order_id'      => 'int',
        'customer_id'   => 'int',
        'extra_padding' => 'int',
        'cco_fabric'    => 'int',
        'con_top'       => 'int',
        'con_mid'       => 'int',
        'con_bot'       => 'int',
        'implants'      => 'int',
        'ok_rm'         => 'int',
        'waistline'     => 'int',
        'is_new'        => 'int',
    ];

    protected $fillable = [
        'order_id',
        'customer_id',
        'current_w',
        'expected_w',
        'height',
        'bra_cup_size',
        'm_bra_cup_size',
        'top_style',
        'bottom_style',
        'coverage_level',
        'top_size',
        'top_comment',
        'extra_padding',
        'bottom_front_size_no',
        'bottom_front_letter',
        'bottom_front_comment',
        'bottom_back_size_no',
        'bottom_back_letter',
        'bottom_back_comment',
        'cco_fabric',
        'con_top',
        'con_mid',
        'con_bot',
        'gap_between_cups',
        'special_instruction',
        'implants',
        'ok_rm',
        'waistline',
        'is_new',
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function sewingModifications()
    {
        return $this->hasMany(OrderSewingPattern::class);
    }

    public function patterns()
    {
        return $this->hasMany(Order::class);
    }
}

<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 10 Apr 2019 16:12:36 +0700.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class ItemCard
 *
 * @property int $runid
 * @property string $product_code
 * @property string $product_type
 * @property string $collection
 * @property string $design_code
 * @property string $color
 * @property string $original_code
 * @property float $total_rm
 * @property float $total_pm
 * @property float $total_av
 * @property float $foh_cost
 * @property float $shipping_cost
 * @property float $total_mc
 * @property float $admin_cost
 * @property float $remake_cost
 * @property float $paypal_cost
 * @property float $total_cost
 * @property int $margin
 * @property float $margin_thb
 * @property float $actual_margin
 * @property float $actual_margin_thb
 * @property float $rate_thb_usd
 * @property float $csp_thb
 * @property float $fsp_thb
 * @property float $csp_usd
 * @property float $fsp_usd
 * @property int $status_card
 * @property string $how_to
 * @property float $net_margin
 * @property float $net_margin_per
 * @property string $create_user
 * @property string $update_user
 * @property \Carbon\Carbon $time_stamp
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class ItemCard extends Eloquent
{
    protected $primaryKey = 'runid';

    protected $casts = [
        'total_rm'          => 'float',
        'total_pm'          => 'float',
        'total_av'          => 'float',
        'foh_cost'          => 'float',
        'shipping_cost'     => 'float',
        'total_mc'          => 'float',
        'admin_cost'        => 'float',
        'remake_cost'       => 'float',
        'paypal_cost'       => 'float',
        'total_cost'        => 'float',
        'margin'            => 'int',
        'margin_thb'        => 'float',
        'actual_margin'     => 'float',
        'actual_margin_thb' => 'float',
        'rate_thb_usd'      => 'float',
        'csp_thb'           => 'float',
        'fsp_thb'           => 'float',
        'csp_usd'           => 'float',
        'fsp_usd'           => 'float',
        'status_card'       => 'int',
        'net_margin'        => 'float',
        'net_margin_per'    => 'float'
    ];

    protected $dates = [
        'time_stamp'
    ];

    protected $fillable = [
        'product_code',
        'product_type',
        'collection',
        'design_code',
        'color',
        'original_code',
        'total_rm',
        'total_pm',
        'total_av',
        'foh_cost',
        'shipping_cost',
        'total_mc',
        'admin_cost',
        'remake_cost',
        'paypal_cost',
        'total_cost',
        'margin',
        'margin_thb',
        'actual_margin',
        'actual_margin_thb',
        'rate_thb_usd',
        'csp_thb',
        'fsp_thb',
        'csp_usd',
        'fsp_usd',
        'status_card',
        'how_to',
        'net_margin',
        'net_margin_per',
        'create_user',
        'update_user',
        'time_stamp'
    ];


    public function product()
    {
        return $this->belongsTo(Product::class, 'sku', 'original_code');
    }

    public function order_product()
    {
        return $this->belongsTo(OrderProduct::class, 'sku', 'original_code');
    }

    public function sewing_materials()
    {
        return $this->hasMany(SewingRawMaterial::class, 'product_id', 'runid');
    }
}

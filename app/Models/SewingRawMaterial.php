<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 28 Jan 2019 09:49:02 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class SewingRawMaterial
 *
 * @property int $id
 * @property int $product_id
 * @property int $part_number_id
 * @property string $part_number
 * @property string $description
 * @property string $size
 * @property float $width
 * @property float $length
 * @property int $quantity
 * @property string $unit
 * @property float $unit_price
 * @property float $ship
 * @property float $total
 * @property float $loss
 * @property \Carbon\Carbon $time_stamp
 * @package App\Models
 */
class SewingRawMaterial extends Eloquent
{
    public $timestamps = false;

    protected $casts = [
        'product_id'     => 'int',
        'part_number_id' => 'int',
        'width'          => 'float',
        'length'         => 'float',
        'quantity'       => 'int',
        'unit_price'     => 'float',
        'ship'           => 'float',
        'total'          => 'float',
        'loss'           => 'float'
    ];

    protected $dates = [
        'time_stamp'
    ];

    protected $fillable = [
        'product_id',
        'part_number_id',
        'part_number',
        'description',
        'size',
        'width',
        'length',
        'quantity',
        'unit',
        'unit_price',
        'ship',
        'total',
        'loss',
        'time_stamp'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function fabric()
    {
        return $this->hasOne(Fabric::class, 'fabric_id', 'part_number_id');
    }
}

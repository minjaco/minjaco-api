<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 05 Jul 2019 11:52:56 +0700.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

/**
 * Class Department
 *
 * @property int $id
 * @property int $parent_id
 * @property string $name
 * @property string $slug
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Department extends Eloquent
{
    use HasSlug;

    protected $casts = [
        'parent_id' => 'int',
    ];

    protected $fillable = [
        'parent_id',
        'name',
        'slug',
    ];

    public function users()
    {
        return $this->hasMany(User::class, 'redirect_uri', 'slug');
    }

    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }
}

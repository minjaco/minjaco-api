<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 29 Jan 2019 03:36:28 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class InfoReceived
 *
 * @property int $id
 * @property int $order_id
 * @property string $top_con
 * @property string $mid_con
 * @property string $bot_con
 * @property string $top_style
 * @property string $bot_style
 * @property string $bot_front_cov_level
 * @property string $bot_back_cov_level
 * @property string $weight_c
 * @property string $weight_e
 * @property string $tall
 * @property string $bra_cup_size_c
 * @property string $bra_cup_size_s
 * @property string $how_com
 * @property string $anything_else
 * @property string $how_confi
 * @property string $implants
 * @property int $top_id
 * @property int $mid_id
 * @property int $bot_id
 * @property string $comp_name
 * @property string $hear_ab
 * @property \Carbon\Carbon $time_stamp
 *
 * @package App\Models
 */
class InfoReceived extends Eloquent
{
    protected $table = 'info_received';
    public $timestamps = false;

    protected $casts = [
        'order_id' => 'int',
        'top_id'   => 'int',
        'mid_id'   => 'int',
        'bot_id'   => 'int'
    ];

    protected $dates = [
        'time_stamp'
    ];

    protected $fillable = [
        'order_id',
        'top_con',
        'mid_con',
        'bot_con',
        'top_style',
        'bot_style',
        'bot_front_cov_level',
        'bot_back_cov_level',
        'weight_c',
        'weight_e',
        'tall',
        'bra_cup_size_c',
        'bra_cup_size_s',
        'how_com',
        'anything_else',
        'how_confi',
        'implants',
        'top_id',
        'mid_id',
        'bot_id',
        'comp_name',
        'hear_ab',
        'time_stamp'
    ];

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id', 'minja_id');
    }

    public function determineSize()
    {
        return $this->hasOne(DetermineSize::class, 'order_id', 'order_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Laravel\Scout\Searchable;
use Spatie\Activitylog\Traits\CausesActivity;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, SoftDeletes, HasRoles, Searchable, CausesActivity;

    // Laravel scout
    public $asYouType = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable
        = [
            'uuid',
            'name',
            'avatar',
            'email',
            'redirect_uri',
            'password',
        ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden
        = [
            'password',
            'remember_token',
            'email',
            'email_verified_at',
            'created_at',
            'updated_at',
            'deleted_at',
        ];

    public function dashboardMessages()
    {
        return $this->hasMany(DashboardMessage::class);
    }

    public function notes()
    {
        return $this->hasMany(OrderNote::class);
    }

    public function department()
    {
        return $this->belongsTo(Department::class, 'redirect_uri', 'slug');
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array                      = $this->toArray();
        $array[$this->getKeyName()] = $this->getKey();

        /*$array = [
            'name'  => $this->name,
            'email' => $this->email,
        ];*/

        return $array;
    }
}

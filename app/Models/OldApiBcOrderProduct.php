<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ApiBcOrderProduct
 *
 * @property int $bc_id
 * @property int $p_id
 * @property string $p_name
 * @property string $p_sku
 * @property string $p_type
 * @property float $p_base_price
 * @property float $p_total_ex_tax
 * @property float $p_weight
 * @property int $p_quantity
 * @property string $p_is_refunded
 * @property int $p_quantity_refunded
 * @property Carbon $p_show_date
 * @property string $p_special_request
 * @property int $p_payment_option
 *
 * @package App\Models
 */
class OldApiBcOrderProduct extends Model
{
    protected $connection = 'mysql_old';
    protected $table = 'api_bc_order_products';
    public $incrementing = false;
    public $timestamps = false;

    protected $casts = [
        'bc_id'               => 'int',
        'p_id'                => 'int',
        'p_base_price'        => 'float',
        'p_total_ex_tax'      => 'float',
        'p_weight'            => 'float',
        'p_quantity'          => 'int',
        'p_quantity_refunded' => 'int',
        'p_payment_option'    => 'int',
    ];

    protected $dates = [
        'p_show_date',
    ];

    protected $fillable = [
        'bc_id',
        'p_id',
        'p_name',
        'p_sku',
        'p_type',
        'p_base_price',
        'p_total_ex_tax',
        'p_weight',
        'p_quantity',
        'p_is_refunded',
        'p_quantity_refunded',
        'p_show_date',
        'p_special_request',
        'p_payment_option',
    ];
}

<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 23 Aug 2019 11:03:19 +0700.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Crystal
 *
 * @property int $runid
 * @property string $item_code
 * @property string $item_group
 * @property string $color
 * @property string $shape_code
 * @property string $shape_type
 * @property string $cutting
 * @property string $size
 * @property string $supplier
 * @property string $description
 * @property int $quantity
 * @property int $min_stock
 * @property float $unit_price
 * @property string $unit
 * @property string $location_stock
 * @property string $supplier_code
 * @property int $status
 * @property string $rate_group
 * @property string $action_by
 * @property \Carbon\Carbon $create_time
 * @property \Carbon\Carbon $time_stamp
 * @property float $shipping_cost
 * @property float $total_price
 *
 * @package App\Models
 */
class Crystal extends Eloquent
{
    protected $primaryKey = 'runid';
    public $timestamps = false;

    protected $casts = [
        'quantity'      => 'int',
        'min_stock'     => 'int',
        'unit_price'    => 'float',
        'status'        => 'int',
        'shipping_cost' => 'float',
        'total_price'   => 'float',
    ];

    protected $dates = [
        'create_time',
        'time_stamp',
    ];

    protected $fillable = [
        'item_code',
        'item_group',
        'color',
        'shape_code',
        'shape_type',
        'cutting',
        'size',
        'supplier',
        'description',
        'quantity',
        'min_stock',
        'unit_price',
        'unit',
        'location_stock',
        'supplier_code',
        'status',
        'rate_group',
        'action_by',
        'create_time',
        'time_stamp',
        'shipping_cost',
        'total_price',
    ];
}

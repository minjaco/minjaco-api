<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class OldOrder extends Eloquent
{
    protected $connection = 'mysql_old';
    protected $table = 'tb_orders';
    protected $primaryKey = 'order_id';
    public $incrementing = false;
    public $timestamps = false;

    protected $casts = [
        'order_id'       => 'int',
        'order_ref_no'   => 'int',
        'status_id'      => 'int',
        'payment_option' => 'int',
        'is_free'        => 'int',
        'is_wellness'    => 'int',
    ];

    protected $dates = [
        'order_date',
        'show_date',
        'ship_date',
        'time_stamp',
    ];

    protected $fillable = [
        'order_id',
        'id_encode',
        'order_from',
        'order_ref_no',
        'client_name',
        'client_last_name',
        'client_email',
        'urgent_order',
        'product_code',
        'order_type',
        'order_type_ref_id',
        'order_remake_type',
        'order_date',
        'show_date',
        'ship_date',
        'client_note',
        'staff_note',
        'sub_total',
        'shipping_cost',
        'grand_total',
        'status_id',
        'client_email_nd',
        'payment_option',
        'is_free',
        'is_wellness',
        'time_stamp',
    ];
}

<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 28 Jan 2019 09:48:53 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class PackingRawMaterial
 *
 * @property int $id
 * @property int $product_id
 * @property int $part_number_id
 * @property string $part_number
 * @property string $description
 * @property string $size
 * @property int $quantity
 * @property string $unit
 * @property float $unit_price
 * @property float $ship
 * @property float $total
 * @property float $loss
 * @property \Carbon\Carbon $time_stamp
 *
 * @package App\Models
 */
class PackingRawMaterial extends Eloquent
{
    public $timestamps = false;

    protected $casts = [
        'product_id'     => 'int',
        'part_number_id' => 'int',
        'quantity'       => 'int',
        'unit_price'     => 'float',
        'ship'           => 'float',
        'total'          => 'float',
        'loss'           => 'float'
    ];

    protected $dates = [
        'time_stamp'
    ];

    protected $fillable = [
        'product_id',
        'part_number_id',
        'part_number',
        'description',
        'size',
        'quantity',
        'unit',
        'unit_price',
        'ship',
        'total',
        'loss',
        'time_stamp'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}

<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 25 Mar 2019 16:33:34 +0700.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class DashboardMessage
 *
 * @property int $id
 * @property int $user_id
 * @property string $group
 * @property int $type
 * @property int $is_completed
 * @property int $to
 * @property string $content
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 *
 * @package App\Models
 */
class DashboardMessage extends Eloquent
{
    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $casts = [
        'user_id'      => 'int',
        'type'         => 'int',
        'to'           => 'int',
        'is_completed' => 'int'
    ];

    protected $fillable = [
        'user_id',
        'to',
        'group',
        'type',
        'content',
        'is_completed'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function threads()
    {
        return $this->hasMany(DashboardMessageThread::class)->with('user')->latest();
    }
}

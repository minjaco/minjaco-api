<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 10 May 2019 10:13:57 +0700.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class OrderSewingPattern
 *
 * @property int $id
 * @property int $determine_size_id
 * @property string $place
 * @property string $modification
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class OrderSewingPattern extends Eloquent
{
    protected $casts = [
        'determine_size_id' => 'int',
    ];

    protected $fillable = [
        'determine_size_id',
        'place',
        'modification',
    ];

    public function determineSize()
    {
        return $this->belongsTo(DetermineSize::class);
    }
}

<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 21 Mar 2019 13:25:47 +0700.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class CustomerExtraInformation
 *
 * @property int $id
 * @property int $customer_id
 * @property string $title
 * @property string $value
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class CustomerExtraInformation extends Eloquent
{
    protected $table = 'customer_extra_information';

    protected $casts = [
        'customer_id' => 'int'
    ];

    protected $fillable = [
        'customer_id',
        'title',
        'value'
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id', 'customer_id');
    }
}

<?php

    /**
     * Created by Reliese Model.
     * Date: Wed, 23 Jan 2019 07:27:11 +0000.
     */

    namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
     * Class CustomerServiceNote
     *
     * @property int            $id
     * @property string         $note_uuid
     * @property int            $customer_id
     * @property int            $user_id
     * @property int            $order_id
     * @property int            $type
     * @property string         $content
     * @property \Carbon\Carbon $created_at
     * @property \Carbon\Carbon $updated_at
     * @property string         $deleted_at
     *
     * @package App\Models
     */
    class CustomerServiceNote extends Eloquent
    {
        use \Illuminate\Database\Eloquent\SoftDeletes;

        protected $casts
            = [
                'customer_id' => 'int',
                'user_id'     => 'int',
                'order_id'    => 'int',
                'type'        => 'int',
            ];

        protected $fillable
            = [
                'note_uuid',
                'customer_id',
                'user_id',
                'order_id',
                'type',
                'content',
            ];
    }

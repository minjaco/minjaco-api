<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ApiBcOrder
 *
 * @property int $bc_id
 * @property int $customer_id
 * @property int $status_id
 * @property float $subtotal_ex_tax
 * @property float $base_shipping_cost
 * @property float $total_ex_tax
 * @property int $items_total
 * @property string $payment_method
 * @property string $payment_provider_id
 * @property string $geoip_country
 * @property string $geoip_country_iso2
 * @property string $customer_message
 * @property float $gift_amount
 * @property float $coupon_discount
 * @property string $coupons
 * @property Carbon $order_date
 * @property Carbon $time_stamp
 *
 * @package App\Models
 */
class OldApiBcOrder extends Model
{
    protected $connection = 'mysql_old';
    protected $table = 'api_bc_order';
    protected $primaryKey = 'bc_id';
    public $incrementing = false;
    public $timestamps = false;

    protected $casts = [
        'bc_id'              => 'int',
        'customer_id'        => 'int',
        'status_id'          => 'int',
        'subtotal_ex_tax'    => 'float',
        'base_shipping_cost' => 'float',
        'total_ex_tax'       => 'float',
        'items_total'        => 'int',
        'gift_amount'        => 'float',
        'coupon_discount'    => 'float',
    ];

    protected $dates = [
        'order_date',
        'time_stamp',
    ];

    protected $fillable = [
        'bc_id',
        'customer_id',
        'status_id',
        'subtotal_ex_tax',
        'base_shipping_cost',
        'total_ex_tax',
        'items_total',
        'payment_method',
        'payment_provider_id',
        'geoip_country',
        'geoip_country_iso2',
        'customer_message',
        'gift_amount',
        'coupon_discount',
        'coupons',
        'order_date',
        'time_stamp',
    ];
}

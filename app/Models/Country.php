<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 11 Apr 2019 11:52:35 +0700.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Country
 *
 * @property int $id
 * @property string $country_name
 * @property string $country_iso2
 * @property string $country_iso3
 * @property int $country_ds
 * @property int $country_ds_full
 *
 * @package App\Models
 */
class Country extends Eloquent
{
    public $incrementing = false;
    public $timestamps = false;

    protected $casts = [
        'id'              => 'int',
        'country_ds'      => 'int',
        'country_ds_full' => 'int'
    ];

    protected $fillable = [
        'country_name',
        'country_iso2',
        'country_iso3',
        'country_ds',
        'country_ds_full'
    ];

    public function sizes()
    {
        return $this->hasMany(CountrySize::class, 'country_id', 'id');
    }
}

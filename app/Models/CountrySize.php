<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 11 Apr 2019 11:52:35 +0700.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Country
 *
 * @property int $id
 * @property string $country_name
 * @property string $country_iso2
 * @property string $country_iso3
 * @property int $country_ds
 * @property int $country_ds_full
 *
 * @package App\Models
 */
class CountrySize extends Eloquent
{
    public $incrementing = false;
    public $timestamps = false;
    public $table = 'country_size';

    protected $casts = [
        'country_id' => 'int'
    ];

    protected $fillable = [
        'country_id',
        'country_size',
        'md_size'
    ];
}

<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ApiBcOrderIncomplete
 *
 * @property int $bc_id
 * @property Carbon $in_order_date
 * @property string $in_customer_message
 * @property string $in_first_name
 * @property string $in_last_name
 * @property string $in_country
 * @property string $in_country_iso2
 * @property string $in_phone
 * @property string $in_email
 * @property int $in_status
 * @property string $in_comment
 * @property Carbon $in_follow_up
 * @property string $in_staff
 * @property string $in_order_from
 * @property string $in_user_name
 * @property int $in_status_edit
 *
 * @package App\Models
 */
class OldApiBcOrderIncomplete extends Model
{
    protected $connection = 'mysql_old';
    protected $table = 'api_bc_order_incomplete';
    protected $primaryKey = 'bc_id';
    public $timestamps = false;

    protected $casts = [
        'in_status'      => 'int',
        'in_status_edit' => 'int',
    ];

    protected $dates = [
        'in_order_date',
        'in_follow_up',
    ];

    protected $fillable = [
        'bc_id',
        'in_order_date',
        'in_customer_message',
        'in_first_name',
        'in_last_name',
        'in_country',
        'in_country_iso2',
        'in_phone',
        'in_email',
        'in_status',
        'in_comment',
        'in_follow_up',
        'in_staff',
        'in_order_from',
        'in_user_name',
        'in_status_edit',
    ];
}

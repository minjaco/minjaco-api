<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 25 Jan 2019 03:45:18 +0000.
 */

namespace App\Models;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Customer
 *
 * @property int $id
 * @property int $customer_id
 * @property string $uuid
 * @property string $name
 * @property string $phone_number
 * @property string $email
 * @property string $address
 * @property string $country
 * @property string $instagram
 * @property string $facebook
 * @property string $linkedin
 * @property string $website
 * @property string $federations
 * @property string $division
 * @property string $team_name
 * @property int $is_team_leader
 * @property string $coach_name
 * @property int $is_coach
 * @property string $competed_before
 * @property string $next_competition_date
 * @property string $profile
 * @property int $accepts_marketing
 * @property int $status
 * @property \Carbon\Carbon $registered_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 *
 * @package App\Models
 */
class Customer extends Eloquent
{
    use \Illuminate\Database\Eloquent\SoftDeletes, Searchable;

    protected $casts = [
        'customer_id'       => 'int',
        'is_team_leader'    => 'int',
        'is_couch'          => 'int',
        'accepts_marketing' => 'int',
        'status'            => 'int',
    ];

    protected $dates = [
        'registered_at',
    ];

    protected $fillable = [
        'customer_id',
        'uuid',
        'name',
        'phone_number',
        'email',
        'address',
        'country',
        'instagram',
        'facebook',
        'linkedin',
        'website',
        'federations',
        'division',
        'team_name',
        'is_team_leader',
        'coach_name',
        'is_coach',
        'competed_before',
        'next_competition_date',
        'profile',
        'accepts_marketing',
        'status',
        'registered_at',
    ];


    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array = [
            'id'    => $this->id,
            'name'  => $this->name,
            'email' => $this->email,
            'phone' => $this->phone_number,
        ];

        return $array;
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'customer_id', 'customer_id')
            ->where('bc_status', '!=', 'Incomplete')
            ->latest();
    }

    public function extra_info()
    {
        return $this->hasMany(CustomerExtraInformation::class, 'customer_id', 'customer_id');
    }

    //todo : need to review
    /*public function sizes()
    {
        return $this->hasMany(DetermineSize::class, 'customer_id', 'customer_id');
    }*/

    /*public function activities()
    {
        return $this->hasMany(customerActivity::class);
    }*/

    public function serviceNotes()
    {
        return $this->hasMany(CustomerServiceNote::class);
    }
}

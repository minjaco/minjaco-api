<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 25 Jan 2019 04:51:51 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class OrderShipping
 *
 * @property int $id
 * @property int $order_id
 * @property string $first_name
 * @property string $last_name
 * @property string $company
 * @property string $street_1
 * @property string $street_2
 * @property string $city
 * @property string $zip
 * @property string $country
 * @property string $country_iso2
 * @property string $state
 * @property string $phone
 * @property string $email
 * @property string $items_total
 * @property string $shipping_method
 * @property float $base_cost
 * @property float $cost_inc_tax
 * @property int $shipping_zone_id
 * @property string $shipping_zone_name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class OrderShipping extends Eloquent
{
    public $incrementing = false;

    protected $casts = [
        'id' => 'int',
        'order_id' => 'int',
        'base_cost' => 'float',
        'cost_inc_tax' => 'float',
        'shipping_zone_id' => 'int'
    ];

    protected $fillable = [
        'id',
        'order_id',
        'first_name',
        'last_name',
        'company',
        'street_1',
        'street_2',
        'city',
        'zip',
        'country',
        'country_iso2',
        'state',
        'phone',
        'email',
        'items_total',
        'shipping_method',
        'base_cost',
        'cost_inc_tax',
        'shipping_zone_id',
        'shipping_zone_name'
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}

<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 28 Jan 2019 10:17:18 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Product
 *
 * @property int $id
 * @property int $product_id
 * @property string $name
 * @property string $sku
 * @property float $price
 * @property string $is_visible
 * @property string $is_featured
 * @property int $total_sold
 * @property int $view_count
 * @property string $custom_url
 * @property string $image_url
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Product extends Eloquent
{
    protected $casts = [
        'product_id' => 'int',
        'price'      => 'float',
        'total_sold' => 'int',
        'view_count' => 'int'
    ];

    protected $fillable = [
        'product_id',
        'name',
        'sku',
        'price',
        'is_visible',
        'is_featured',
        'total_sold',
        'view_count',
        'custom_url',
        'image_url',
        'description'
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function itemCard()
    {
        return $this->hasOne(ItemCard::class, 'original_code', 'sku');
    }
}

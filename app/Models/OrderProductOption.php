<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 28 Jan 2019 06:12:15 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class OrderProductOption
 *
 * @property int $id
 * @property int $product_id
 * @property string $display_name
 * @property string $display_value
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class OrderProductOption extends Eloquent
{
    protected $casts = [
        'product_id' => 'int'
    ];

    protected $fillable = [
        'product_id',
        'display_name',
        'display_value',
        'name'
    ];

    public function product()
    {
        return $this->belongsTo(OrderProduct::class);
    }
}

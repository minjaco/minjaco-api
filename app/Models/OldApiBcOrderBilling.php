<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ApiBcOrderBilling
 *
 * @property int $bc_id
 * @property string $bill_first_name
 * @property string $bill_last_name
 * @property string $bill_company
 * @property string $bill_street_1
 * @property string $bill_street_2
 * @property string $bill_city
 * @property string $bill_state
 * @property string $bill_zip
 * @property string $bill_country
 * @property string $bill_country_iso2
 * @property string $bill_phone
 * @property string $bill_email
 *
 * @package App\Models
 */
class OldApiBcOrderBilling extends Model
{
    protected $connection = 'mysql_old';
    protected $table = 'api_bc_order_billing';
    protected $primaryKey = 'bc_id';
    public $incrementing = false;
    public $timestamps = false;

    protected $casts = [
        'bc_id' => 'int',
    ];

    protected $fillable = [
        'bc_id',
        'bill_first_name',
        'bill_last_name',
        'bill_company',
        'bill_street_1',
        'bill_street_2',
        'bill_city',
        'bill_state',
        'bill_zip',
        'bill_country',
        'bill_country_iso2',
        'bill_phone',
        'bill_email',
    ];
}

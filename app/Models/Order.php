<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 28 Jan 2019 04:52:13 +0000.
 */

namespace App\Models;

use Carbon\Carbon;
use jdavidbakr\ReplaceableModel\ReplaceableModel;
use Neurony\Duplicate\Options\DuplicateOptions;
use Neurony\Duplicate\Traits\HasDuplicates;
use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Order
 *
 * @property int $id
 * @property int $order_id
 * @property int $customer_id
 * @property int $minja_id
 * @property string $order_from
 * @property string $order_source
 * @property int $items_total
 * @property string $country
 * @property string $payment_method
 * @property string $payment_provider_id
 * @property string $payment_status
 * @property string $external_source
 * @property int $bc_status_id
 * @property string $bc_status
 * @property int $mc_status_id
 * @property int $mc_old_status_id
 * @property int $urgent_order
 * @property int $has_multiple_products
 * @property float $coupon_discount
 * @property float $discount_amount
 * @property float $base_shipping_cost
 * @property float $subtotal_inc_tax
 * @property float $total_inc_tax
 * @property int $payment_option
 * @property string $customer_message
 * @property string $staff_notes
 * @property \Carbon\Carbon $show_date
 * @property \Carbon\Carbon $date_created
 * @property \Carbon\Carbon $date_modified
 * @property \Carbon\Carbon $date_shipped
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property mixed $billing_address
 *
 * @package App\Models
 */
class Order extends Eloquent
{
    //github.com/Neurony/laravel-duplicate
    use HasDuplicates, ReplaceableModel;

    public $incrementing = false;

    protected $casts = [
        'id'                    => 'int',
        'order_id'              => 'int',
        'customer_id'           => 'int',
        'minja_id'              => 'int',
        'items_total'           => 'int',
        'bc_status_id'          => 'int',
        'mc_status_id'          => 'int',
        'mc_old_status_id'      => 'int',
        'urgent_order'          => 'int',
        'has_multiple_products' => 'int',
        'coupon_discount'       => 'float',
        'discount_amount'       => 'float',
        'base_shipping_cost'    => 'float',
        'subtotal_inc_tax'      => 'float',
        'total_inc_tax'         => 'float',
        'payment_option'        => 'int',
    ];

    protected $dates = [
        'show_date',
        'date_created',
        'date_modified',
        'date_shipped',
    ];

    protected $fillable = [
        'id',
        'order_id',
        'customer_id',
        'minja_id',
        'order_from',
        'order_source',
        'items_total',
        'country',
        'payment_method',
        'payment_provider_id',
        'payment_status',
        'external_source',
        'bc_status_id',
        'bc_status',
        'mc_status_id',
        'mc_old_status_id',
        'urgent_order',
        'has_multiple_products',
        'coupon_discount',
        'discount_amount',
        'base_shipping_cost',
        'subtotal_inc_tax',
        'total_inc_tax',
        'payment_option',
        'customer_message',
        'staff_notes',
        'show_date',
        'date_created',
        'date_modified',
        'date_shipped',
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id', 'customer_id');
    }

    public function status()
    {
        return $this->hasOne(Status::class, 'id', 'mc_status_id');
    }

    public function products()
    {
        return $this->hasMany(OrderProduct::class, 'order_id', 'order_id');
    }

    public function notes()
    {
        return $this->hasMany(OrderNote::class, 'order_id', 'minja_id')->latest();
    }

    public function billings()
    {
        return $this->hasMany(OrderBilling::class);
    }

    public function swatches()
    {
        return $this->hasMany(OrderSwatch::class);
    }

    public function shipping()
    {
        return $this->hasMany(OrderShipping::class, 'order_id', 'order_id');
    }

    public function infoReceived()
    {
        return $this->hasOne(InfoReceived::class);
    }

    public function size()
    {
        return $this->hasOne(DetermineSize::class, 'order_id', 'minja_id');
    }

    public function sewingRawMaterials()
    {
        return $this->hasMany(SewingRawMaterial::class, 'order_id', 'minja_id');
    }

    public function crystalRawMaterials()
    {
        return $this->hasMany(CrystalRawMaterial::class, 'order_id', 'minja_id');
    }

    public function packingRawMaterials()
    {
        return $this->hasMany(PackingRawMaterial::class, 'order_id', 'minja_id');
    }

    public function scopeToday($query)
    {
        return $query->whereBetween('date_created', [Carbon::today()->startOfDay(), Carbon::today()->endOfDay()])
            ->orderBy('order_id', 'DESC');
    }

    /**
     * Set the options for the HasDuplicates trait.
     *
     * @return DuplicateOptions
     */
    public function getDuplicateOptions(): DuplicateOptions
    {
        return DuplicateOptions::instance();
    }
}

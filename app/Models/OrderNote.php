<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 05 Mar 2019 02:38:58 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class OrderNote
 *
 * @property int $id
 * @property int $order_id
 * @property int $user_id
 * @property string $content
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class OrderNote extends Eloquent
{
    protected $casts = [
        'order_id' => 'int',
        'user_id' => 'int'
    ];

    protected $fillable = [
        'order_id',
        'user_id',
        'content'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}

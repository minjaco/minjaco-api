<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 20 Jun 2019 16:36:33 +0700.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class OrderSwatch
 *
 * @property int $id
 * @property int $order_id
 * @property int $user_id
 * @property string $path
 * @property string $thumb_path
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class OrderSwatch extends Eloquent
{
    protected $casts = [
        'order_id' => 'int',
        'user_id'  => 'int',
    ];

    protected $fillable = [
        'order_id',
        'user_id',
        'path',
        'thumb_path',
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}

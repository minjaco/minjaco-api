<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 28 Jan 2019 06:12:08 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class OrderProduct
 *
 * @property int $id
 * @property int $order_id
 * @property int $product_id
 * @property string $name
 * @property string $sku
 * @property float $total_inc_tax
 * @property int $quantity
 * @property int $is_refunded
 * @property int $quantity_refunded
 * @property string $p_special_request
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class OrderProduct extends Eloquent
{
    protected $casts = [
        'order_id'          => 'int',
        'product_id'        => 'int',
        'total_inc_tax'     => 'float',
        'quantity'          => 'int',
        'is_refunded'       => 'int',
        'quantity_refunded' => 'int'
    ];

    protected $fillable = [
        'order_id',
        'product_id',
        'name',
        'sku',
        'total_inc_tax',
        'quantity',
        'is_refunded',
        'quantity_refunded',
        'p_special_request'
    ];

    public function options()
    {
        return $this->hasMany(OrderProductOption::class);
    }

    public function item_card()
    {
        return $this->hasOne(ItemCard::class, 'original_code', 'sku');
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function getSkuAttribute($value)
    {
        return str_replace('-', '', $value);
    }
}

<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ApiBcOrderShipping
 *
 * @property int $bc_id
 * @property string $ship_first_name
 * @property string $ship_last_name
 * @property string $ship_company
 * @property string $ship_street_1
 * @property string $ship_street_2
 * @property string $ship_city
 * @property string $ship_zip
 * @property string $ship_country
 * @property string $ship_country_iso2
 * @property string $ship_state
 * @property string $ship_email
 * @property string $ship_phone
 * @property int $ship_items_total
 * @property string $ship_method
 * @property float $ship_base_cost
 * @property float $ship_cost_ex_tax
 * @property float $ship_base_handling_cost
 * @property float $ship_handling_cost_ex_tax
 * @property int $ship_zone_id
 * @property string $ship_zone_name
 *
 * @package App\Models
 */
class OldApiBcOrderShipping extends Model
{
    protected $connection = 'mysql_old';
    protected $table = 'api_bc_order_shipping';
    protected $primaryKey = 'bc_id';
    public $incrementing = false;
    public $timestamps = false;

    protected $casts = [
        'bc_id'                     => 'int',
        'ship_items_total'          => 'int',
        'ship_base_cost'            => 'float',
        'ship_cost_ex_tax'          => 'float',
        'ship_base_handling_cost'   => 'float',
        'ship_handling_cost_ex_tax' => 'float',
        'ship_zone_id'              => 'int',
    ];

    protected $fillable = [
        'bc_id',
        'ship_first_name',
        'ship_last_name',
        'ship_company',
        'ship_street_1',
        'ship_street_2',
        'ship_city',
        'ship_zip',
        'ship_country',
        'ship_country_iso2',
        'ship_state',
        'ship_email',
        'ship_phone',
        'ship_items_total',
        'ship_method',
        'ship_base_cost',
        'ship_cost_ex_tax',
        'ship_base_handling_cost',
        'ship_handling_cost_ex_tax',
        'ship_zone_id',
        'ship_zone_name',
    ];
}

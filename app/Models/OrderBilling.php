<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 25 Jan 2019 04:56:31 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class OrderBilling
 *
 * @property int $id
 * @property int $order_id
 * @property string $first_name
 * @property string $last_name
 * @property string $company
 * @property string $street_1
 * @property string $street_2
 * @property string $city
 * @property string $zip
 * @property string $country
 * @property string $country_iso2
 * @property string $state
 * @property string $phone
 * @property string $email
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class OrderBilling extends Eloquent
{
    protected $casts = [
        'order_id' => 'int'
    ];

    protected $fillable = [
        'order_id',
        'first_name',
        'last_name',
        'company',
        'street_1',
        'street_2',
        'city',
        'zip',
        'country',
        'country_iso2',
        'state',
        'phone',
        'email'
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}

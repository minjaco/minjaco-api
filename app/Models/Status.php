<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 25 Jan 2019 10:09:17 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Status
 *
 * @property int $id
 * @property string $name
 * @property string $owner
 * @property int $order_by
 *
 * @package App\Models
 */
class Status extends Eloquent
{
    protected $table = 'status';
    public $timestamps = false;

    protected $casts = [
        'status_id' => 'int',
        'order_by' => 'int'
    ];

    protected $fillable = [
        'name',
        'owner',
        'order_by'
    ];
}

<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 03 Apr 2019 10:07:33 +0700.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Connector
 *
 * @property int $runid
 * @property int $con_id
 * @property string $con_code
 * @property string $con_name
 * @property string $con_old_name
 * @property string $con_sku
 * @property string $con_pic
 * @property string $con_type
 * @property int $con_status_info
 * @property int $con_status_bc
 * @property int $con_status_db
 *
 * @package App\Models
 */
class Connector extends Eloquent
{
    protected $primaryKey = 'runid';
    public $timestamps = false;

    protected $casts = [
        'con_id'          => 'int',
        'con_status_info' => 'int',
        'con_status_bc'   => 'int',
        'con_status_db'   => 'int'
    ];

    protected $fillable = [
        'con_id',
        'con_code',
        'con_name',
        'con_old_name',
        'con_sku',
        'con_pic',
        'con_type',
        'con_status_info',
        'con_status_bc',
        'con_status_db'
    ];
}

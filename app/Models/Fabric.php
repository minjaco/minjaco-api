<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 03 Apr 2019 10:33:50 +0700.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Fabric
 *
 * @property int $fabric_id
 * @property string $fabric_code
 * @property string $fabric_old_code
 * @property int $fabric_status
 * @property string $fabric_img
 * @property string $fabric_name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Fabric extends Eloquent
{
    protected $primaryKey = 'fabric_id';
    public $incrementing = false;

    protected $casts = [
        'fabric_id'     => 'int',
        'fabric_status' => 'int'
    ];

    protected $fillable = [
        'fabric_code',
        'fabric_old_code',
        'fabric_status',
        'fabric_img',
        'fabric_name'
    ];
}

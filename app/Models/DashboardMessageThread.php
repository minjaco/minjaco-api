<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 25 Mar 2019 16:33:44 +0700.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class DashboardMessageThread
 *
 * @property int $id
 * @property int $dashboard_message_id
 * @property int $user_id
 * @property string $content
 * @property int $priority
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class DashboardMessageThread extends Eloquent
{
    protected $casts = [
        'dashboard_message_id' => 'int',
        'user_id'              => 'int',
        'priority'             => 'int'
    ];

    protected $fillable = [
        'dashboard_message_id',
        'user_id',
        'content',
        'priority'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function message()
    {
        return $this->belongsTo(DashboardMessage::class);
    }
}

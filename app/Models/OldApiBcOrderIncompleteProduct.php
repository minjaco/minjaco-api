<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ApiBcOrderIncompleteProduct
 *
 * @property int $runid
 * @property int $bc_id
 * @property string $in_product_sku
 * @property Carbon $in_show_date
 * @property string $in_product_comment
 * @property int $in_payment_option
 *
 * @package App\Models
 */
class OldApiBcOrderIncompleteProduct extends Model
{
    protected $connection = 'mysql_old';
    protected $table = 'api_bc_order_incomplete_product';
    protected $primaryKey = 'runid';
    public $timestamps = false;

    protected $casts = [
        'bc_id'             => 'int',
        'in_payment_option' => 'int',
    ];

    protected $dates = [
        'in_show_date',
    ];

    protected $fillable = [
        'bc_id',
        'in_product_sku',
        'in_show_date',
        'in_product_comment',
        'in_payment_option',
    ];
}

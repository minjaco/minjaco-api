<?php

namespace App\Exports\FinishedSuitsSheets;

use Illuminate\Database\Query\Builder;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;

class FinishedSuitsCalculationListSheet implements FromQuery, WithTitle, WithHeadings, ShouldAutoSize
{
    private $begin_date;
    private $end_date;

    public function __construct($begin_date, $end_date)
    {
        $this->begin_date = $begin_date;
        $this->end_date   = $end_date;
    }

    /**
     * @return Builder
     */
    public function query()
    {
        return \DB::connection('mysql_old')
            ->table('tb_orders')
            ->select(
                \DB::raw(
                    "(
				CASE
						WHEN product_code LIKE 'CB%' THEN
						'CB'
						WHEN product_code LIKE 'DV%' THEN
						'DV'
						WHEN product_code LIKE 'PB%' THEN
						'PB'
						WHEN product_code LIKE 'RB%' THEN
						'RB'
						WHEN product_code LIKE 'TR%' OR 'MT%' THEN
						'TR/MT'
						WHEN product_code LIKE 'CJ%' THEN
						'CJ'
						WHEN product_code LIKE 'CO%' OR 'CONN%' THEN
						'CO/CONN'
						WHEN product_code LIKE 'Fabric%' THEN
						'Fabric'
						ELSE 'Other'
						END )"
                ),
                \DB::raw('COUNT(*)')
            )
            ->join('tb_orders_management', 'tb_orders.order_id', '=', 'tb_orders_management.order_id')
            ->leftJoin('invoice_data', 'invoice_data.invoice_order', '=', 'tb_orders.order_id')
            ->whereBetween('tb_orders_management.take_photo_date', [$this->begin_date, $this->end_date])
            ->orderByRaw('COUNT(*) DESC')
            ->groupBy(
                \DB::raw(
                    "(
				CASE
						WHEN product_code LIKE 'CB%' THEN
						'CB'
						WHEN product_code LIKE 'DV%' THEN
						'DV'
						WHEN product_code LIKE 'PB%' THEN
						'PB'
						WHEN product_code LIKE 'RB%' THEN
						'RB'
						WHEN product_code LIKE 'TR%'
						OR 'MT%' THEN
							'TR/MT'
							WHEN product_code LIKE 'CJ%' THEN
							'CJ'
							WHEN product_code LIKE 'CO%'
							OR 'CONN%' THEN
								'CO/CONN'
								WHEN product_code LIKE 'Fabric%' THEN
								'Fabric' ELSE 'Other'
							END
							)"
                )
            );
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Calculation List';
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'Product Type',
            'Count',
        ];
    }
}

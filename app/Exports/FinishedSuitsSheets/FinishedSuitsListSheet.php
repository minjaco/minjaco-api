<?php

namespace App\Exports\FinishedSuitsSheets;

use Illuminate\Database\Query\Builder;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;

class FinishedSuitsListSheet implements FromQuery, WithTitle, WithHeadings, ShouldAutoSize
{
    private $begin_date;
    private $end_date;

    public function __construct($begin_date, $end_date)
    {
        $this->begin_date = $begin_date;
        $this->end_date   = $end_date;
    }

    /**
     * @return Builder
     */
    public function query()
    {
        return \DB::connection('mysql_old')
            ->table('tb_orders')
            ->join('tb_orders_management', 'tb_orders.order_id', '=', 'tb_orders_management.order_id')
            ->leftJoin('invoice_data', 'invoice_data.invoice_order', '=', 'tb_orders.order_id')
            ->whereBetween('tb_orders_management.take_photo_date', [$this->begin_date, $this->end_date])
            ->orderBy('tb_orders.order_id')
            ->select(
                'tb_orders_management.take_photo_date',
                'tb_orders.order_id',
                'invoice_data.invoice_no',
                'tb_orders.product_code',
                \DB::raw('"1" as quantity')
            );
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Product List';
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'Take Photos Date (Final QC)',
            'Minja Order No.',
            'Invoice No.',
            'Product Code',
            'Quantity',
        ];
    }
}

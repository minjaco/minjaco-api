<?php

namespace App\Exports\ShippingReportSheets;

use Illuminate\Database\Query\Builder;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;

class ShippingListSheet implements FromQuery, WithTitle, WithHeadings, ShouldAutoSize
{
    private $begin_date;
    private $end_date;

    public function __construct($begin_date, $end_date)
    {
        $this->begin_date = $begin_date;
        $this->end_date   = $end_date;
    }

    /**
     * @return Builder
     */
    public function query()
    {
        return \DB::connection('mysql_old')
            ->table('tb_orders')
            ->leftJoin('tb_orders_shipping', 'tb_orders_shipping.order_id', '=', 'tb_orders.order_id')
            ->leftJoin('payment_deposit', 'tb_orders.order_id', '=', 'payment_deposit.order_id')
            ->leftJoin('invoice_data', 'invoice_data.invoice_order', '=', 'tb_orders.order_id')
            ->whereRaw(
                'tb_orders.order_id IN ( SELECT invoice_order FROM invoice_data WHERE send_day BETWEEN ? AND ? ORDER BY invoice_order )',
                [$this->begin_date, $this->end_date]
            )
            ->orderByRaw('invoice_data.send_day, invoice_data.tracking_number ')
            ->select(
                'invoice_data.send_day',
                \DB::raw(" ( 'Minja' ) AS ME"),
                'invoice_data.tracking_number',
                'tb_orders.order_id',
                \DB::raw("IF( payment_deposit.transection_id != '', CONCAT( tb_orders_shipping.TransectionID, '-', payment_deposit.transection_id ), tb_orders_shipping.TransectionID ) AS transection_id"),
                'tb_orders.product_code',
                \DB::raw('"1" as quantity'),
                \DB::raw('FORMAT(tb_orders.grand_total, 2) as price'),
                'invoice_data.invoice_no'
            );
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Shipping List';
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'Actual Ship Date',
            'DHL Account To Use',
            'Tracking Number',
            'Minja Order Id.',
            'TransactionID',
            'Product Code',
            'Quantity',
            'Price',
            'Invoice No.',
        ];
    }
}

<?php

namespace App\Exports;

use App\Exports\SewnReportSheets\SewingAccCodeCalculationListSheet;
use App\Exports\SewnReportSheets\SewingAllCodeCalculationListSheet;
use App\Exports\SewnReportSheets\SewingFabCodeCalculationListSheet;
use App\Exports\SewnReportSheets\SewingMaterialListSheet;
use App\Exports\SewnReportSheets\SewingOtherCodeCalculationListSheet;
use App\Exports\SewnReportSheets\SewingPdConCodeCalculationListSheet;
use App\Exports\SewnReportSheets\SewingPdCupCodeCalculationListSheet;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class SewingReport implements WithMultipleSheets
{
    use Exportable;

    protected $begin_date;
    protected $end_date;

    public function __construct($begin_date, $end_date)
    {
        $this->begin_date = $begin_date;
        $this->end_date   = $end_date;
    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        return [
            new SewingMaterialListSheet($this->begin_date, $this->end_date),
            new SewingAllCodeCalculationListSheet($this->begin_date, $this->end_date),
            new SewingFabCodeCalculationListSheet($this->begin_date, $this->end_date),
            new SewingPdConCodeCalculationListSheet($this->begin_date, $this->end_date),
            new SewingPdCupCodeCalculationListSheet($this->begin_date, $this->end_date),
            new SewingAccCodeCalculationListSheet($this->begin_date, $this->end_date),
            new SewingOtherCodeCalculationListSheet($this->begin_date, $this->end_date),
        ];
    }
}

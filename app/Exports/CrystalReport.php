<?php

namespace App\Exports;


use App\Exports\CrystalReportSheets\CrystalAllCodeCalculationListSheet;
use App\Exports\CrystalReportSheets\CrystalMaterialListSheet;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class CrystalReport implements WithMultipleSheets
{
    use Exportable;

    protected $begin_date;
    protected $end_date;

    public function __construct($begin_date, $end_date)
    {
        $this->begin_date = $begin_date;
        $this->end_date   = $end_date;
    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        return [
            new CrystalMaterialListSheet($this->begin_date, $this->end_date),
            new CrystalAllCodeCalculationListSheet($this->begin_date, $this->end_date),
        ];
    }
}

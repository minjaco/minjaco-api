<?php

namespace App\Exports\FinishGoodReportSheets;

use Illuminate\Database\Query\Builder;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;

class FinishGoodFabricCodeCalculationListSheet implements FromQuery, WithHeadings, ShouldAutoSize, WithTitle
{
    private $begin_date;
    private $end_date;

    public function __construct($begin_date, $end_date)
    {
        $this->begin_date = $begin_date;
        $this->end_date   = $end_date;
    }

    /**
     * @return Builder
     */
    public function query()
    {
        return \DB::connection('mysql_old')
            ->table('tb_orders')
            ->join('invoice_data', 'invoice_data.invoice_order', '=', 'tb_orders.order_id')
            ->where('product_code', 'LIKE', 'Fabric%')
            ->whereBetween('tb_orders.time_stamp', [$this->begin_date, $this->end_date])
            ->groupBy('product_code')
            ->orderBy('product_code')
            ->select(
                'product_code',
                \DB::raw('COUNT(product_code)'),
                'grand_total',
                \DB::raw('ROUND(COUNT(product_code) * grand_total, 2)')
            );
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Finish Goods Fabric Code';
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'Code',
            'All Quantity',
            'Unit Price',
            'Amount',
        ];
    }
}

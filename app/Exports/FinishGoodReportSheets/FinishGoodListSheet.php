<?php

namespace App\Exports\FinishGoodReportSheets;

use Illuminate\Database\Query\Builder;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;

class FinishGoodListSheet implements FromQuery, WithTitle, WithHeadings, ShouldAutoSize
{
    private $begin_date;
    private $end_date;

    public function __construct($begin_date, $end_date)
    {
        $this->begin_date = $begin_date;
        $this->end_date   = $end_date;
    }

    /**
     * @return Builder
     */
    public function query()
    {
        return \DB::connection('mysql_old')
            ->table('tb_orders')
            ->join('invoice_data', 'invoice_data.invoice_order', '=', 'tb_orders.order_id')
            ->where(function ($query) {
                $query->where('tb_orders.product_code', 'LIKE', 'CJ%')
                    ->orWhere('tb_orders.product_code', 'LIKE', 'CONN%')
                    ->orWhere('tb_orders.product_code', 'LIKE', 'Fabric%')
                    ->orWhere('tb_orders.product_code', 'LIKE', 'AC%');
            })
            ->whereBetween('tb_orders.time_stamp', [$this->begin_date, $this->end_date])
            ->orderBy('tb_orders.order_id')
            ->select(
                'tb_orders.order_id',
                'invoice_data.invoice_no',
                'tb_orders.product_code',
                \DB::raw('"" as part_number'),
                \DB::raw('"" as description'),
                \DB::raw('"1" as quantity'),
                \DB::raw('"Set" as unit'),
                \DB::raw('ROUND(tb_orders.grand_total,4)'),
                \DB::raw('FORMAT(1 * tb_orders.grand_total, 4)')
            );
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Finish Goods List';
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'Minja Order No.',
            'Invoice No.',
            'Product Code',
            'Material Code',
            'Material Description',
            'Quantity',
            'Unit',
            'Unit Price',
            'Total Price',
        ];
    }
}

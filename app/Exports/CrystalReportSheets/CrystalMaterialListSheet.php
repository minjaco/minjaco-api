<?php

namespace App\Exports\CrystalReportSheets;

use Illuminate\Database\Query\Builder;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;

class CrystalMaterialListSheet implements FromQuery, WithTitle, WithHeadings, ShouldAutoSize
{
    private $begin_date;
    private $end_date;

    public function __construct($begin_date, $end_date)
    {
        $this->begin_date = $begin_date;
        $this->end_date   = $end_date;
    }

    /**
     * @return Builder
     */
    public function query()
    {
        return \DB::connection('mysql_old')
            ->table('tb_orders')
            ->join('tb_orders_management', 'tb_orders.order_id', '=', 'tb_orders_management.order_id')
            ->leftJoin('order_crystal_raw_mat', 'tb_orders.order_id', '=', 'order_crystal_raw_mat.order_id')
            ->leftJoin('Crystal', 'order_crystal_raw_mat.part_number_id', '=', 'Crystal.runid')
            ->leftJoin('tb_stock', 'order_crystal_raw_mat.part_number_id', '=', 'tb_stock.runid')
            ->leftJoin('invoice_data', 'invoice_data.invoice_order', '=', 'tb_orders.order_id')
            ->whereBetween('tb_orders_management.ppm_date', [$this->begin_date, $this->end_date])
            ->orderBy('tb_orders.order_id')
            ->select(
                'tb_orders.order_id',
                'invoice_data.invoice_no',
                'tb_orders.product_code',
                'order_crystal_raw_mat.part_number',
                'order_crystal_raw_mat.description',
                'order_crystal_raw_mat.quantity',
                'order_crystal_raw_mat.unit',
                \DB::raw('ROUND(Crystal.unit_price,4)'),
                \DB::raw('FORMAT(order_crystal_raw_mat.quantity * Crystal.unit_price, 4)')
            );
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Crystal Material List';
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'Minja Order No.',
            'Invoice No.',
            'Product Code',
            'Material Code',
            'Material Description',
            'Quantity',
            'Unit',
            'Unit Price',
            'Total Price',
        ];
    }
}

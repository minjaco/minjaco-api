<?php

namespace App\Exports\CrystalReportSheets;

use Illuminate\Database\Query\Builder;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;

class CrystalAllCodeCalculationListSheet implements FromQuery, WithHeadings, ShouldAutoSize, WithTitle
{
    private $begin_date;
    private $end_date;

    public function __construct($begin_date, $end_date)
    {
        $this->begin_date = $begin_date;
        $this->end_date   = $end_date;
    }

    /**
     * @return Builder
     */
    public function query()
    {
        return \DB::connection('mysql_old')
            ->table('order_crystal_raw_mat')
            ->join('tb_orders', 'tb_orders.order_id', '=', 'order_crystal_raw_mat.order_id')
            ->join('Crystal', 'Crystal.runid', '=', 'order_crystal_raw_mat.part_number_id')
            ->whereBetween('order_crystal_raw_mat.time_stamp', [$this->begin_date, $this->end_date])
            ->groupBy('part_number')
            ->orderBy('part_number')
            ->select(
                'part_number',
                \DB::raw('SUM(order_crystal_raw_mat.quantity)'),
                \DB::raw('ROUND(Crystal.unit_price,4)'),
                \DB::raw('ROUND(SUM(order_crystal_raw_mat.quantity) * Crystal.unit_price,4)')
            );
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Crystal All Code';
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'Code',
            'All Quantity',
            'Unit Price',
            'Amount',
        ];
    }
}

<?php

namespace App\Exports\SewnReportByStatusSheets;

use App\Enums\OrderStatus;
use Illuminate\Database\Query\Builder;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;

class SewingMaterialListSheet implements FromQuery, WithTitle, WithHeadings, ShouldAutoSize
{
    private $begin_date;
    private $end_date;
    private $status;

    public function __construct($begin_date, $end_date, $status)
    {
        $this->begin_date = $begin_date;
        $this->end_date   = $end_date;
        $this->status     = $status;
    }

    /**
     * @return Builder
     */
    public function query()
    {
        return \DB::connection('mysql_old')
            ->table('tb_orders')
            ->join('tb_orders_management', 'tb_orders.order_id', '=', 'tb_orders_management.order_id')
            ->leftJoin('order_sewing_raw_mat', 'tb_orders.order_id', '=', 'order_sewing_raw_mat.order_id')
            ->leftJoin('tb_stock', 'order_sewing_raw_mat.part_number_id', '=', 'tb_stock.runid')
            ->leftJoin('invoice_data', 'invoice_data.invoice_order', '=', 'tb_orders_management.order_id')
            ->where('tb_orders.status_id', $this->status)
            ->whereBetween('tb_orders_management.ppm_date', [$this->begin_date, $this->end_date])
            ->orderBy('tb_orders.order_id')
            ->select(
                'tb_orders.order_id',
                'invoice_data.invoice_no',
                'tb_orders.product_code',
                'order_sewing_raw_mat.part_number',
                'order_sewing_raw_mat.description',
                'order_sewing_raw_mat.quantity',
                'order_sewing_raw_mat.unit',
                'tb_stock.unit_price',
                \DB::raw('FORMAT(order_sewing_raw_mat.quantity * tb_stock.unit_price, 2)')
            );
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Sewing Material List';
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'Minja Order No.',
            'Invoice No.',
            'Product Code',
            'Material Code',
            'Material Description',
            'Quantity',
            'Unit',
            'Unit Price',
            'Total Price',
        ];
    }
}

<?php

namespace App\Exports\SewnReportByStatusSheets;

use App\Enums\OrderStatus;
use Illuminate\Database\Query\Builder;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;

class SewingAccCodeCalculationListSheet implements FromQuery, WithHeadings, ShouldAutoSize, WithTitle
{
    private $begin_date;
    private $end_date;
    private $status;

    public function __construct($begin_date, $end_date, $status)
    {
        $this->begin_date = $begin_date;
        $this->end_date   = $end_date;
        $this->status     = $status;
    }

    /**
     * @return Builder
     */
    public function query()
    {
        return \DB::connection('mysql_old')
            ->table('tb_orders')
            ->join('tb_orders_management', 'tb_orders.order_id', '=', 'tb_orders_management.order_id')
            ->leftJoin('order_sewing_raw_mat', 'tb_orders.order_id', '=', 'order_sewing_raw_mat.order_id')
            ->leftJoin('tb_stock', 'order_sewing_raw_mat.part_number_id', '=', 'tb_stock.runid')
            ->leftJoin('invoice_data', 'invoice_data.invoice_order', '=', 'tb_orders_management.order_id')
            ->where('part_number', 'LIKE', 'ACC-%')
            ->where('tb_orders.status_id', $this->status)
            ->whereBetween('tb_orders_management.ppm_date', [$this->begin_date, $this->end_date])
            ->groupBy('part_number')
            ->orderBy('part_number')
            ->select(
                'part_number',
                \DB::raw('SUM(order_sewing_raw_mat.quantity)'),
                'unit_price',
                \DB::raw('ROUND(SUM(order_sewing_raw_mat.quantity) * unit_price,2)')
            );
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Sewing ACC Code';
    }


    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'Code',
            'All Quantity',
            'Unit Price',
            'Amount',
        ];
    }
}

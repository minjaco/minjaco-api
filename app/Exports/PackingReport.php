<?php

namespace App\Exports;

use App\Exports\PackingReportSheets\PackingAllCodeCalculationListSheet;
use App\Exports\PackingReportSheets\PackingMaterialListSheet;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class PackingReport implements WithMultipleSheets
{
    use Exportable;

    protected $begin_date;
    protected $end_date;

    public function __construct($begin_date, $end_date)
    {
        $this->begin_date = $begin_date;
        $this->end_date   = $end_date;
    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        return [
            new PackingMaterialListSheet($this->begin_date, $this->end_date),
            new PackingAllCodeCalculationListSheet($this->begin_date, $this->end_date),
        ];
    }
}

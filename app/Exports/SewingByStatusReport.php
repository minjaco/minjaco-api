<?php

namespace App\Exports;

use App\Exports\SewnReportByStatusSheets\SewingAccCodeCalculationListSheet;
use App\Exports\SewnReportByStatusSheets\SewingAllCodeCalculationListSheet;
use App\Exports\SewnReportByStatusSheets\SewingFabCodeCalculationListSheet;
use App\Exports\SewnReportByStatusSheets\SewingMaterialListSheet;
use App\Exports\SewnReportByStatusSheets\SewingOtherCodeCalculationListSheet;
use App\Exports\SewnReportByStatusSheets\SewingPdConCodeCalculationListSheet;
use App\Exports\SewnReportByStatusSheets\SewingPdCupCodeCalculationListSheet;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class SewingByStatusReport implements WithMultipleSheets
{
    use Exportable;

    protected $begin_date;
    protected $end_date;
    protected $status;


    public function __construct($begin_date, $end_date, $status)
    {
        $this->begin_date = $begin_date;
        $this->end_date   = $end_date;
        $this->status     = $status;
    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        return [
            new SewingMaterialListSheet($this->begin_date, $this->end_date, $this->status),
            new SewingAllCodeCalculationListSheet($this->begin_date, $this->end_date, $this->status),
            new SewingFabCodeCalculationListSheet($this->begin_date, $this->end_date, $this->status),
            new SewingPdConCodeCalculationListSheet($this->begin_date, $this->end_date, $this->status),
            new SewingPdCupCodeCalculationListSheet($this->begin_date, $this->end_date, $this->status),
            new SewingAccCodeCalculationListSheet($this->begin_date, $this->end_date, $this->status),
            new SewingOtherCodeCalculationListSheet($this->begin_date, $this->end_date, $this->status),
        ];
    }
}

<?php

namespace App\Exports;

use App\Exports\FinishGoodReportSheets\FinishGoodAcCodeCalculationListSheet;
use App\Exports\FinishGoodReportSheets\FinishGoodCjCodeCalculationListSheet;
use App\Exports\FinishGoodReportSheets\FinishGoodConnCodeCalculationListSheet;
use App\Exports\FinishGoodReportSheets\FinishGoodFabricCodeCalculationListSheet;
use App\Exports\FinishGoodReportSheets\FinishGoodListSheet;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class FinishGoodReport implements WithMultipleSheets
{
    use Exportable;

    protected $begin_date;
    protected $end_date;

    public function __construct($begin_date, $end_date)
    {
        $this->begin_date = $begin_date;
        $this->end_date   = $end_date;
    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        return [
            new FinishGoodListSheet($this->begin_date, $this->end_date),
            new FinishGoodCjCodeCalculationListSheet($this->begin_date, $this->end_date),
            new FinishGoodAcCodeCalculationListSheet($this->begin_date, $this->end_date),
            new FinishGoodConnCodeCalculationListSheet($this->begin_date, $this->end_date),
            new FinishGoodFabricCodeCalculationListSheet($this->begin_date, $this->end_date),
        ];
    }
}

<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class InvoiceExport implements FromView
{
    public $invid;

    public function __construct($invid)
    {
        $this->invid = $invid;
    }

    public function view(): View
    {
        $invoice_id = $this->invid;

        $invoice = \DB::connection('mysql_old')
            ->table('invoice_data')
            ->where('invoice_no', $invoice_id)
            ->first();

        $order = \DB::connection('mysql_old')
            ->table('tb_orders')
            ->where('order_id', $invoice->invoice_order)
            ->first();

        $bc_order = \DB::connection('mysql_old')
            ->table('api_bc_order')
            ->where('bc_id', $order->order_ref_no)
            ->first();

        $order_products = \DB::connection('mysql_old')
            ->table('api_bc_order_products')
            ->where('bc_id', $order->order_ref_no)
            ->get();

        $billing = \DB::connection('mysql_old')
            ->table('api_bc_order_billing')
            ->where('bc_id', $order->order_ref_no)
            ->first();

        $shipping = \DB::connection('mysql_old')
            ->table('api_bc_order_shipping')
            ->where('bc_id', $order->order_ref_no)
            ->first();


        $ship_with = \DB::connection('mysql_old')
            ->table('ship_with')
            ->where('order_id', $order->order_id)
            ->get()
            ->pluck('ship_with');

        $ship_with = implode(',', array_values($ship_with->toArray()));

        $order_data = \DB::connection('mysql_old')
            ->table('tb_orders')
            ->whereRaw('order_id in(SELECT order_id FROM ship_with WHERE ship_with IN (' . $ship_with . ') OR order_id=' . $order->order_id . ') AND status_id != \'26\'')
            ->get();


        //$order_data = $callD->SelectOne('tb_orders','*'," WHERE (order_id in(SELECT order_id FROM ship_with WHERE ship_with='{$sw_data[1]['ship_with']}') OR order_id='{$ordernum}') AND status_id != '26'",2);

        //dd($invoice, $order, $bc_order, $order_products, $billing, $shipping, $order_data);

        //$order_shipping = \DB::connection('mysql_old')->table('tb_orders_shipping');

        return view(
            'exports.invoice',
            compact(
                'invoice_id',
                'invoice',
                'order',
                'bc_order',
                'order_products',
                'billing',
                'shipping',
                'order_data'
            )
        );
    }
}

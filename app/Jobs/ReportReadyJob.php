<?php

namespace App\Jobs;

use App\Mail\SendReportEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ReportReadyJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $file_path;
    public $mail_to;
    public $report_name;

    /**
     * Create a new job instance.
     *
     * @param $file_path
     * @param $mail_to
     */
    public function __construct($report_name, $file_path, $mail_to)
    {
        $this->report_name = $report_name;
        $this->file_path   = $file_path;
        $this->mail_to     = $mail_to;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Mail::to($this->mail_to)->send(new SendReportEmail($this->report_name, $this->file_path));
        \Storage::delete('reports/'.$this->file_path);
    }
}

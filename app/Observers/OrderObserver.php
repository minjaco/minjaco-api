<?php

namespace App\Observers;

use App\Models\Order;
use Illuminate\Http\Request;

class OrderObserver
{

    /*protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }*/

    /**
     * Handle the order "created" event.
     *
     * @param \App\Models\Order $order
     * @return void
     */
    public function created(Order $order)
    {
        \Cache::forget('today-orders');
    }

    /**
     * Handle the order "updating" event.
     *
     * @param \App\Models\Order $order
     * @return void
     */
    public function updating(Order $order)
    {
        activity('order-activity')
            ->by(auth()->user())
            ->on($order)
            ->withProperties([
                'order_id' => $order->minja_id,
                'type'     => 'update',
                [
                    'attributes' => [
                        'name' => 'mc_status_id',
                        'text' => $order->mc_status_id,
                    ],
                    'old'        => [
                        'name' => 'mc_status_id',
                        'text' => $order->getOriginal('mc_status_id'),
                    ],
                ]
            ])
            ->log('Order ##LINK## has been updated');
    }

    public function updated(Order $order)
    {
        \Cache::forget(md5($order->id) . '-order-details');
    }

    /**
     * Handle the order "deleted" event.
     *
     * @param \App\Models\Order $order
     * @return void
     */
    public function deleted(Order $order)
    {
        activity('order-activity')
            ->by(auth()->user())
            ->on($order)
            ->log('Order ##LINK## has been deleted');
    }
}

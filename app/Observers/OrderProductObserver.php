<?php

namespace App\Observers;

use App\Models\OrderProduct;

class OrderProductObserver
{
    public function deleting(OrderProduct $orderProduct)
    {
        $orderProduct->options()->delete();
    }
}

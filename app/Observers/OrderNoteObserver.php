<?php

namespace App\Observers;

use App\Models\OrderNote;
use Illuminate\Http\Request;
use Spatie\Activitylog\Models\Activity;

class OrderNoteObserver
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Handle the order note "created" event.
     *
     * @param \App\Models\OrderNote $orderNote
     * @return void
     */
    public function created(OrderNote $orderNote)
    {
        activity('order-note-activity')
            ->causedBy(auth()->user())
            ->performedOn($orderNote)
            ->withProperties([
                'order_id' => $orderNote->order_id,
                'note'     => $orderNote->content,
                'type'     => 'note'
            ])
            ->tap(function (Activity $activity) use ($orderNote) {
                $activity->parent_id = $orderNote->order_id;
            })
            ->log('A note has been added to order ##LINK##');
        //->log('The subject name is :subject.name, the causer name is :causer.name and Laravel is :properties.laravel');

        \Cache::forget(md5($orderNote->order_id) . '-order-details');
    }


    /**
     * Handle the order note "deleted" event.
     *
     * @param \App\Models\OrderNote $orderNote
     * @return void
     */
    public function deleted(OrderNote $orderNote)
    {
        activity('order-note-activity')
            ->by(auth()->user())
            ->on($orderNote)
            ->withProperties([
                'order_id' => $orderNote->order_id,
                'note'     => $orderNote->content,
                'type'     => 'note'
            ])
            ->tap(function (Activity $activity) use ($orderNote) {
                $activity->parent_id = $orderNote->order_id;
            })
            ->log('A note has been deleted from order ##LINK##');

        \Cache::forget(md5($orderNote->order_id) . '-order-details');
    }
}

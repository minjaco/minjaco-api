<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('websockets:clean')->daily();
        //$schedule->command('etsy:orders')->everyFiveMinutes();
        //BigCommerceOrders.php
        //$schedule->command('bc:orders')->withoutOverlapping()->everyMinute();
        //$schedule->command('minja:get-info-received')->withoutOverlapping()->everyMinute();
        //$schedule->command('minja:get-item-cards')->hourly();
        //$schedule->command('minja:clean-incomplete-orders')->dailyAt('00:00');

        //Maintenance
        $schedule->command('monitor:check-uptime')->everyMinute();
        $schedule->command('monitor:check-certificate')->daily();

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}

<?php

namespace App\Console\Commands;

use App\Imports\CrystalImporter;
use Illuminate\Console\Command;

class ImportCrystals extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'imp:crystals';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imports crystals';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->output->title('Starting import');
        (new CrystalImporter())->withOutput($this->output)->import(storage_path('crystals.xlsx'));
        $this->output->success('Import successful');
    }
}

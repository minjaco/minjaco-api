<?php

namespace App\Console\Commands;

use App\Enums\OrderStatus;
use App\Enums\OrderType;
use App\Enums\PaymentOption;
use App\Models\Customer;
use App\Models\OldOrder;
use App\Models\Order;
use App\Models\OrderBilling;
use App\Models\OrderProduct;
use App\Models\OrderProductOption;
use App\Models\OrderShipping;
use Carbon\Carbon;
use Gentor\Etsy\Facades\Etsy;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class EtsyOrderReceived extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'etsy:order';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Saves Etsy order into MinjaCo';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $orders = app(OldOrder::class)
            ->where('order_from', 'ETSY')
            ->where('order_id', '>', 181027)
            ->get();

        foreach ($orders as $order) {

            $args = [
                'params'       => [
                    'receipt_id' => $order->order_ref_no,
                ],
                'associations' => [
                    'Transactions',
                    'Country',
                    'Buyer',
                ],
            ];


            $data = Etsy::getShop_Receipt2($args);

            foreach ($data['results'] as $result) {
                $check_customer = app(Customer::class)
                    ->where('customer_id', $result['buyer_user_id'])
                    ->first();

                if (!$check_customer) {
                    $c               = new Customer();
                    $c->customer_id  = $result['buyer_user_id'];
                    $c->uuid         = Str::uuid();
                    $c->name         = $result['name'];
                    $c->phone_number = '';
                    $c->email        = $result['buyer_email'];
                    if (isset($result['Buyer']['creation_tsz']) && $result['Buyer']['creation_tsz']) {
                        $c->registered_at = Carbon::createFromTimestamp($result['Buyer']['creation_tsz'])->format('Y-m-d H:i:s');
                    } else {
                        $c->registered_at = null;
                    }
                    $c->accepts_marketing = 0;
                    $c->address           = $result['first_line'] . ' ' . $result['second_line'] . ' ' . $result['city'] . ' ' . $result['state'] . ' ' . $result['zip'];
                    $c->country           = $result['Country']['name'];


                    $c->save();
                }

                $o                      = new Order();
                $o->order_id            = $result['receipt_id'];
                $o->minja_id            = $order->order_id;
                $o->customer_id         = $result['buyer_user_id'];
                $o->order_from          = 'ETSY';
                $o->order_source        = '';
                $o->items_total         = count($result['Transactions']);
                $o->country             = $result['Country']['name'];
                $o->payment_method      = $result['payment_method'];
                $o->payment_option      = PaymentOption::FULL;
                $o->payment_provider_id = '';
                $o->payment_status      = '';
                $o->external_source     = '';
                $o->bc_status_id        = 0;
                $o->bc_status           = 0; // Order status is 0 (incompleted)
                $o->mc_status_id        = $order->status_id;
                $o->coupon_discount     = 0;
                $o->discount_amount     = $result['discount_amt'];
                $o->base_shipping_cost  = $result['total_shipping_cost'];
                $o->subtotal_inc_tax    = $result['subtotal'];
                $o->total_inc_tax       = $result['grandtotal'];
                $o->customer_message    = $result['message_from_buyer'];
                $o->staff_notes         = '';

                if ($result['creation_tsz']) {
                    $o->date_created = Carbon::createFromTimestamp($result['creation_tsz'])->format('Y-m-d H:i:s');
                } else {
                    $o->date_created = null;
                }

                if ($result['shipped_date']) {
                    $o->date_shipped = Carbon::createFromTimestamp($result['shipped_date'])->format('Y-m-d H:i:s');
                } else {
                    $o->date_shipped = null;
                }

                $name = explode(' ', $result['name']);

                $ob               = new OrderBilling();
                $ob->order_id     = $result['receipt_id'];
                $ob->first_name   = trim($name[0]);
                $ob->last_name    = trim(isset($name[1]) ? $name[1] : '');
                $ob->company      = '';
                $ob->street_1     = $result['first_line'];
                $ob->street_2     = $result['second_line'];
                $ob->city         = $result['city'];
                $ob->zip          = $result['zip'];
                $ob->state        = $result['state'];
                $ob->country      = $result['Country']['name'];
                $ob->country_iso2 = $result['Country']['iso_country_code'];
                $ob->phone        = '';
                $ob->email        = $result['buyer_email'];

                $ob->save();


                $os                  = new OrderShipping();
                $os->order_id        = $result['receipt_id'];
                $os->first_name      = trim($name[0]);
                $os->last_name       = trim(isset($name[1]) ? $name[1] : '');
                $os->company         = '';
                $os->street_1        = $result['first_line'];
                $os->street_2        = $result['second_line'];
                $os->city            = $result['city'];
                $os->zip             = $result['zip'];
                $os->state           = $result['state'];
                $os->country         = $result['Country']['name'];
                $os->country_iso2    = $result['Country']['iso_country_code'];
                $os->phone           = '';
                $os->email           = $result['buyer_email'];
                $os->items_total     = count($result['Transactions']);
                $os->shipping_method = isset($result['shipping_details']['shipping_method']) ? $result['shipping_details']['shipping_method'] : '';
                $os->base_cost       = $result['total_shipping_cost'];
                $os->cost_inc_tax    = $result['total_shipping_cost'];
                $os->save();


                $products = [];

                foreach ($result['Transactions'] as $transaction) {
                    $products [] = [
                        'product_id'    => $transaction['product_data']['product_id'],
                        'name'          => $transaction['title'],
                        'sku'           => $transaction['product_data']['sku'],
                        'quantity'      => $transaction['quantity'],
                        'price_inc_tax' => $result['total_price'],
                        'product_data'  => $transaction['product_data'],
                    ];
                }

                if ($products) {
                    $d = [];

                    if (count($products) > 1) { // check the order has multiple products for linking
                        $o->has_multiple_products = 1;
                    }

                    foreach ($products as $product) {
                        $p                    = new OrderProduct();
                        $p->order_id          = $result['receipt_id'];
                        $p->product_id        = $product['product_id'];
                        $p->name              = $product['name'];
                        $p->sku               = $product['sku'];
                        $p->total_inc_tax     = $product['price_inc_tax'];
                        $p->quantity          = $product['quantity'];
                        $p->is_refunded       = 0;
                        $p->quantity_refunded = 0;

                        $p->save();

                        foreach ($product['product_data']['property_values'] as $option) {
                            $po                = new OrderProductOption();
                            $po->product_id    = $product['product_id'];
                            $po->display_name  = $option['property_name'];
                            $po->display_value = $option['values'][0];
                            $po->name          = $option['property_name'];

                            $po->save();

                            if ($po->display_name === 'My Show Date (Month):' || $po->display_name === 'My Show Date (Day of the Month):') {
                                $d [] = $po->display_value;
                            }
                        }
                    }


                    try {

                        if ($result['creation_tsz']) {
                            $date = Carbon::createFromTimestamp($result['creation_tsz'])->year . '-' . Carbon::parse('1 ' . $d[0])->month . '-' . $d[1];
                        } else {
                            $date = Carbon::today()->subYear();
                        }

                        //
                        if ($this->_checkIsUrgent($date)) {
                            $o->urgent_order = OrderType::URGENT;
                        } else {
                            $o->urgent_order = OrderType::REGULAR;
                        }

                        $o->show_date = Carbon::parse($date)->format('Y-m-d');
                    } catch (\Exception $e) {
                        $o->urgent_order = OrderType::REGULAR;
                    }
                }


                $o->payment_option = PaymentOption::FULL;
                $o->save();
            }
        }

        $this->info('Done!');
    }

    private function _checkIsUrgent(string $date): bool
    {
        $diff = Carbon::parse($date)->diffInDays(Carbon::now());

        return $diff <= (int)env('MINIMUM_ORDER_DAYS') ?? false;
    }

    private function _generateMinjaOrderId()
    {
        $order = Order::orderBy('minja_id', 'desc')->first();

        return $order->minja_id + 1;
    }
}

<?php

namespace App\Console\Commands;

use App\Models\ItemCard;
use Carbon\Carbon;
use Illuminate\Console\Command;

class FetchItemCards extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'minja:get-item-cards';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get item cards info';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $last_item_card = app(ItemCard::class)
            ->select('runid')
            ->orderBy('runid', 'desc')
            ->firstOrFail();

        $old_records = \DB::connection('mysql_old')
            ->table('item_card')
            ->where('runid', '>', $last_item_card->runid)
            ->orderBy('runid', 'desc')
            ->get();

        $i = 0;

        foreach ($old_records as $inf) {

            app(ItemCard::class)->create(collect($inf)->toArray());

            $i++;
        }

        if ($i > 0) {
            \Log::info('(' . $i . ') Item Card fetched : ' . Carbon::now());
        }

        $this->info('Done!');
    }
}

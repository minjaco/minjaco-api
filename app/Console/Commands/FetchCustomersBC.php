<?php

namespace App\Console\Commands;

use App\Models\Customer;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class FetchCustomersBC extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bc:fetch-customers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch customer from BC';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Customer::truncate();

        for ($i = 1; $i <= 163; $i++) {
            $filter = [
                'page' => $i,
                //'min_date_created' => '2018-01-01'
            ];
            $customers = \Bigcommerce::getCustomers($filter);

            foreach ($customers as $customer) {
                $c = new Customer();
                $c->customer_id = $customer->id;
                $c->uuid = Str::uuid();
                $c->name = $customer->first_name . ' ' . $customer->last_name;
                $c->phone_number = $customer->phone;
                $c->email = $customer->email;
                $c->registered_at = Carbon::parse($customer->date_created)->format('Y-m-d H:i:s');
                $c->accepts_marketing = $customer->accepts_marketing ? 1 : 0;

                $address = collect(\Bigcommerce::getCustomerAddresses($customer->id))->first();

                if ($address) {
                    $c->address = $address->street_1 . ' ' . $address->street_2 . ' ' . $address->city . ' ' . $address->state . ' ' . $address->zip;
                    $c->country = $address->country;
                }

                $c->save();
            }

            sleep(5);
        }

        $this->info('Done!');
    }
}

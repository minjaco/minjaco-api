<?php

namespace App\Console\Commands;

use App\Enums\OrderStatus;
use App\Enums\OrderType;
use App\Enums\PaymentOption;
use App\Models\Customer;
use App\Models\OldOrder;
use App\Models\Order;
use App\Models\OrderBilling;
use App\Models\OrderProduct;
use App\Models\OrderProductOption;
use App\Models\OrderShipping;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class BigCommerceOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bc:orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch periodically big commerce orders';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $filter = ['min_id' => $this->_latestBigCommerceOrderId() + 1];

        $orders = \Bigcommerce::getOrders($filter);

        if (!$orders) {
            return;
        }

        $i = 0;
        foreach ($orders as $order) {
            $id = $order->id;

            $o                      = new Order();
            $o->order_id            = $id;
            $o->minja_id            = $this->_generateMinjaOrderId($id);
            $o->customer_id         = $order->customer_id;
            $o->order_from          = 'BC-EN';
            $o->order_source        = $order->order_source;
            $o->items_total         = $order->items_total;
            $o->country             = $order->geoip_country;
            $o->payment_method      = $order->payment_method;
            $o->payment_option      = PaymentOption::FULL;
            $o->payment_provider_id = $order->payment_provider_id ?? '';
            $o->payment_status      = $order->payment_status;
            $o->external_source     = $order->external_source;
            $o->bc_status_id        = $order->status_id;
            $o->bc_status           = $order->status;
            $o->mc_status_id        = OrderStatus::WAITING_FOR_SIZING_INFO;
            $o->coupon_discount     = $order->coupon_discount;
            $o->discount_amount     = $order->discount_amount;
            $o->base_shipping_cost  = $order->base_shipping_cost;
            $o->subtotal_inc_tax    = $order->subtotal_inc_tax;
            $o->total_inc_tax       = $order->total_inc_tax;
            $o->customer_message    = $order->customer_message;
            $o->staff_notes         = $order->staff_notes;
            $o->order_source        = $order->order_source;
            $o->date_created        = Carbon::parse($order->date_created)->addHours(7)->format('Y-m-d H:i:s');
            $o->date_modified       = Carbon::parse($order->date_modified)->addHours(7)->format('Y-m-d H:i:s');
            $o->date_shipped        = Carbon::parse($order->date_shipped)->addHours(7)->format('Y-m-d H:i:s');

            $ob               = new OrderBilling();
            $ob->order_id     = $id;
            $ob->first_name   = $order->billing_address->first_name;
            $ob->last_name    = $order->billing_address->last_name;
            $ob->company      = $order->billing_address->company;
            $ob->street_1     = $order->billing_address->street_1;
            $ob->street_2     = $order->billing_address->street_2;
            $ob->city         = $order->billing_address->city;
            $ob->zip          = $order->billing_address->zip;
            $ob->state        = $order->billing_address->state;
            $ob->country      = $order->billing_address->country;
            $ob->country_iso2 = $order->billing_address->country_iso2;
            $ob->phone        = $order->billing_address->phone;
            $ob->email        = $order->billing_address->email;

            $ob->save();

            //Customers
            $customer = \Bigcommerce::getCustomer($order->customer_id);

            if ($customer) {
                //Check customer exits
                $check = app(Customer::class)->where('email', $customer->email)->first();

                if (!$check) {
                    $c                    = new Customer();
                    $c->customer_id       = $customer->id;
                    $c->uuid              = Str::uuid();
                    $c->name              = $customer->first_name . ' ' . $customer->last_name;
                    $c->phone_number      = $customer->phone;
                    $c->email             = $customer->email;
                    $c->registered_at     = Carbon::parse($customer->date_created)->addHours(7)->format('Y-m-d H:i:s');
                    $c->accepts_marketing = $customer->accepts_marketing ? 1 : 0;

                    $address = collect(\Bigcommerce::getCustomerAddresses($customer->id))->first();

                    if ($address) {
                        $c->address = $address->street_1 . ' ' . $address->street_2 . ' ' . $address->city . ' ' . $address->state . ' ' . $address->zip;
                        $c->country = $address->country;
                    }

                    $c->save();
                }
            }

            //Customers End

            $o_shipping = collect(\Bigcommerce::getOrderShippingAddresses($id))->first();

            if ($o_shipping) {
                $os                     = new OrderShipping();
                $os->order_id           = $o_shipping->order_id;
                $os->first_name         = $o_shipping->first_name;
                $os->last_name          = $o_shipping->last_name;
                $os->company            = $o_shipping->company;
                $os->street_1           = $o_shipping->street_1;
                $os->street_2           = $o_shipping->street_2;
                $os->city               = $o_shipping->city;
                $os->zip                = $o_shipping->zip;
                $os->country            = $o_shipping->country;
                $os->country_iso2       = $o_shipping->country_iso2;
                $os->state              = $o_shipping->state;
                $os->phone              = $o_shipping->phone;
                $os->email              = $o_shipping->email;
                $os->items_total        = $o_shipping->items_total;
                $os->shipping_method    = $o_shipping->shipping_method;
                $os->base_cost          = $o_shipping->base_cost;
                $os->cost_inc_tax       = $o_shipping->cost_inc_tax;
                $os->shipping_zone_id   = $o_shipping->shipping_zone_id;
                $os->shipping_zone_name = $o_shipping->shipping_zone_name;
                $os->save();
            }

            $products = collect(\Bigcommerce::getOrderProducts($id));

            if ($products) {
                if ($products->count() > 1) { // check the order has multiple pruducts for linking
                    $o->has_multiple_products = 1;
                }

                foreach ($products as $product) {
                    $p                    = new OrderProduct();
                    $p->order_id          = $id;
                    $p->product_id        = $product->product_id;
                    $p->name              = $product->name;
                    $p->sku               = $product->sku;
                    $p->total_inc_tax     = $product->total_inc_tax;
                    $p->quantity          = $product->quantity;
                    $p->is_refunded       = $product->is_refunded;
                    $p->quantity_refunded = $product->quantity_refunded;

                    $p->save();

                    foreach ($product->product_options as $option) {
                        $po                = new OrderProductOption();
                        $po->product_id    = $product->product_id;
                        $po->display_name  = $option->display_name;
                        $po->display_value = $option->display_value;
                        $po->name          = $option->name;

                        $po->save();

                        if ($option->name === 'Show Date') {
                            if ($this->_checkIsUrgent($option->display_value)) {
                                $o->urgent_order = OrderType::URGENT;
                            }

                            $o->show_date = Carbon::parse($option->display_value)->format('Y-m-d');
                        } elseif ($option->name === 'Payment option' && (int)$option->value === 656) {// 656 stands for partial payment
                            $o->payment_option = PaymentOption::PARTIAL;
                        }
                    }
                }
            }

            $o->save();

            $i++;
        }

        if ($i > 0) {
            \Log::info('(' . $i . ') Orders fetched : ' . Carbon::now());
        }

    }

    private function _checkIsUrgent(string $date): bool
    {
        $diff = Carbon::parse($date)->diffInDays(Carbon::now());

        return $diff <= (int)env('MINIMUM_ORDER_DAYS') ?? false;
    }

    private function _generateMinjaOrderId($order_id)
    {
        return app(Order::class)
                ->orderBy('minja_id', 'desc')
                ->first()
                ->minja_id + 1;
    }

    private function _latestBigCommerceOrderId()
    {
        return app(Order::class)
            ->where('order_from', 'BC-EN')
            ->orderBy('order_id', 'desc')
            ->first()
            ->order_id;
    }
}

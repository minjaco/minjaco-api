<?php

namespace App\Console\Commands;

use App\Imports\SizeDifferenceImporter;
use Illuminate\Console\Command;

class ImportCbSizeDifferences extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'imp:cbsd';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import crystal bikini size differences data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->output->title('Starting import');
        (new SizeDifferenceImporter)->withOutput($this->output)->import(storage_path('cb_size_difference.xlsx'));
        $this->output->success('Import successful');
    }
}

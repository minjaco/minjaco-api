<?php

namespace App\Console\Commands;

use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CleanIncompleteOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'minja:clean-incomplete-orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cleans incomplete orders from database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        app(Order::class)
            ->where('bc_status', 'Incomplete')
            ->delete();

        \Log::info('DB was cleaned up from incomplete orders : ' . Carbon::now());
        $this->info('Done!');
    }
}

<?php

namespace App\Console\Commands;

use App\Imports\FsSizeDifferenceImporter;
use Illuminate\Console\Command;

class ImportFsSizeDifferences extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'imp:fssd';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import figure suit size differences data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->output->title('Starting import');
        (new FsSizeDifferenceImporter)->withOutput($this->output)->import(storage_path('fs_size_difference.xlsx'));
        $this->output->success('Import successful');
    }
}

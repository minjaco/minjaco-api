<?php

namespace App\Console\Commands;

use App\Models\Product;
use Illuminate\Console\Command;

class FetchProdutcsBC extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bc:fetch-products';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch products from BC';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Product::truncate();

        for ($i = 1; $i <= 45; $i++) {
            $filter   = ['page' => $i];
            $products = \Bigcommerce::getProducts($filter);

            foreach ($products as $product) {
                $p              = new Product();
                $p->product_id  = $product->id;
                $p->name        = $product->name;
                $p->sku         = $product->sku;
                $p->price       = $product->price;
                $p->is_visible  = $product->is_visible;
                $p->is_featured = $product->is_featured;
                $p->total_sold  = $product->total_sold;
                $p->view_count  = $product->view_count;
                $p->custom_url  = $product->custom_url;
                $p->image_url   = $product->primary_image->standard_url ?? '';
                $p->description = $product->description;

                $p->save();
            }

            sleep(10);
        }

        $this->info('Done!');
    }
}

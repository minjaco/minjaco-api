<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class CreateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create 
                        {--username= : User name surname} 
                        {--email= : User email} 
                        {--password= : User password} 
                        {--department= : User department}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'User creator';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user_name  = $this->option('username');
        $email      = $this->option('email');
        $password   = $this->option('password');
        $department = $this->option('department');


        try {
            $user = app(User::class)
                ->create([
                    'uuid'         => Str::uuid(),
                    'name'         => $user_name,
                    'email'        => $email,
                    'password'     => bcrypt($password),
                    'redirect_uri' => $department,
                ]);

            $user->assignRole($department);

            $this->info('User ' . $user_name . ' created');
        } catch (\Exception $exception) {
            $this->error('Something went wrong! ' . $exception->getMessage());
        }
    }
}

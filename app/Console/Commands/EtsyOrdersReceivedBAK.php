<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Gentor\Etsy\Facades\Etsy;
use Illuminate\Console\Command;

class EtsyOrdersReceivedBAK extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'etsy:orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Saves Etsy orders into Bigcommerce';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $args = [
            'params'       => [
                'shop_id'     => env('ETSY_SHOP_ID'),
                //todo : change min_created to proper timestamp by latest etsy order
                'min_created' => Carbon::now()->subDays(2)->timestamp,
                'limit'       => env('ETSY_LIMIT'),
            ],
            'associations' => [
                'Transactions',
                'Country',
            ],
        ];

        $data = Etsy::findAllShopReceipts($args);

        foreach ($data['results'] as $result) {
            $customer = \Bigcommerce::getCustomers([
                'email' => $result['buyer_email'],
            ]);

            $name = explode(' ', $result['name']);

            if ($customer) {
                $products = [];

                foreach ($result['Transactions'] as $transaction) {
                    $products [] = [
                        'name'          => $transaction['title'],
                        'quantity'      => $transaction['quantity'],
                        'price_inc_tax' => $result['total_price'],
                    ];
                }


                $new_order_data = [
                    //7 - Awaiting Payment, 9 - Awaiting Shipment
                    'status_id'       => $result['was_paid'] ? 7 : 9,
                    'customer_id'     => $customer[0]->id,
                    'billing_address' => [
                        'first_name'   => trim($name[0]),
                        'last_name'    => trim($name[1]),
                        'street_1'     => $result['first_line'],
                        'street_2'     => $result['second_line'],
                        'city'         => $result['city'],
                        'state'        => $result['state'] ?? 'state',
                        'zip'          => $result['zip'],
                        'country'      => $result['Country']['name'],
                        'country_iso2' => $result['Country']['iso_country_code'],
                        'email'        => $result['buyer_email'],
                    ],
                    'products'        => $products,
                ];

            //$new_order = \Bigcommerce::createOrder($new_order_data);

                //dd($products, $new_order_data);
                //\Log::info('Etsy order for existing customer. ID: ' . $result['receipt_id']);
            } else {
                //todo : create customer if does not exist

                $new_customer_data = [
                    'first_name' => trim($name[0]),
                    'last_name'  => trim($name[1]),
                    'email'      => $result['buyer_email'],
                ];

                //$new_customer = \Bigcommerce::createCustomer($new_customer_data);

                $new_customer_address_data = [
                    'first_name' => trim($name[0]),
                    'last_name'  => trim($name[1]),
                    'phone'      => '000-000-0000',//fixme: check here
                    'street_1'   => $result['first_line'],
                    'street_2'   => $result['second_line'],
                    'city'       => $result['city'],
                    'state'      => $result['state'] ?? 'state',
                    'zip'        => $result['zip'],
                    'country'    => $result['Country']['name'],
                ];


                //\Bigcommerce::createCustomerAddress(
                //    $new_customer->id,
                //    $new_customer_address_data
                //);

                $products = [];

                foreach ($result['Transactions'] as $transaction) {
                    $products [] = [
                        'name'          => $transaction['title'],
                        'quantity'      => $transaction['quantity'],
                        'price_inc_tax' => $result['total_price'],
                    ];
                }


                $new_order_data = [
                    //7 - Awaiting Payment, 9 - Awaiting Shipment
                    'status_id'       => $result['was_paid'] ? 7 : 9,
                    'customer_id'     => 1,//todo : new id will come from bigcommerce
                    'billing_address' => [
                        'first_name'   => trim($name[0]),
                        'last_name'    => trim($name[1]),
                        'street_1'     => $result['first_line'],
                        'street_2'     => $result['second_line'],
                        'city'         => $result['city'],
                        'state'        => $result['state'] ?? 'state',
                        'zip'          => $result['zip'],
                        'country'      => $result['Country']['name'],
                        'country_iso2' => $result['Country']['iso_country_code'],
                        'email'        => $result['buyer_email'],
                    ],
                    'products'        => $products,
                ];


                //dd($new_customer_data, $new_customer_address_data, $new_order_data);
                //\Log::info('Etsy order for new customer. ID: ' . $result['receipt_id'] . ' customer email : ' . $result['buyer_email']);
            }
        }
    }
}

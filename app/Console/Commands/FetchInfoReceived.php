<?php

namespace App\Console\Commands;

use App\Enums\OrderStatus;
use App\Models\InfoReceived;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Console\Command;

class FetchInfoReceived extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'minja:get-info-received';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get received info for orders';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $last_order_local = app(InfoReceived::class)
            ->orderBy('order_id', 'desc')
            ->firstOrFail();

        $old_records = \DB::connection('mysql_old')
            ->table('info_received')
            ->where('order_id', '>', $last_order_local->order_id)
            ->get();

        $i = 0;

        foreach ($old_records as $inf) {

            $old_order = \DB::connection('mysql_old')
                ->table('tb_orders')
                ->where('order_id', $inf->order_id)
                ->first();

            $order = app(Order::class)
                ->where('order_id', $old_order->order_ref_no)
                ->first();

            if($order) {

                $info                      = new InfoReceived();
                $info->order_id            = $order->minja_id;
                $info->top_con             = $inf->top_con;
                $info->mid_con             = $inf->mid_con;
                $info->bot_con             = $inf->bot_con;
                $info->top_style           = $inf->top_style;
                $info->bot_style           = $inf->bot_style;
                $info->bot_front_cov_level = $inf->bot_front_cov_level;
                $info->bot_back_cov_level  = $inf->bot_back_cov_level;
                $info->weight_c            = $inf->weight_c;
                $info->weight_e            = $inf->weight_e;
                $info->tall                = $inf->tall;
                $info->bra_cup_size_c      = $inf->bra_cup_size_c;
                $info->bra_cup_size_s      = $inf->bra_cup_size_s;
                $info->how_com             = $inf->how_com;
                $info->anything_else       = $inf->anything_else;
                $info->how_confi           = $inf->how_confi;
                $info->implants            = $inf->implants;
                $info->top_id              = $inf->top_id;
                $info->mid_id              = $inf->mid_id;
                $info->bot_id              = $inf->bot_id;
                $info->comp_name           = $inf->comp_name;
                $info->hear_ab             = $inf->hear_ab;
                $info->time_stamp          = $inf->time_stamp;

                $info->save();

                $order->mc_status_id = OrderStatus::SIZING_INFO_RECEIVED;
                $order->save();

                $i++;
            }
        }

        if ($i > 0) {
            \Log::info('(' . $i . ') Info Received fetched : ' . Carbon::now());
        }

        $this->info('Done!');

    }
}

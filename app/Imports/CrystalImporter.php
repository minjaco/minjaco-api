<?php

namespace App\Imports;

use App\Models\Crystal;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Maatwebsite\Excel\Concerns\WithProgressBar;
use Maatwebsite\Excel\Concerns\WithStartRow;

class CrystalImporter implements ToCollection, WithStartRow, WithProgressBar, WithCalculatedFormulas
{
    use Importable;

    public function collection(Collection $rows)
    {

        foreach ($rows as $row) {

            if ($row->filter()->isNotEmpty()) {

                if ($row[0] === null) {
                    continue;
                }

                $crystal = app(Crystal::class)->where('item_code', $row[2])->first();

                if ($crystal) {

                    $crystal->update([
                        'item_group'     => $row[3],
                        'color'          => $row[4],
                        'shape_code'     => $row[5],
                        'shape_type'     => 0,
                        'cutting'        => $row[6],
                        'size'           => $row[7],
                        'description'    => $row[8],
                        'quantity'       => $row[38],
                        'unit_price'     => $row[9],
                        'shipping_cost'  => $row[10],
                        'total_price'    => $row[11],
                        'unit'           => 'Piece',
                        'location_stock' => 'Crystal',
                        'status'         => 1,
                        'action_by'      => 'System',
                        'rate_group'     => 'A',
                        'time_stamp'     => Carbon::now(),
                    ]);

                } else {
                    Crystal::create([
                        'item_code'      => $row[2],
                        'item_group'     => $row[3],
                        'color'          => $row[4],
                        'shape_code'     => $row[5],
                        'shape_type'     => 0,
                        'cutting'        => $row[6],
                        'size'           => $row[7],
                        'description'    => $row[8],
                        'quantity'       => $row[38],
                        'unit_price'     => $row[9],
                        'shipping_cost'  => $row[10],
                        'total_price'    => $row[11],
                        'unit'           => 'Piece',
                        'location_stock' => 'Crystal',
                        'status'         => 1,
                        'action_by'      => 'System',
                        'rate_group'     => 'A',
                        'time_stamp'     => Carbon::now(),
                    ]);
                }
            }

        }

    }

    public function startRow(): int
    {
        return 5;
    }
}

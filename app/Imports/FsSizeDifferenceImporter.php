<?php

namespace App\Imports;

use App\Models\FsSizeDifference;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Maatwebsite\Excel\Concerns\WithProgressBar;
use Maatwebsite\Excel\Concerns\WithStartRow;

class FsSizeDifferenceImporter implements ToCollection, WithStartRow, WithProgressBar, WithCalculatedFormulas
{
    use Importable;

    public function collection(Collection $rows)
    {

        //$fix   = 648;
        //$pack1 = 360;
        //$pack2 =
        foreach ($rows as $row) {

            if ($row->filter()->isNotEmpty()) {

                if ($row[0] === null) {
                    continue;
                }

                //$total = ceil($row[19] * $fix);

                //$r = ceil($total / $pack1);

                //$dist = $r > 3 ? 'One big pack ' : $r.' small pack ';

                FsSizeDifference::create([
                    'top_size'                 => $row[0],
                    'bottom_size'              => $row[1],
                    'bottom_cut'               => $row[2],
                    'pattern_code_one'         => $row[11],
                    'pattern_code_two'         => $row[11],
                    'pattern_code_three'       => $row[11],
                    'top_sqrcm'                => $row[12],
                    'bottom_front_sqrcm'       => $row[13],
                    'bottom_back_sqrcm'        => $row[14],
                    'bottom_total_sqrcm'       => $row[15],
                    'top_bottom_total_sqrcm'   => $row[16],
                    'top_percent'              => round($row[18], 2),
                    'bottom_front_percent'     => round($row[19], 2),
                    'bottom_back_percent'      => round($row[20], 2),
                    'bottom_total_percent'     => round($row[21], 2),
                    'top_bottom_total_percent' => round($row[22], 2),
                    //'total_crystals_as_pcs'    => $total,
                    //'total_pack_to_distribute' => $dist,
                ]);
            }

        }

    }

    public function startRow(): int
    {
        return 5;
    }
}

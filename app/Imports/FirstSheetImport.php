<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class FirstSheetImport implements ToCollection
{
    /**
     * Import Figure suit sizing chart
     * @param Collection $collection
     */
    public function collection(Collection $collection)
    {
        $matrix = [];

        $heights = [];
        $weights = [];

        $temp = 0;
        $add  = 1;

        foreach ($collection->get(2) as $key => $value) {
            if ($key === 0) {
                $heights [] = 0;
            } elseif ($key === 57) {
                $heights [] = 0;
            } elseif ($key === 1) {
                $temp       = $value;
                $heights [] = $temp;
            } else {
                $heights [] = @($temp + $add);
                $add++;
            }
        }

        $w    = 0;
        $temp = 0;
        $add  = 1;

        foreach ($collection as $row) {
            if ($w < 49) {
                if ($w < 3) {
                    $weights [] = 0;
                } elseif ($w === 3) {
                    $temp       = $row[0];
                    $weights [] = $temp;
                } else {
                    $weights [] = @($temp + $add);
                    $add++;
                }
            }

            $w++;
        }


        $t = 0;
        $k = 0;

        foreach ($collection as $row) {
            if ($t > 2 && $t < 49) {
                foreach ($row as $r) {
                    $matrix[$heights[$k]][$weights[$t]] = $row[$k] ?? 0;
                    $k++;
                }
                $k = 0;
            }
            $t++;
        }

        echo '<pre>';
        $this->varexport($matrix);
        echo '</pre>';
    }

    /**
     * Array clean up helper
     * @param $expression
     * @param bool $return
     * @return mixed|string|string[]|null
     */
    private function varexport($expression, $return = false)
    {
        $export = var_export($expression, true);
        $export = preg_replace('/^([ ]*)(.*)/m', '$1$1$2', $export);
        $array  = preg_split("/\r\n|\n|\r/", $export);
        $array  = preg_replace(["/\s*array\s\($/", "/\)(,)?$/", "/\s=>\s$/"], [null, ']$1', ' => ['], $array);
        $export = implode(PHP_EOL, array_filter(['['] + $array));
        if ((bool)$return) {
            return $export;
        } else {
            echo $export;
        }
    }
}

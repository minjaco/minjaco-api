<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class ImageImport implements ToCollection, WithChunkReading
{
    /**
     * @param Collection $collection
     */
    public function collection(Collection $collection)
    {
        $i = 0;
        foreach ($collection as $row) {
            if (($i > 1) && $row->filter()->isNotEmpty()) {
                try {
                    $folders = explode('/', $row[2]);
                    $f = $folders[0] . DIRECTORY_SEPARATOR . $folders[1];
                    \File::move(
                        storage_path('product_images' . DIRECTORY_SEPARATOR . $row[2]),
                        storage_path('product_images' . DIRECTORY_SEPARATOR . $f . DIRECTORY_SEPARATOR . $row[3])
                    );
                } catch (\Exception $ex) {
                    \Log::error($ex->getMessage());
                }
            }
            $i++;
        }
        echo 'Done';
    }

    public function chunkSize(): int
    {
        return 500;
    }
}

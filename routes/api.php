<?php

Broadcast::routes(['middleware' => 'auth:api']);

Route::group(
//['prefix' => 'v1'],
    ['prefix' => ''],
    function () {
        Route::group(
            [
                'prefix'    => 'auth',
                'namespace' => 'Api\Auth',
            ],
            function () {
                Route::post('login', 'AuthController@login');
                Route::post('register', 'AuthController@register');

                Route::group(
                    [
                        'middleware' => 'auth:api',
                    ],
                    function () {
                        Route::get('logout', 'AuthController@logout');
                        Route::get('me', 'AuthController@user');
                    }
                );
            }
        );

        Route::group(
            [
                'namespace'  => 'Api',
                'middleware' => 'auth:api',
            ],
            function () {
                Route::get('permissions', 'PermissionController');
                Route::get('roles', 'RoleController');
            }
        );

        Route::group(
            [
                'prefix'     => 'customer-service',
                'namespace'  => 'Api\CustomerService',
                'middleware' => 'auth:api',
            ],
            function () {
                Route::get('events', 'CalenderController@events');
                Route::resource('order-notes', 'OrderNoteController');
                Route::resource('internal-messages', 'InternalMessageController');
                Route::resource('internal-message-replies', 'InternalMessageReplyController');
            }
        );

        Route::group(
            [
                'prefix'     => 'common',
                'namespace'  => 'Api',
                'middleware' => 'auth:api',
            ],
            function () {
                Route::get('department-users', 'Department\DepartmentHelperController@users');
                Route::get('orders-of-today', 'OrdersOfTodayController@index');
                Route::post('replicate-order', 'OrderHelperController@replicate');
                Route::resource('order-status', 'OrderStatusController');
                Route::resource('customer-extra-information', 'CustomerExtraInformationController');
                Route::resource('customer-default-email', 'CustomerDefaultEmailController');
                Route::resource('customers', 'CustomerController');
                Route::resource('orders', 'OrderController');
                Route::resource('management-messages', 'ManagementMessageController');
                Route::resource('sizing', 'SizingController');

            }
        );

        Route::group(
            [
                'prefix'     => 'data',
                'namespace'  => 'Api',
                'middleware' => 'auth:api',
            ],
            function () {

                Route::get('customers', 'DataServeController@customers');
                Route::get('statuses', 'DataServeController@statuses');
                Route::get('top-styles', 'DataServeController@topStyles');
                Route::get('bottom-styles', 'DataServeController@bottomStyles');
                Route::get('figure-suit-bottom-styles', 'DataServeController@figureSuitBottomStyles');
                Route::get('coverage', 'DataServeController@coverageLevel');
                Route::get('connectors/{type}', 'DataServeController@connectors');
                Route::get('fabrics', 'DataServeController@fabrics');
                Route::get('bottom-sizes', 'DataServeController@bottomSizes');
                Route::post('customer-profile-picture', 'DataHandleController@customerProfilePicture');
                Route::post('order-swatches', 'DataHandleController@orderSwatches');
                Route::post('delete-order-swatch', 'DataHandleController@deleteOrderSwatch');
                Route::post('combination-data', 'DataServeController@combinationData');
                Route::post('combination-letter-data', 'DataServeController@combinationLetterData');
                Route::post('figure-suit-bottom-back-data', 'DataServeController@figureSuitBottomBack');
                Route::post('figure-suit-bottom-front-data', 'DataServeController@figureSuitBottomFront');
                Route::post('figure-suit-bottom', 'DataServeController@figureSuitBottom');
            }
        );

        Route::get('test', function () {
            /*event(
                (new App\Events\ManagementMessageCreated('Dean', 'customer-service'))->dontBroadcastToCurrentUser()
            );*/

            $user = \App\Models\User::first();

            /*$message = new \App\Models\DashboardMessage();

            $message->user_id = 5;
            $message->type    = \App\Enums\DashboardMessageType::DEPARTMENT;
            $message->group   = 'customer-service';
            $message->content = 'Content 2';

            $message->save();

            broadcast(new App\Events\ManagementMessageSent($user, $message));//->toOthers();*/

            $message = new \App\Models\DashboardMessage();

            $message->user_id = 3;
            $message->to      = 0;
            $message->type    = \App\Enums\DashboardMessageType::MANAGEMENT;
            $message->group   = 'everybody';
            $message->content = 'Content Every Body';

            $message->save();

            // toOthers checks the X-Socket-Id
            broadcast(new App\Events\ManagementMessageSent($user, $message))->toOthers();
            /*event(
                (new App\Events\ManagementMessageSent($user, $message))->dontBroadcastToCurrentUser()
            );*/

            Cache::tags(['management-messages'])->flush();

            //$order = \App\Models\Order::first();

            //broadcast(new App\Events\OrderReceived($order));

            return 'Event has been sent!';
        });
    }
);

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('invoice/{invid}', 'HelperController@invoice');
Route::get('matrix', 'HelperController@matrix');
Route::get('etsy', 'EtsyHelperController@index');
Route::get('etsy/callback', 'EtsyHelperController@store');
Route::get('etsy/data', 'EtsyHelperController@data');
Route::get('rename', 'HelperController@rename');
Route::get('cb-rename', 'HelperController@cbRename');
Route::get('enc', 'HelperController@enc');
Route::get('denc', 'HelperController@denc');
Route::get('viewa', 'HelperController@viewa');
Route::get('search', 'HelperController@search');
Route::get('try', 'HelperController@try');
Route::get('old-db', 'HelperController@oldDb');
Route::get('get-bc-products', 'HelperController@fetchProductsFromBigCommerce');
Route::get('get-bc-customers', 'HelperController@fetchCustomersFromBigCommerce');
Route::get('get-ac-contact', 'HelperController@fetchCustomersFromActiveCampaign');
Route::get('pimages', 'HelperController@productImages');
Route::get('convert', 'HelperController@convert');
Route::get('ci-url', 'HelperController@createUserImageUrl');
Route::get('ci-url-decode/{token}', 'HelperController@decodeUserOrder');
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::group(
    ['prefix' => 'webhooks'],
    function () {
        //Disabled due to scheduled command as cron jon
        //Route::post('order-created', 'WebHookController@orderCreated');
        /*Route::post('order-updated', 'WebHookController@orderUpdated');// Todo : for new minja
        Route::post('order-status-updated', 'WebHookController@orderStatusUpdated');
        Route::post('customer-created', 'WebHookController@customerCreated');
        Route::post('customer-updated', 'WebHookController@customerUpdated');
        Route::post('product-created', 'WebHookController@productCreated');
        Route::post('product-updated', 'WebHookController@productUpdated');*/

        Route::post('order-status-updated', 'OldMinjaHook\OrderController');
        //Route::get('order-status-updated', 'OldMinjaHook\OrderController');
    }
);

Route::group(
    ['prefix' => 'reports', 'middleware' => 'cors',],
    function () {
        Route::get('sewing-report', 'Report\GenericController@sewn');
        Route::get('sewing-report-by-status', 'Report\GenericController@sewnByStatus');
        Route::get('crystal-report', 'Report\GenericController@crystal');
        Route::get('packing-report', 'Report\GenericController@packing');
        Route::get('finished-suits-report', 'Report\GenericController@finishedSuits');
        Route::get('finish-good-report', 'Report\GenericController@finishGoods');
        Route::get('shipping-report', 'Report\GenericController@shipping');
    }
);


Route::get('/{any}', 'SpaController@index')->where('any', '.*');

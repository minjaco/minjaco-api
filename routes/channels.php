<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('management-message.{role}', function ($user, $role) {

    return $user->hasRole($role) || $role === 'everybody';

});

Broadcast::channel('internal-message.{role}', function ($user, $role) {

    return $user->hasRole($role);

});

Broadcast::channel('internal-message-to-user.{id}', function ($user, $id) {

    return (int)$user->id === (int)$id;

});


Broadcast::channel('order-received', function ($user) {

    return $user ?? false;

});

Broadcast::channel('online.{role}', function ($user, $role) {

    if ($user->hasRole($role)) {
        return $user;
    }

});

Broadcast::channel('order-activity.{order_id}', function ($user, $order_id) {
    return $user ?? false;
});
